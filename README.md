# BackApp

[![CI Status](http://img.shields.io/travis/Gegham Harutyunyan/BackApp.svg?style=flat)](https://travis-ci.org/Gegham Harutyunyan/BackApp)
[![Version](https://img.shields.io/cocoapods/v/BackApp.svg?style=flat)](http://cocoapods.org/pods/BackApp)
[![License](https://img.shields.io/cocoapods/l/BackApp.svg?style=flat)](http://cocoapods.org/pods/BackApp)
[![Platform](https://img.shields.io/cocoapods/p/BackApp.svg?style=flat)](http://cocoapods.org/pods/BackApp)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

BackApp is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "BackApp"
```

## Author

Gegham Harutyunyan, gegham.harutyunyan@way4app.com

## License

BackApp is available under the MIT license. See the LICENSE file for more info.
