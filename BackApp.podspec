#
# Be sure to run `pod lib lint BackApp.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BackApp'
  s.version          = '0.2.002'
  s.summary          = 'BackApp is iOS SDK for BackApp backend'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/gegham_harutyunyan/backapp'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Gegham Harutyunyan' => 'gegham.harutyunyan@way4app.com' }
  s.source           = { :git => 'https://gegham_harutyunyan@bitbucket.org/gegham_harutyunyan/backapp.git', :tag => 'v0.2.002' }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.2'

  s.source_files = 'BackApp/Classes/**/*'
  
  # s.resource_bundles = {
  #   'BackApp' => ['BackApp/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'SwiftyJSON'
  s.dependency 'Alamofire'
  s.dependency 'GRDB.swift', '= 2.0'
end
