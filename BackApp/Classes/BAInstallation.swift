//
//  BAInstallation.swift
//  Pods
//
//  Created by GeghamHarutyunyan on 6/19/17.
//
//

import Foundation

@objc

open class BAInstallation : BAObject
{
    @objc public internal(set) dynamic var deviceType:String?
    @objc public dynamic var deviceToken: String?
    @objc public internal(set) dynamic var localeIdentifier: String?
    @objc dynamic var badge: BAInteger?
    @objc dynamic var channels : BAArray?
    
    static var installationId: String?{
        if let installation_id = UserDefaults.standard.object(forKey: "installation_id") as? String
        {
            return installation_id
        }
        
        return nil
    }
    
    static var token: String?{
        get {
            if let tok = UserDefaults.standard.object(forKey: "installation_token")
            {
                return (tok as! String)
            }
            
            return nil
        }
        
        set {
            if newValue != nil
            {
                UserDefaults.standard.set(newValue, forKey: "installation_token")
            }
        }
    }
    
    // MARK: - Base
    open override class func mapName() -> String
    {
        return "Installation"
    }
    
    class func setCurrentInstallation<T>(installation:T) where T:BAObject
    {
        UserDefaults.standard.setValue(installation._id, forKey: "installation_id")
        
        if !installation.isEventuallyCall
        {
            try! installation.saveLocal()
        }
    }

    class func removeInstallation()
    {
        UserDefaults.standard.removeObject(forKey: "installation_token")
        if let installation_id = BAInstallation.installationId
        {
            let install = BAInstallation()
            install._id = installation_id
            _ = try! install.deleteLocal()
        }
    }
    
    public class func currentInstallation() -> Self {
        return  currentInstallation(type: self)
    }
    
    private class func currentInstallation<T>(type: T.Type) -> T where T:BAObject
    {
        // By key
        if let installation_id = BAInstallation.installationId
        {
            let install = T.instance(objType: type, _id: installation_id, local_id: nil)
            if !install.isNewObject
            {
                return install
            }
            
            install.ignoreObserving {
                install._id = installation_id
            }
            
            var query = BAQuery(type)
            query.includePointers()
            if install.fetchSingle(query)
            {
                return install
            }
            
            return install
        }
        else
        {
            let install = T()
            install["deviceType"] = "ios"
            install["localeIdentifier"] = TimeZone.current.identifier

            let result = BackApp.create_Local(instance: install)
            if result.isSuccess
            {
                UserDefaults.standard.setValue(result.value!._id, forKey: "installation_id")
            }
            
            return install
        }
    }
    
    ///Sets the device token string property from an Data-encoded token
    public func setDeviceToken(from data:Data)
    {
        let tokenParts = data.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        self.deviceToken = token
    }
}
