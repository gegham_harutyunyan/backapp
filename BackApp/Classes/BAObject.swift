//
//  BAObject.swift
//  BackApp
//
//  Created by GeghamHarutyunyan on 1/13/17.
//  Copyright © 2017 WAY4APP. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import GRDB

enum BAStateType
{
    case Cloud
    case Action
    case Eventually
}

struct BARealtionState
{
    var added:[String] //_ids
    var deleted:[String] //_ids
}

final class BAStateObject
{
    var isCreate = false
    var states : Set<String>!
    var type : BAStateType!
    var relations = Dictionary<String, BARealtionState>()
    var encodedParams: [String : Any]!
    
    required init(_ type: BAStateType = .Cloud)
    {
        self.type = type
    }
    
    func addRelationState(_ relationName: String, added:[String], deleted:[String])
    {
        let relState = BARealtionState(added: added, deleted: deleted)
        self.relations[relationName] = relState
    }
    
    func getRelationState(_ relationName: String) -> BARealtionState?
    {
        return relations[relationName]
    }
}

extension BAObject
{
    func addNewState(_ type: BAStateType = .Cloud) -> BAStateObject
    {
        let objState = BAStateObject(type)
        
        objState.states = self.propertiesToUpdate
        self.propertiesToUpdate.removeAll()
        
        if type == .Eventually
        {
            self.FieldsToUpdate = ""
        }
        
        if self.isSyncProcess()
        {
            self.stateObjects.insert(objState, at: 0)
        }
        else
        {
            self.stateObjects.append(objState)
        }
        
        //Encode params
        objState.encodedParams = self.paramsToSend()
        
        return objState
    }
    
    func lastStateChanges() -> Set<String>
    {
        let state = self.stateObjects.last
        
        if let props = state?.states
        {
            return props
        }
        
        return Set()
    }
    
    func allChangedKeys(relations included: Bool = true) -> Set<String>
    {
        var allProps = Set<String>()
        for state in self.stateObjects
        {
            let props = state.states!
            allProps = allProps.union(props)
        }
        
        if included == false
        {
            let rels = self.getPointersAndRelations().relations
            let excludedRels = allProps.filter({ (key) -> Bool in
                if rels.contains(key)
                {
                    return false
                }
                
                return true
            })
            
            allProps = Set(excludedRels)
        }
        
        return allProps
    }
    
    func failedState()
    {
        if let state = self.stateObjects.first
        {
            self.stateObjects.removeFirst()
            
            let failedProps = state.states.filter({ (key) -> Bool in
                
                if self.allChangedKeys().contains(key)
                {
                    return false
                }
                
                return true
            })
            
            self.propertiesToUpdate = self.propertiesToUpdate.union(failedProps)
            if state.type == .Eventually
            {
                self.FieldsToUpdate = propertiesToUpdate.joined(separator: ",")
            }
        }
    }
    
    func removeState()
    {
        self.stateObjects.removeFirst()
    }
    
    // MARK: - Relation States
    func relationState(_ relationName: String) -> (added:[String], deleted:[String])
    {
        var added = [String]()
        var deleted = [String]()
        for state in self.stateObjects
        {
            if let relState = state.relations[relationName]
            {
                added += relState.added
                deleted += relState.deleted
            }
        }
        
        return (added, deleted)
    }
}

@objc
open class BAObject: NSObject, TableMapping, Persistable
{
    // MARK: - Properties
    
    // Local
    @objc var local_id: BAInteger?
    @objc internal(set) var ba_status :String = BAObjectStatus.None.rawValue
    @objc internal(set) var FieldsToUpdate : String = ""
    
    // Cloud
    @objc internal(set) public dynamic var _id          :String
    @objc internal(set) public dynamic var createdAt    :Date? = nil
    @objc internal(set) public dynamic var updatedAt    :Date? = nil
    
    //Other properties
    static var MindData = [String : Any]()
    static var GlobalInfo = [String: Any]()
    static let observer = BAObserver()
    
    var propertiesToUpdate = Set<String>()
    var stateObjects = [BAStateObject]()
    var LocalUsedProperties = Dictionary<String, Any>()
    
    
    // MARK: - Base
    required public override init() {
        
        _id = "\(k.DefaultKeys.temp_prefix)\(UUID().uuidString)"
        
        super.init()
        
        LocalUsedProperties[k.DefaultKeys.Active_Keys] = BAKeys()
        BAObject.observer.startObserving(self)
        
        for (_ ,function) in getFuncions(of: .Filter)
        {
            function.subscribeChannel = String(ObjectIdentifier(self).hashValue)
        }
    }
    
    deinit
    {
        BAObject.observer.stopObserving(self)
    }
    
    open class func mapName() -> String
    {
        return ""
    }
    
    
    /// This function determine either this class local or not
    open class func isLocalClass() -> Bool
    {
        return false
    }
    
    // MARK: - Properties
    class func ignoredPropertiesCloudSend() -> [String]
    {
        return onlyLocalProperties() + fullIgnoredProperties()
    }
    
    class func onlyLocalProperties() -> [String]
    {
        return [k.DefaultKeys.status, k.DefaultKeys.local_PK, k.DefaultKeys.local_FieldsToUpdate]
    }
    
    class func fullIgnoredProperties() -> [String]
    {
        return ["propertiesToUpdate",
                "LocalUsedProperties",
                "observers",
                "propertyObservers",
                "_propertyObserversDict",
                "stateObjects"] + ignoredProperties()
    }
    
    open class func ignoredProperties() -> [String]
    {
        return []
    }
    
    // MARK: - Observation
    
    public enum ObserveType
    {
        case Next
        case Last
    }
    public typealias UpdateModelCallBack = () -> Void
    
    private var observers:[(type: ObserveType,block: UpdateModelCallBack)] = [] {
        willSet(values) {
            
            if let val = values.last
            {
                if val.type == .Last
                {
                    val.block()
                }
            }
        }
    }
    
    private var _propertyObserversDict : [String: (type: ObserveType,block: UpdateModelCallBack)] = [:]
    private var propertyObservers:[(property: String,type: ObserveType,block: UpdateModelCallBack)] = [] {
        willSet(values) {
            
            if let val = values.last
            {
                _propertyObserversDict[val.property] = (val.type, val.block)
                if val.type == .Last
                {
                    val.block()
                }
            }
        }
    }
    
    public func observe(_ type:ObserveType, callback:@escaping UpdateModelCallBack)
    {
        observers.append((type,callback))
    }
    
    public func observe(property name:String, type: ObserveType, callback:@escaping UpdateModelCallBack)
    {
        propertyObservers.append((name,type,callback))
    }
    
    func notify(property name: String? = nil)
    {
        if (name != nil)
        {
            if let callback = _propertyObserversDict[name!]
            {
                callback.block()
            }
        }
        else
        {
            for callback in observers
            {
                callback.block()
            }
        }
    }
    
    // MARK: - DB Protocol Methods
    
    public func encode(to container: inout PersistenceContainer) {
        for key in propertyNames(includeFunctions: false)
        {
            if let type = getTypeOfProperty(name: key)
            {
                if let val = value(forKey: key)
                {
                    //BOOL
                    if BATypes.BABool.isEqual(type: type)
                    {
                        let optBool:BABool = val as! BABool
                        container[key] = optBool.value as DatabaseValueConvertible?
                    }
                        //FILE
                    else if BATypes.BAFile.isEqual(type: type)
                    {
                        let file:BAFile = val as! BAFile
                        container[key] = file.url as DatabaseValueConvertible?
                    }
                        //NUMBER
                    else if BATypes.BANumber.isEqual(type: type)
                    {
                        let optBool:BANumber = val as! BANumber
                        container[key] = optBool.value as DatabaseValueConvertible?
                    }
                        //INTEGER
                    else if BATypes.BAInteger.isEqual(type: type)
                    {
                        let optBool:BAInteger = val as! BAInteger
                        container[key] = optBool.value as DatabaseValueConvertible?
                    }
                        //DATE
                    else if BATypes.Date.isEqual(type: type)
                    {
                        container[key] = (val as! Date).iso8601
                    }
                        //ARRAY
                    else if BATypes.BAArray.isEqual(type: type)
                    {
                        container[key] = (val as! BAArray).data as DatabaseValueConvertible?
                    }
                        //DICTIONARY
                    else if BATypes.BADictionary.isEqual(type: type)
                    {
                        container[key] = (val as! BADictionary).data as DatabaseValueConvertible?
                    }
                        //STRING
                    else if BATypes.String.isEqual(type: type)
                    {
                        container[key] = val as? String
                    }
                        //RELATION
                    else if BATypes.BARelation.isEqual(type: type)
                    {
                        
                    }
                        //PPOINTER
                    else
                    {
                        //
                        container[key] = (val as! BAObject)._id
                    }
                }
                else
                {
                    if !BATypes.BARelation.isEqual(type: type)
                    {
                        container[key] = nil as DatabaseValueConvertible?
                    }
                }
            }
        }
    }
    
    /// The name of the database table
    public static var databaseTableName:String { return mapName() }
    
    public func localInstance(rows: [Row], query:BAQuery? = nil,isNested: Bool = false)
    {
        if rows.count == 0
        {
            return
        }
        
        // Merging rows in one row
        // It may needed only if few pointers exists in the object
        var arr = rows[0].map { (key, value) -> (String,DatabaseValue) in
            return (key, value)
        }
        
        if rows.count > 1
        {
            let propNames = propertyNames(includeRelations: false)
            for index in 1...(rows.count - 1)
            {
                var nextArr = rows[index].map { (key, value) -> (String,DatabaseValue) in
                    return (key, value)
                }
                nextArr.removeSubrange(0...propNames.count-1)
                arr = arr + nextArr
            }
        }
        
        //Fill base object
        let propNames = propertyNames(includeRelations: false, includeFunctions: false)
        let dict = getDict(of: propNames, from: &arr)
        fillFromLocalDB(dict: dict, propNames: propNames)
        
        //No need to fetch pointers/relations of subdocument
        if (isNested){ return }
        
        if var mainQuery = query
        {
            let includedPointers = mainQuery.getIncludedSubdocs().pointer
            
            for pointerName in includedPointers
            {
                if let type = getTypeOfProperty(name: pointerName)
                {
                    if arr.count > 0
                    {
                        let ty = BAObject.getTypeObject(of: "\(type.clearType())")
                        
                        let pointerPropNames = ty.propertyNames(includeRelations: false, includeFunctions: false)
                        let pointerDict = getDict(of: pointerPropNames, from: &arr)
                        
                        //Try to get it from Mind
                        if let server_ID = pointerDict[k.DefaultKeys.ServerUniqueID]?.storage.value as? String
                        {
                            var local_ID: BAInteger? = nil
                            if let locVal = pointerDict[k.DefaultKeys.local_PK]?.storage.value as? Int64
                            {
                                local_ID = BAInteger(locVal)
                            }
                            
                            let tyObj = ty.instance(objType: ty, _id: server_ID, local_id: local_ID)
                            
                            if !tyObj.isNewObject
                            {
                                self[pointerName] = tyObj
                            }
                            else
                            {
                                tyObj.fillFromLocalDB(dict: pointerDict, propNames: pointerPropNames)
                                
                                self[pointerName] = tyObj
                                
                                //Keep In mind
                                BAObject.keepInMind(obj: tyObj)
                            }
                        }
                    }
                }
            }
        }
        
        fillRelationData(query)
    }
    
    private func fillRelationData(_ query: BAQuery?)
    {
        if var mainQuery = query
        {
            let includedRelations = mainQuery.getIncludedSubdocs().relation
            
            for relationName in includedRelations
            {
                let ba_relation = self.value(forKey: relationName) as! BARelation
                
                let values = self.selectRelationData(ba_relation.storedType,
                                                     relationName: relationName)
                
                if values.count > 0
                {
                    for val in values
                    {
                        ba_relation.store(object: val)
                    }
                }
            }
        }
    }
    
    private func getDict(of propNames:[String],from array: inout [(String, DatabaseValue)]) -> Dictionary<String, DatabaseValue>
    {
        let subarray = array[0...propNames.count - 1]
        let combined = [(String, DatabaseValue)]() + subarray
        
        //remove elements for this object
        array.removeSubrange(0...propNames.count-1)
        
        return Dictionary(keyValuePairs: combined)
    }
    
    private func fillFromLocalDB(dict: Dictionary<String, DatabaseValue>, propNames: [String])
    {
        if dict[k.DefaultKeys.ServerUniqueID] != nil
        {
            self.ignoreObserving {
                for prop in propNames
                {
                    if let type = self.getTypeOfProperty(name: prop)
                    {
                        if let val = dict[prop]?.storage.value
                        {
                            if BATypes.Date.isEqual(type: type)
                            {
                                self[prop] = (val as! String).dateFromISO8601
                            }
                            else if BATypes.BABool.isEqual(type: type)
                            {
                                let ba_bool: BABool?
                                if let value = self.value(forKey: prop)
                                {
                                    ba_bool = (value as! BABool)
                                }
                                else
                                {
                                    ba_bool = BABool()
                                    self[prop] = ba_bool
                                }
                                
                                ba_bool?.value = (val as! NSNumber).boolValue
                            }
                            else if BATypes.BAFile.isEqual(type: type)
                            {
                                let ba_file: BAFile?
                                if let val = self.value(forKey: prop)
                                {
                                    ba_file = (val as! BAFile)
                                }
                                else
                                {
                                    ba_file = BAFile()
                                    self[prop] = ba_file
                                }
                                
                                ba_file?.updateURL((val as! String))
                            }
                            else if BATypes.BANumber.isEqual(type: type)
                            {
                                let ba_num: BANumber?
                                if let value = self.value(forKey: prop)
                                {
                                    ba_num = (value as! BANumber)
                                }
                                else
                                {
                                    ba_num = BANumber()
                                    self[prop] = ba_num
                                }
                                
                                ba_num?.value = (val as! Double)
                            }
                            else if BATypes.BAInteger.isEqual(type: type)
                            {
                                let ba_int: BAInteger?
                                if let value = self.value(forKey: prop)
                                {
                                    ba_int = (value as! BAInteger)
                                }
                                else
                                {
                                    ba_int = BAInteger()
                                    self[prop] = ba_int
                                }
                                
                                ba_int?.value = (val as! Int64)
                            }
                            else if BATypes.BAArray.isEqual(type: type)
                            {
                                let ba_array: BAArray?
                                if let value = self.value(forKey: prop)
                                {
                                    ba_array = (value as! BAArray)
                                }
                                else
                                {
                                    ba_array = BAArray()
                                    self[prop] = ba_array
                                }
                                
                                ba_array?.data = (val as! Data)
                            }
                            else if BATypes.BADictionary.isEqual(type: type)
                            {
                                let ba_dict: BADictionary?
                                if let value = self.value(forKey: prop)
                                {
                                    ba_dict = (value as! BADictionary)
                                }
                                else
                                {
                                    ba_dict = BADictionary()
                                    self[prop] = ba_dict
                                }
                                
                                ba_dict?.data = (val as! Data)
                            }
                            else if BATypes.String.isEqual(type: type)
                            {
                                self[prop] = val
                            }
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - KVC
    
    open subscript(key: String) -> Any? {
        get {
            if let type = getTypeOfProperty(name: key)
            {
                if BATypes.BABool.isEqual(type: type)
                {
                    let ba_bool: BABool? = value(forKey: key) as? BABool
                    return ba_bool?.value
                }
                else if BATypes.BAFile.isEqual(type: type)
                {
                    let ba_file: BAFile? = value(forKey: key) as? BAFile
                    return ba_file?.url
                }
                else if BATypes.BAFunction.isEqual(type: type)
                {
                    let ba_func: BAFunction? = value(forKey: key) as? BAFunction
                    
                    if  ba_func?.funcName == .Size ||
                        ba_func?.funcName == .Sum ||
                        ba_func?.funcName == .Min ||
                        ba_func?.funcName == .Max
                    {
                        return ba_func?.valueStr
                    }
                    else
                    {
                        return ba_func?.obj as? BAObject
                    }
                }
                else if BATypes.BANumber.isEqual(type: type)
                {
                    let optNum:BANumber? = value(forKey: key) as? BANumber
                    return optNum?.value
                }
                else if BATypes.BAInteger.isEqual(type: type)
                {
                    let optInt:BAInteger? = value(forKey: key) as? BAInteger
                    return optInt?.value
                }
                else if BATypes.BAArray.isEqual(type: type)
                {
                    if let array = value(forKey: key)
                    {
                        return (array as! BAArray).value
                    }
                    return nil
                }
                else if BATypes.BADictionary.isEqual(type: type)
                {
                    if let array = value(forKey: key)
                    {
                        return (array as! BADictionary).value
                    }
                    return nil
                }
                else if BATypes.BARelation.isEqual(type: type)
                {
                    if let relation = value(forKey: key)
                    {
                        return (relation as! BARelation).objects
                    }
                }
                else if BATypes.Date.isEqual(type: type)
                {
                    if let date = value(forKey: key)
                    {
                        return (date as! Date).iso8601
                    }
                }
                else if BATypes.String.isEqual(type: type)
                {
                    return value(forKey: key)
                }
                else //Pointer
                {
                    if (value(forKey: key) as? BAObject) != nil
                    {
                        return value(forKey: key)
                    }
                }
            }
            
            return value(forKey: key)
        }
        set(value) {
            
            setValue(value, forKey: key)
        }
    }
    
    open override func setValue(_ value: Any?, forUndefinedKey key: String) {
        self[key] = value
    }
    
    open override func value(forUndefinedKey key: String) -> Any? {
        return self[key]
    }
}


// MARK: - BARequestProtocol
extension BAObject
{
    func prepareResultGeneric<T>(type:T.Type ,result: BAResult<Data?>, query:BAQuery? = nil, rType: HTTPMethod, completion: @escaping (BAResult<T>) -> Void) where T:BAObject
    {
        switch (result)
        {
        case .Success(_):
            
            //to get JSON return value
            self.convertResponseObject(result.value!, query: query, rType:rType, completion: {
                
                //Remove last State
                if rType == .post || rType == .put
                {
                    self.removeState()
                }
                
                completion(BAResult.Success(self as! T))
            })
            
        case .Failure(_, _):
            
            //Prepare failed State
            if rType == .post || rType == .put
            {
                self.failedState()
            }
            
            //to get Error
            completion(BAResult.Failure((result.error?.message)!, result.error?.code))
        }
    }
    
    // MARK: - Base Request
    static func request(
        _ url:URLConvertible,
        method:HTTPMethod = .get,
        parameters: [String : Any]? = nil,
        filters: BAQuery? = nil,
        completion: @escaping (_ res: BAResult<Data?>) -> Void)
    {
        BackApp.addOperation(BackApp.cloudQueue) {
            
            let request = Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: BackApp.getRequestHeaders(with: filters)).validate()
            
            // semaphore with count equal to zero is useful for synchronizing completion of work, in our case the renewal of auth token
            let sema = DispatchSemaphore(value: 0);
            
            request.response { response in
                
                let result = BackApp.checkResponseResult(res: response)
                
                completion(result)
                
                // Signal that we are done
                sema.signal();
            }
            
            // Now we wait until the response block will send send a signal
            _ = sema.wait(timeout: DispatchTime.distantFuture)
        }
    }
    
    // MARK: - Convert Response
    func convertResponseObject(_ res: Data?, query:BAQuery? = nil, rType: HTTPMethod, completion: @escaping () -> Void)
    {
        //to get JSON return value
        if let result = res
        {
            let jsonObj = JSON(result)
            
            switch (jsonObj.type)
            {
            case Type.array: break
                //Do Something
                
            case Type.dictionary:
                
                //Keep In Mind
                let type = Swift.type(of:self)
                if self.isSyncProcess()
                {
                    let obj = type.instance(objType: type, _id: self._id, local_id: self.local_id)
                    if !obj.isNewObject
                    {
                        obj.LocalUsedProperties[k.DefaultKeys.Obj_Sync] = true
                        type.startFillProcess(of: obj, dictonaryObject: jsonObj.dictionaryObject!, query: query, rType: rType)
                        self.ignoreObserving{
                            self._id = obj._id
                        }
                        obj.FieldsToUpdate = self.FieldsToUpdate
                    }
                    else
                    {
                        type.startFillProcess(of: self, dictonaryObject: jsonObj.dictionaryObject!, query: query, rType: rType)
                    }
                }
                else
                {
                    type.startFillProcess(of: self, dictonaryObject: jsonObj.dictionaryObject!, query: query, rType: rType)
                }
                
                
                
            default:break
            }
        }
        
        completion()
    }
    
    class func startFillProcess<T>(of object: T, dictonaryObject: [String : Any], query: BAQuery? = nil, rType: HTTPMethod) where T: BAObject
    {
        object.fillAll(dictonaryObject: dictonaryObject,query: query, rType: rType)
        
        //Keep In Mind
        let type = Swift.type(of:object)
        type.keepInMind(obj: object)
        
        //if update on CurrentUser//CurrentINstalations
        if object is BAUser
        {
            if (object as! BAUser).isMe
            {
                BAUser.setCurrentUser(user: object as! BAUser)
            }
        }
        else if object is BAInstallation
        {
            if object._id == BAInstallation.currentInstallation()._id
            {
                BAInstallation.setCurrentInstallation(installation: object)
            }
        }
    }
    
    // MARK: - Request Costructors
    func getRequestURL() -> String
    {
        let collectionName = type(of:self).mapName()
        let url = BARequestType.URL(of: collectionName)
        
        return url
    }
    
    func paramsToSend(_ isRelation: Bool = false) -> [String : Any]
    {
        // Set up params to send
        var param = [String:Any?]()
        
        // If the object was saved as Eventually before
        // It should also has FieldsToUpdate local saved properties
        // Get all possible fields that need to update
        var properties:Set<String>!
        if isRelation
        {
            if !self.isCloudObject
            {
                properties = Set(propertyNames(includeLocals: false, includeFunctions: false))
                
                if !self.isLocalObject
                {
                    properties.remove(k.DefaultKeys.ServerUniqueID)
                }
                
                properties.remove(k.DefaultKeys.updatedAt)
                properties.remove(k.DefaultKeys.createdAt)
            }
            else
            {
                properties = Set(arrayLiteral: k.DefaultKeys.ServerUniqueID)
            }
        }
        else
        {
            if self.isLocalObject
            {
                properties = Set(propertyNames(includeLocals: false, includeFunctions: false))
                properties.remove(k.DefaultKeys.ServerUniqueID)
                let valuedProps = properties.filter({ (prop) -> Bool in
                    if self[prop] != nil
                    {
                        return true
                    }
                    
                    return false
                })
                
                properties = Set(valuedProps)
            }
            else
            {
                if self.isNewObject
                {
                    properties = Set(propertyNames(includeLocals: false, includeFunctions: false))
                    properties = properties.union(self.lastStateChanges())
                    
                    properties.remove(k.DefaultKeys.ServerUniqueID)
                    properties.remove(k.DefaultKeys.updatedAt)
                    properties.remove(k.DefaultKeys.createdAt)
                }
                else
                {
                    properties = self.lastStateChanges()
                    if self.FieldsToUpdate.isEmpty == false
                    {
                        let oldUpdatedFields = self.FieldsToUpdate.components(separatedBy: ",")
                        properties = properties.union(oldUpdatedFields)
                    }
                }
            }
        }
        
        print(properties)
        
        let ignoredProps = type(of:self).ignoredPropertiesCloudSend()
        
        for propery in properties
        {
            if  ignoredProps.contains(propery)
            {
                continue
            }
            
            if let propType = self.getTypeOfProperty(name: propery)
            {
                if BATypes.BARelation.isEqual(type: propType)
                {
                    let ba_relation = self.value(forKey: propery) as! BARelation
                    let addedObjs:[BAObject]!   // = ba_relation.added
                    let removedObjs:[BAObject]! // = ba_relation.removed
                    
                    let state = self.stateObjects.last
                    let relationState = self.relationState(propery)
                    
                    if state?.type == .Eventually
                    {
                        let (addedRels, removedRels) = selectOnlyLocalRelations(typeB: ba_relation.storedType, relationName: propery)
                        
                        addedObjs = addedRels.flatMap({ (rel) -> BAObject? in
                            
                            let obj = ba_relation.stored[rel] as! BAObject
                            if !relationState.added.contains(rel)
                            {
                                return obj
                            }
                            
                            return nil
                        })
                        
                        //remove from relation
                        removedObjs = removedRels.flatMap({ (rel) -> BAObject? in
                            
                            let obj = BAObject()
                            obj._id = rel
                            
                            if relationState.deleted.contains(rel)
                            {
                                return obj
                            }
                            
                            return nil
                        })
                    }
                    else
                    {
                        addedObjs = ba_relation.added.filter({ (obj) -> Bool in
                            if relationState.added.contains(obj._id)
                            {
                                return false
                            }
                            
                            return true
                        })
                        removedObjs = ba_relation.removed.filter({ (obj) -> Bool in
                            if relationState.deleted.contains(obj._id)
                            {
                                return false
                            }
                            
                            return true
                        })
                    }
                    
                    //Add relation State
                    let addObjectIDS = addedObjs.flatMap { (obj)->String? in return obj._id }
                    let removedObjectIDS = removedObjs.flatMap { (obj)->String? in return obj._id }
                    state?.addRelationState(propery, added: addObjectIDS, deleted: removedObjectIDS)
                    
                    var addedArr = Array<Any>()
                    if addedObjs.count > 0
                    {
                        for obj in addedObjs
                        {
                            addedArr.append(obj.paramsToSend(true))
                        }
                    }
                    
                    var removedArr = Array<Any>()
                    if removedObjs.count > 0
                    {
                        for obj in removedObjs
                        {
                            removedArr.append(obj.paramsToSend(true))
                        }
                    }
                    
                    //"fieldName": {"AddRelation": [{"title" : "New Book"}], "RemoveRelation": [{"_id":"58d4c17b27987e0c6c243cd6"}]}
                    
                    if addedArr.count > 0 || removedArr.count > 0
                    {
                        param[propery] = ["AddRelation" : addedArr, "RemoveRelation" : removedArr]
                    }
                }
                else
                {
                    if let val = self[propery]
                    {
                        if let obj = val as? BAObject
                        {
                            //Sync process, All objects will be created seperatly
                            if self.isSyncPointer(obj)
                            {
                                self.addNeedToSync([propery])
                            }
                            else
                            {
                                param[propery] = obj.paramsToSend(true)
                            }
                        }
                        else
                        {
                            param[propery] = val
                        }
                    }
                    else
                    {
                        param.updateValue(nil, forKey: propery)
                    }
                }
            }
        }
        
        return param as Any as! [String : Any]
    }
}

extension BackApp
{
    // MARK: - Request Costructors
    static func getRequestHeaders(with filters: BAQuery? = nil) -> [String : String]
    {
        var headers = [String : String]()
        headers["app_id"]   = BackApp.applicationID
        headers["app_key"]  = BackApp.applicationKey
        
        if (BAUser.token != nil)
        {
            headers["Authorization"] = BAUser.token!
        }
        
        if var mutFilter = filters
        {
            headers.combineWith(other: mutFilter.list)
        }
        
        return headers
    }
    
    static func checkResponseResult(res: DefaultDataResponse) -> BAResult<Data?>
    {
        //to get status code
        guard res.error == nil else {
            
            var errorMessage = "Something when wrong, please try again a bit later."
            
            if let data = res.data
            {
                let responseJSON = JSON(data: data)
                
                let message = responseJSON["Message"].stringValue
                if !message.isEmpty {
                    errorMessage = message
                }
            }
            
            return .Failure(errorMessage, res.response?.statusCode)
        }
        
        return .Success(res.data)
    }
}

// MARK: - Fill Object
extension BAObject
{
    func ignoreObserving(operation: @escaping ()->Void)
    {
        //Enable block observing
        LocalUsedProperties[k.DefaultKeys.Observing_Block] = true
        
        operation()
        
        //Disable block observing
        LocalUsedProperties.removeValue(forKey: k.DefaultKeys.Observing_Block)
    }
    
    func fillAll(dictonaryObject: [String : Any], query: BAQuery? = nil, rType: HTTPMethod)
    {
        //If relations also should be updated in localDB
        let isLocal = needToSaveLocaly()
        
        let isNew = self.isNewObject
        
        if let token = dictonaryObject["token"] as? String
        {
            BAUser.token = token
        }
        
        // Query can exists only in case of get/getAll
        // If not exists
        // Then get all default parameters, reduce 'dictonaryObject' keys, and set all remaining keys to nil
        // If exists
        // Then get all selected keys, reduce 'dictonaryObject' keys, and set all remaining keys to nil
        
        var selectedKeys:Set<String>!
        var myQuery = query
        if query == nil
        {
            myQuery = BAQuery(type(of: self))
        }
        
        if rType == .get
        {
            selectedKeys = myQuery?.getFullSelectedKeys()
        }
        else if rType == .put
        {
            selectedKeys = Set(dictonaryObject.keys.sorted())
        }
        else if rType == .post
        {
            selectedKeys = Set(self.propertyNames(includePointers: true, includeRelations: true, includeLocals: false, includeFunctions: false))
        }
        
        //Get this responce state
        let allChangedKeys = self.allChangedKeys(relations: false)
        
        selectedKeys.forEach({ (key) in
            
            if let type = getTypeOfProperty(name: key)
            {
                if !allChangedKeys.contains(key)
                {
                    self.ignoreObserving {
                        
                        if let value = dictonaryObject[key], !(value is NSNull)
                        {
                            if BATypes.Date.isEqual(type: type)
                            {
                                self[key] = (value as? String)?.dateFromISO8601
                            }
                            else if BATypes.BABool.isEqual(type: type)
                            {
                                if let val = value as? Bool
                                {
                                    let ba_bool: BABool?
                                    if let value = self.value(forKey: key)
                                    {
                                        ba_bool = (value as! BABool)
                                    }
                                    else
                                    {
                                        ba_bool = BABool()
                                        self[key] = ba_bool
                                    }
                                    
                                    ba_bool?.value = val
                                }
                            }
                            else if BATypes.BAFile.isEqual(type: type)
                            {
                                let ba_file: BAFile?
                                if let val = self.value(forKey: key)
                                {
                                    ba_file = (val as! BAFile)
                                }
                                else
                                {
                                    ba_file = BAFile()
                                    self[key] = ba_file
                                }
                                
                                ba_file?.updateURL((value as! String))
                            }
                            else if BATypes.BAFunction.isEqual(type: type)
                            {
                                let ba_func: BAFunction?
                                if let val = self.value(forKey: key)
                                {
                                    ba_func = (val as! BAFunction)
                                    
                                    if ba_func?.funcName == .Size
                                    {
                                        ba_func?.valueStr = "\(value)"
                                    }
                                    else if ba_func?.funcName == .Filter
                                    {
                                        if let val = (value as? Array<[String : Any]>), val.count > 0
                                        {
                                            if let nestedClassType = ba_func?.filteredType
                                            {
                                                for (_, element) in val.enumerated()
                                                {
                                                    let server_ID = element[k.DefaultKeys.ServerUniqueID] as! String
                                                    
                                                    let nestedClass = nestedClassType.instance(objType: nestedClassType, _id: server_ID, local_id: nil)
                                                    nestedClass.fillAll(dictonaryObject: element, rType: rType)
                                                    
                                                    //Keep In Mind
                                                    nestedClassType.keepInMind(obj: nestedClass)
                                                    ba_func?.obj = nestedClass
                                                }
                                            }
                                        }
                                    }
                                    else if ba_func?.funcName == .Sum ||
                                        ba_func?.funcName == .Min ||
                                        ba_func?.funcName == .Max
                                    {
                                        ba_func?.valueStr = "\(value)"
                                    }
                                }
                            }
                            else if BATypes.BANumber.isEqual(type: type)
                            {
                                if let val = value as? NSNumber
                                {
                                    let ba_num: BANumber?
                                    if let value = self.value(forKey: key)
                                    {
                                        ba_num = (value as! BANumber)
                                    }
                                    else
                                    {
                                        ba_num = BANumber()
                                        self[key] = ba_num
                                    }
                                    
                                    ba_num?.value = val.doubleValue
                                }
                            }
                            else if BATypes.BAInteger.isEqual(type: type)
                            {
                                if let val = value as? Int64
                                {
                                    let ba_int: BAInteger?
                                    if let value = self.value(forKey: key)
                                    {
                                        ba_int = (value as! BAInteger)
                                    }
                                    else
                                    {
                                        ba_int = BAInteger()
                                        self[key] = ba_int
                                    }
                                    
                                    ba_int?.value = val
                                }
                            }
                            else if BATypes.BAArray.isEqual(type: type)
                            {
                                if let val = (value as? Array<Any>)
                                {
                                    let ba_array: BAArray?
                                    if let value = self.value(forKey: key)
                                    {
                                        ba_array = (value as! BAArray)
                                    }
                                    else
                                    {
                                        ba_array = BAArray()
                                        self[key] = ba_array
                                    }
                                    
                                    ba_array?.data = Data.instance(dict: val)
                                }
                            }
                            else if BATypes.BADictionary.isEqual(type: type)
                            {
                                if let val = (value as? Dictionary<String, Any>)
                                {
                                    let ba_dict: BADictionary?
                                    if let value = self.value(forKey: key)
                                    {
                                        ba_dict = (value as! BADictionary)
                                    }
                                    else
                                    {
                                        ba_dict = BADictionary()
                                        self[key] = ba_dict
                                    }
                                    
                                    ba_dict?.data = Data.instance(dict: val)
                                }
                            }
                            else if BATypes.String.isEqual(type: type)
                            {
                                self[key] = (value as! String)
                            }
                            else if BATypes.BARelation.isEqual(type: type)
                            {
                                let ba_relation = self.value(forKey: key) as! BARelation
                                if let val = (value as? Array<[String : Any]>), val.count > 0
                                {
                                    //Get Type
                                    var nestedClassType:BAObject.Type!
                                    if let relType = ba_relation.storedType
                                    {
                                        nestedClassType = BAObject.getTypeObject(of: "\(relType)")
                                    }
                                    
                                    //if request is .GET, then all relation objects included
                                    self.cleanupRelationObjects(of: key, relation: ba_relation,includeLocal: isLocal)
                                    
                                    for (_, element) in val.enumerated()
                                    {
                                        var nestedClass = nestedClassType.init()
                                        
                                        //if new objects added
                                        if self.isEventuallyCall
                                        {
                                            //sort stored items by local id
                                            //if val._id is not exists in stored items, then it's was just local
                                            //order only local items with created order
                                            if (ba_relation.stored[(element[k.DefaultKeys.ServerUniqueID] as! String)] == nil)
                                            {
                                                var objs = ba_relation.objects as! [BAObject]
                                                objs = objs.filter{$0.isLocalObject}.sorted{($0.local_id?.cValue)! < ($1.local_id?.cValue)!}
                                                nestedClass = objs.first!
                                                ba_relation.stored.removeValue(forKey: nestedClass._id)
                                            }
                                        }
                                        else if ba_relation.added.count > 0
                                        {
                                            nestedClass = ba_relation.added.first!
                                            ba_relation.added.removeFirst()
                                        }
                                        else
                                        {
                                            let server_ID = element[k.DefaultKeys.ServerUniqueID] as! String
                                            nestedClass = nestedClassType.instance(objType: nestedClassType, _id: server_ID, local_id: nil)
                                        }
                                        
                                        nestedClass.fillAll(dictonaryObject: element, rType: rType)
                                        
                                        //Keep In Mind
                                        nestedClassType.keepInMind(obj: nestedClass)
                                        
                                        ba_relation.store(object: nestedClass)
                                        
                                        //Add Object in relation
                                        let relationAddSubscribe = "\(String(ObjectIdentifier(self).hashValue) + key + "RelationObjectAdded")"
                                        NotificationCenter.default.post(name: Notification.Name(relationAddSubscribe),
                                                                        object: nil,
                                                                        userInfo: [k.DefaultKeys.NotyKey : [nestedClass]])
                                    }
                                }
                                
                                // If request is .put, then not all realtion objects were included
                                if rType == .put
                                {
                                    self.refreshRelationObjects(of: key, relation: ba_relation, includeLocal: isLocal)
                                }
                            }
                            else
                            {
                                if let val = (value as? Dictionary<String , Any>), val.count > 0
                                {
                                    let nestedClassType = BAObject.getTypeObject(of: type.clearType())
                                    
                                    let server_ID = val[k.DefaultKeys.ServerUniqueID] as! String
                                    
                                    if rType == .get
                                    {
                                        let nestedClass = nestedClassType.instance(objType: nestedClassType, _id: server_ID, local_id: nil)
                                        nestedClass.fillAll(dictonaryObject: val, rType: rType)
                                        
                                        self[key] = nestedClass
                                        
                                        //Keep In Mind
                                        nestedClassType.keepInMind(obj: nestedClass)
                                    }
                                    else
                                    {
                                        if  ((self[key] as? BAObject) == nil)
                                        {
                                            //New
                                            let nestedClass = nestedClassType.init()
                                            self[key] = nestedClass
                                        }
                                        
                                        
                                        if var obj = self[key] as? BAObject
                                        {
                                            if obj.isNewObject || obj._id == server_ID
                                            {
                                                //Existing
                                                obj.fillAll(dictonaryObject: val, rType: rType)
                                            }
                                            else
                                            {
                                                let nestedClass = nestedClassType.instance(objType: nestedClassType, _id: server_ID, local_id: nil)
                                                self[key] = nestedClass
                                                obj = nestedClass
                                            }
                                            
                                            //Keep In Mind
                                            nestedClassType.keepInMind(obj: obj)
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if let val = self[key]
                            {
                                if BATypes.Date.isEqual(type: type)
                                {
                                    self[key] = nil
                                }
                                else if BATypes.BABool.isEqual(type: type)
                                {
                                    let ba_bool: BABool?
                                    if let value = self.value(forKey: key)
                                    {
                                        ba_bool = (value as! BABool)
                                        ba_bool?.value = nil
                                    }
                                }
                                else if BATypes.BAFile.isEqual(type: type)
                                {
                                    let ba_file: BAFile?
                                    if let val = self.value(forKey: key)
                                    {
                                        ba_file = (val as! BAFile)
                                        ba_file?.url = nil
                                    }
                                }
                                else if BATypes.BAFunction.isEqual(type: type)
                                {
                                    let ba_func: BAFunction?
                                    if let val = self.value(forKey: key)
                                    {
                                        ba_func = (val as! BAFunction)
                                        ba_func?.valueStr = nil
                                        ba_func?.obj = nil
                                    }
                                }
                                else if BATypes.BANumber.isEqual(type: type)
                                {
                                    let ba_num: BANumber?
                                    if let value = self.value(forKey: key)
                                    {
                                        ba_num = (value as! BANumber)
                                        ba_num?.value = nil
                                    }
                                }
                                else if BATypes.BAInteger.isEqual(type: type)
                                {
                                    let ba_int: BAInteger?
                                    if let value = self.value(forKey: key)
                                    {
                                        ba_int = (value as! BAInteger)
                                        ba_int?.value = nil
                                    }
                                }
                                else if BATypes.BAArray.isEqual(type: type)
                                {
                                    let ba_array: BAArray?
                                    if let value = self.value(forKey: key)
                                    {
                                        ba_array = (value as! BAArray)
                                        ba_array?.data = nil
                                    }
                                }
                                else if BATypes.BADictionary.isEqual(type: type)
                                {
                                    let ba_dict: BADictionary?
                                    if let value = self.value(forKey: key)
                                    {
                                        ba_dict = (value as! BADictionary)
                                        ba_dict?.data = nil
                                    }
                                }
                                else if BATypes.String.isEqual(type: type)
                                {
                                    self[key] = nil
                                }
                                else if BATypes.BARelation.isEqual(type: type)
                                {
                                    //Delete removed relations throug web admin
                                    let ba_relation = self.value(forKey: key) as! BARelation
                                    self.cleanupRelationObjects(of: key, relation: ba_relation, includeLocal: isLocal)
                                }
                                else
                                {
                                    if let obj = val as? BAObject
                                    {
                                        //Sync process, All objects will be ceated seperatly
                                        if !self.isSyncPointer(obj)
                                        {
                                            self[key] = nil
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })
        
        if (!isNew)
        {
            notify()
        }
    }
    
    class func getTypeObject(of property: String) -> BAObject.Type
    {
        let fullType = "\(Bundle.main.infoDictionary!["CFBundleExecutable"] as! String).\(property)"
        let nestedClassType = NSClassFromString(fullType) as! BAObject.Type
        
        return nestedClassType
    }
    
    func needToSaveLocaly() -> Bool
    {
        var isLocal = false
        if let loc = LocalUsedProperties[k.DefaultKeys.Server_Local] as? Bool
        {
            isLocal = loc
        }
        else
        {
            if self is BAUser
            {
                if (self as! BAUser).isMe
                {
                    isLocal = true
                    LocalUsedProperties[k.DefaultKeys.Server_Local] = true
                }
            }
            else if self is BAInstallation
            {
                if self._id == BAInstallation.currentInstallation()._id
                {
                    isLocal = true
                    LocalUsedProperties[k.DefaultKeys.Server_Local] = true
                }
            }
            else if self.isEventuallyCall
            {
                isLocal = true
                LocalUsedProperties[k.DefaultKeys.Server_Local] = true
            }
        }
        
        return isLocal
    }
}

// MARK: - Relation Funcions
extension BAObject
{
    func getPointersAndRelations() -> (pointers: [String],relations: [String])
    {
        // Try to get from Globals
        let SELF = type(of:self)
        let className = String(describing: SELF)
        if let pointersArray = BAObject.GlobalInfo["\(SELF.mapName()).\(className).pointers"]
        {
            return (pointersArray as! [String], BAObject.GlobalInfo["\(SELF.mapName()).\(className).relations"] as! [String])
        }
        
        var pointers = [String]()
        var relations = [String]()
        for prop in propertyNames()
        {
            if let type = getTypeOfProperty(name: prop)
            {
                if  !BATypes.BAArray.isEqual(type: type) &&
                    !BATypes.BARelation.isEqual(type: type) &&
                    !BATypes.BABool.isEqual(type: type) &&
                    !BATypes.BANumber.isEqual(type: type) &&
                    !BATypes.BAInteger.isEqual(type: type) &&
                    !BATypes.BADictionary.isEqual(type: type) &&
                    !BATypes.String.isEqual(type: type) &&
                    !BATypes.Date.isEqual(type: type) &&
                    !BATypes.BAFile.isEqual(type: type) &&
                    !BATypes.BAFunction.isEqual(type: type)
                {
                    pointers.append(prop)
                }
                else if BATypes.BARelation.isEqual(type: type)
                {
                    relations.append(prop)
                }
            }
        }
        
        // Set Globals
        
        BAObject.GlobalInfo["\(SELF.mapName()).\(className).pointers"] = pointers
        BAObject.GlobalInfo["\(SELF.mapName()).\(className).relations"] = relations
        
        return (pointers,relations)
    }
    
    class func getRelationName(of type:BAObject.Type) -> String
    {
        var arr = [self.mapName(),type.mapName()]
        arr.sort { $0 < $1 }
        
        let relationTableName = "\(arr[0])_\(arr[1])"
        
        return relationTableName
    }
    
    class func getRelationColumnNames(of type:BAObject.Type) -> (selfCol: String,relCol: String)
    {
        let firstCollection = self.mapName()
        let secondCollection = type.mapName()
        
        if firstCollection == secondCollection
        {
            return ("A_\(firstCollection)_id", "B_\(secondCollection)_id")
        }
        else
        {
            return ("\(firstCollection)_id", "\(secondCollection)_id")
        }
    }
    
    func cleanupRelationObjects(of relationName: String, relation: BARelation, includeLocal: Bool)
    {
        if relation.removed.count == 0 ,
            relation.added.count == 0,
            !self.isEventuallyCall
        {
            //cleanup
            relation.stored.removeAll()
            
            if includeLocal
            {
                removeAllRelations(of: relationName, relation: relation)
            }
        }
    }
    
    func refreshRelationObjects(of relationName: String, relation: BARelation, includeLocal: Bool)
    {
        let state = self.stateObjects.first
        let relationState = state?.getRelationState(relationName)
        
        if let deleted = relationState?.deleted, deleted.count > 0
        {
            let isEventuallyCall = self.isEventuallyCall
            if !isEventuallyCall
            {
                if includeLocal
                {
                    removeRelations(of: relationName, relation: relation, deletedObjects: deleted)
                }
                
                //Notification about remove relation objects
                let relationDeleteSubscribe = "\(String(ObjectIdentifier(self).hashValue) + relationName + "RelationObjectRemoved")"
                NotificationCenter.default.post(name: Notification.Name(relationDeleteSubscribe),
                                                object: nil,
                                                userInfo: [k.DefaultKeys.NotyKey : relation.removed])
                
                //Remove deleted relation objects from Object
                relation.removeDeletedKeys(deleted)
            }
        }
    }
}

// MARK: - Property Functions
extension BAObject
{
    static func propertyNames(includePointers: Bool = true,
                              includeRelations: Bool = true,
                              includeLocals: Bool = true,
                              includeFunctions: Bool = true) -> [String]
    {
        return self.init().propertyNames(includePointers:includePointers,
                                         includeRelations: includeRelations,
                                         includeLocals: includeLocals,
                                         includeFunctions: includeFunctions)
    }
    
    func propertyNames(includePointers: Bool = true,
                       includeRelations: Bool = true,
                       includeLocals: Bool = true,
                       includeFunctions: Bool = true) -> [String] {
        
        // Try to get from Globals
        let SELF = type(of:self)
        let className = String(describing: SELF)
        if let props_Global = BAObject.GlobalInfo["\(SELF.mapName()).\(className).propertyNames:\(includePointers),\(includeRelations),\(includeLocals),\(includeFunctions)"]
        {
            return props_Global as! [String]
        }
        
        
        var propNames = [String]()
        
        var isExist = true
        var mirror = Mirror(reflecting: self)
        
        
        repeat
        {
            propNames.insert(contentsOf: mirror.children.flatMap { $0.label }, at: 0)
            
            if mirror.superclassMirror != nil
            {
                mirror = mirror.superclassMirror!
            }
            else
            {
                isExist = false
            }
            
        } while isExist
        
        
        //propNames = Array(Set(propNames).subtracting(SELF.fullIgnoredProperties()))
        for ignore in SELF.fullIgnoredProperties()
        {
            if let index = propNames.index(of: ignore)
            {
                propNames.remove(at: index)
            }
        }

        
        if includeRelations == false
        {
            let relProperties = getPointersAndRelations().relations
            for relProp in relProperties
            {
                if let index = propNames.index(of: relProp)
                {
                    propNames.remove(at: index)
                }
            }
        }
        
        if includePointers == false
        {
            let relProperties = getPointersAndRelations().pointers
            for relProp in relProperties
            {
                if let index = propNames.index(of: relProp)
                {
                    propNames.remove(at: index)
                }
            }
        }
        
        if includeLocals == false
        {
            for locProp in SELF.onlyLocalProperties()
            {
                if let index = propNames.index(of: locProp)
                {
                    propNames.remove(at: index)
                }
            }
        }
        
        if includeFunctions == false
        {
            // Remove Funcion keys
            // Dont need to create columns for Funcions
            for key in self.getKeys(of: .BAFunction)
            {
                if let index = propNames.index(of: key)
                {
                    propNames.remove(at: index)
                }
            }
        }
        
        // Set Globals
        BAObject.GlobalInfo["\(SELF.mapName()).\(className).propertyNames:\(includePointers),\(includeRelations),\(includeLocals),\(includeFunctions)"] = propNames
        
        return propNames
    }
    
    // Returns the property type
    func getTypeOfProperty (name: String) -> String? {
        
        // Try to get from Globals
        let SELF = Swift.type(of:self)
        let className = String(describing: SELF)
        if let typeOfProperty = BAObject.GlobalInfo["\(SELF.mapName()).\(className).getTypeOfProperty:\(name)"]
        {
            return (typeOfProperty as! String)
        }
        
        ///////////////// MAIN FUNC /////////////////
        var type: Mirror = Mirror(reflecting: self)
        
        var typeName = ""
        for child in type.children {
            if child.label! == name {
                typeName = String(describing: Swift.type(of: child.value))
                break
            }
        }
        
        if typeName.isEmpty
        {
            while let parent = type.superclassMirror {
                for child in parent.children {
                    if child.label! == name {
                        typeName = String(describing: Swift.type(of: child.value))
                        break
                    }
                }
                type = parent
            }
        }
        
        ///////////////// MAIN FUNC /////////////////
        
        if typeName.isEmpty
        {
            return nil
        }
        
        
        // Set Globals
        BAObject.GlobalInfo["\(SELF.mapName()).\(className).getTypeOfProperty:\(name)"] = typeName
        
        return typeName
    }
}

// MARK: - Helpers
extension BAObject
{
    var isLocalObject:Bool {
        
        return self._id.contains(k.DefaultKeys.local_prefix)
    }
    
    var isNewObject:Bool {
        
        return self._id.contains(k.DefaultKeys.temp_prefix)
    }
    
    var isCloudObject:Bool {
        
        return (!self.isNewObject && !self.isLocalObject)
    }
    
    var isSync:Bool{
        
        return self.ba_status == BAObjectStatus.Sync.rawValue
    }
}

extension BackApp
{
    static func isReachableConnection() -> BAResult<Bool>
    {
        if !BackApp.sharedInstance.reachability.isReachable
        {
            return (BAResult.Failure(k.DefaultKeys.Messages.NoInternet, 4))
        }
        else
        {
            return (BAResult.Success(true))
        }
    }
    
    static func addOperation(_ queue: DispatchQueue, job: @escaping ()->Void)
    {
        queue.async { 
            job()
        }
    }
}
