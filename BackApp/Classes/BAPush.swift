//
//  BAPush.swift
//  Pods
//
//  Created by GeghamHarutyunyan on 6/21/17.
//
//

import Foundation

struct APS
{
    //{"aps":{"alert":"Parioooor!!!","badge":increment,"sound":"default"}}
    var message: String?
    
    mutating func setMessage(_ message: String) -> APS
    {
        self.message = message
        
        return self
    }
    
    func getNotificationObject() -> [String : Any]
    {
        return ["aps" : ["alert":self.message,"badge":"increment","sound":"default"]]
    }
}


public final class BAPush: NSObject
{
    private var query: BAQuery?
    private var aps = APS()
    
    public class func push() -> BAPush
    {
        return BAPush()
    }
    
    //MARK: - Configuring a Push Notification
    
    
    /// Sets an installation query to which this push notification will be sent
    ///
    /// The query should be created via BAInstallation.query
    ///
    /// - parameter query: - The installation query to set for this push.
    public func setQuery(_ query: BAQuery) -> BAPush
    {
        self.query = query
        return self
    }
    
    /// Sets an alert message for this push notification.
    ///
    /// - parameter message: - The message to send in this push.
    public func setMessage(_ message: String) -> BAPush
    {
        _ = self.aps.setMessage(message)
        return self
    }
    
    ///  Send a password reset request for a specified email.
    ///  If a user account exists with that email, an email will be sent to that address with instructions on how to reset their password.
    ///
    ///  - parameter email: - Email of the account to send a reset password request.
    public func sendPush(completion: @escaping (BAResult<Bool>) -> Void) -> Void
    {
        //Connection chack
        let connectionResult = BackApp.isReachableConnection()
        if connectionResult.isFailure
        {
            completion(connectionResult)
            return
        }
        
        
        guard let query = self.query else {
            
            self.query = BAInstallation.query()
            return
        }
        
        let params = self.aps.getNotificationObject()
        BAObject.request(BARequestType.URL(action: .SendPush), method: .post,parameters: params,filters: query) { result in
            
            switch (result)
            {
            case .Success(_):
                
                //to get JSON return value
                completion(BAResult.Success(true))
                
            case .Failure(_, _):
                
                //to get Error
                completion(BAResult.Failure((result.error?.message)!, result.error?.code))
            }
        }
    }
}
