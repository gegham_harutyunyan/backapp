//
//  BAUser.swift
//  BackApp
//
//  Created by GeghamHarutyunyan on 1/12/17.
//  Copyright © 2017 WAY4APP. All rights reserved.
//
import Foundation

@objc
open class BAUser: BAObject
{
    // MARK: - Properties
    static var    token: String?{
        get {
            if let tok = UserDefaults.standard.object(forKey: "token")
            {
                return (tok as! String)
            }
            
            return nil
        }
        
        set {
            if newValue != nil
            {
                UserDefaults.standard.set(newValue, forKey: "token")
            }
        }
    }
    
    public static var currentUserID: String?{
        if let user_id = UserDefaults.standard.object(forKey: k.DefaultKeys.currentUser_id) as? String
        {
            return user_id
        }
        
        return nil
    }
    
    static var currentUserName: String?{
        if let user_name = UserDefaults.standard.object(forKey: k.DefaultKeys.currentUser_Name) as? String
        {
            return user_name
        }
        
        return nil
    }
    
    var isAnonymous:Bool {
        
        if self.username!.contains(k.DefaultKeys.anonymous_suffix)
        {
            return true
        }
        
        return false
    }
    
    public var isMe:Bool{
        if self.username == BAUser.currentUserName ||
            self._id == BAUser.currentUserID
        {
            return true
        }
        
        return false
    }
    
    static var isCurrentUser_Local:Bool {
        
        if let uID = BAUser.currentUserID
        {
            if uID.contains(k.DefaultKeys.local_prefix)
            {
                return true
            }
        }
        
        return false
    }
    
    @objc public dynamic var username        :String? = nil
    @objc public dynamic var password        :String? = nil
    @objc public dynamic var email           :String? = nil
    @objc public dynamic var emailVerified   :BABool = BABool()
    @objc public dynamic var authData        :String?
    
    // MARK: - Base
    open override class func mapName() -> String
    {
        return "User"
    }
    
    public class func isAuthorized() -> Bool
    {
        return (BAUser.currentUserID != nil)
    }
}

// MARK: - Action List

protocol UserActions
{
    static func registration<T>(newUser: T ,completion: @escaping (BAResult<T>) -> Void) -> Void where T:BAUser
    static func login<T>(user: T, completion: @escaping (BAResult<T>) -> Void) -> Void where T:BAUser
    
}

extension BackApp: UserActions
{
    static public func registration<T>(newUser: T ,completion: @escaping (BAResult<T>) -> Void) -> Void where T:BAUser
    {
        T.action(user: newUser, actionType: BAActions.Register) { (finalResult) in
            
            switch (finalResult)
            {
            case .Success(_):
                
                BAUser.setCurrentUser(user: newUser)
                
            default: break
            }
            
            completion(finalResult)
        }
    }
    
    static public func login<T>(user: T, completion: @escaping (BAResult<T>) -> Void) -> Void where T:BAUser
    {
        T.action(user: user, actionType: BAActions.Login) { (finalResult) in
            
            switch (finalResult)
            {
            case .Success(_):
                
                BAUser.setCurrentUser(user: user)
                
            default: break
            }
            
            completion(finalResult)
        }
    }

    ///  Send a password reset request for a specified email. 
    ///  If a user account exists with that email, an email will be sent to that address with instructions on how to reset their password.
    ///
    ///  - parameter email: - Email of the account to send a reset password request.
    static public func resetPassword(email: String, completion: @escaping (BAResult<Bool>) -> Void) -> Void
    {
        //Connection chack
        let connectionResult = BackApp.isReachableConnection()
        if connectionResult.isFailure
        {
            completion(BAResult.Failure((connectionResult.error?.message)!, connectionResult.error?.code))
            return
        }
        
        BackApp.addOperation(BackApp.operationsQueue)
        {
            BAObject.request(BARequestType.URL(action: .ResetPassword), method: .post, parameters: ["email":email]) { result in
                
                switch (result)
                {
                case .Success(_):
                    
                    //to get JSON return value
                    completion(BAResult.Success(true))
                    
                case .Failure(_, _):
                    
                    //to get Error
                    completion(BAResult.Failure((result.error?.message)!, result.error?.code))
                }
            }
        }
    }

    public static func logOut()
    {
        BAUser.removeUser()
        BAInstallation.removeInstallation()
    }
}


// MARK: - Action
public extension BAUser
{
    // MARK: - Constructor
    fileprivate static func action<T>(user:T, actionType: BAActions, completion: @escaping (BAResult<T>) -> Void) where T:BAUser
    {
        //Connection chack
        let connectionResult = BackApp.isReachableConnection()
        if connectionResult.isFailure
        {
            completion(BAResult.Failure((connectionResult.error?.message)!, connectionResult.error?.code))
            return
        }
        
        //Add new state
        let state = user.addNewState(.Action)
        
        var queue = BackApp.operationsQueue
        if user.isSyncProcess()
        {
            queue = BackApp.syncQueue
        }
        
        BackApp.addOperation(queue) {
            
            BAObject.request(user.actionURL(action: actionType), method: .post, parameters: state.encodedParams) { result in
                
                user.prepareResultGeneric(type: type(of:user), result: result, rType: .post, completion: { finalResult in
                    completion(finalResult)
                })
            }
        }
    }
    
    // MARK: - Constructor Elements
    
    private func actionURL(action: BAActions) -> String
    {
        return BARequestType.URL(action: action)
    }
}
