//
//  BATypes.swift
//  Pods
//
//  Created by GeghamHarutyunyan on 1/24/17.
//
//

import Foundation
import SwiftyJSON


enum BATypes: String
{
    case String
    case Date
    case BABool
    case BANumber
    case BAInteger
    case BAArray
    case BADictionary
    case BARelation
    case BAFile
    case BAFunction
    
    func isEqual(type: String) -> Bool
    {
        if type.contains(self.desc()) || type == self.rawValue
        {
            return true
        }
        
        return false
    }
    
    private func desc() -> String
    {
        return "<\(self.rawValue)>"
    }
}


/// BAFunction type properties should work only on server queries
@objc
public final class BAFunction: BAObservableType
{
    // size     : {$size:\"$x\"},
    // isMe     : {$in: [ \"58b42c95295e9c1130cc0935\", \"$x._id\" ] }}",
    // isMeObj  : {$filter: {input: "$authors", as: "item", cond: { $eq: [ "$$item.username", "sds" ] }}}
    // [{$unwind:{path: "$authors", preserveNullAndEmptyArrays: true}},{$group:{_id : {_id :"$_id",title:"$title"}, count: {$sum:"$authors.integerVal"}}},{$replaceRoot:{newRoot: {_id: "$_id._id", title:"$_id.title", count:"$count"}}}]
    
    // MARK: - GET Properties
    private var privatBoolVal:Bool?
    public var boolValue :Bool {
        get{
            if privatBoolVal != nil
            {
                return privatBoolVal!
            }
            
            if funcName == .Filter
            {
                return (obj != nil)
            }
            else
            {
                if let boolval = valueStr?.toBool()
                {
                    return boolval
                }
                
                return false
            }
        }
        set{
            privatBoolVal = newValue
        }
    }
    
    private var privatIntVal:Int?
    public var intValue :Int {
        get{
            if privatIntVal != nil
            {
                return privatIntVal!
            }
            
            if valueStr != nil
            {
                if let intVal = Int(valueStr!)
                {
                    return intVal
                }
            }
            
            return 0
        }
        set {
            privatIntVal = newValue
        }
    }
    
    var valueStr:String?{
        didSet{
            update?()
        }
    }
    internal(set) public var obj: Any?{
        didSet{
            update?()
        }
    }
    
    // MARK: - Funcion constructor properties
    
    //Global
    var shouldBeAuthorized = false
    var funcName : BAFunctionName?
    var funcType : BAFunctionType = .Standart
    var relationName = ""
    
    //Notification channel
    private var channel = ""
    var subscribeChannel:String {
        get{
            return channel
        }
        set{
            channel = newValue
            //Notifications
            
            //Delete
            if let type = filteredType
            {
                let deleteSubscribe = "\((type.mapName()) + "ObjectRemoved")"
                NotificationCenter.default.addObserver(self, selector: #selector(BAFunction.objectRemoved(notification:)), name: Notification.Name(deleteSubscribe), object: nil)
            }
            
            //Delete from the relation
            let relationDeleteSubscribe = "\(channel + relationName + "RelationObjectRemoved")"
            NotificationCenter.default.addObserver(self, selector: #selector(BAFunction.objectRemoved(notification:)), name: Notification.Name(relationDeleteSubscribe), object: nil)
            
            //Add Object in relation
            let relationAddSubscribe = "\(channel + relationName + "RelationObjectAdded")"
            NotificationCenter.default.addObserver(self, selector: #selector(BAFunction.objectAdded(notification:)), name: Notification.Name(relationAddSubscribe), object: nil)
        }
    }
    
    //Filter
    lazy var additionalConditions = [String : Any]()
    var filteredType:BAObject.Type?
    
    //Group
    var property = ""
    
    public required init(of name:BAFunctionName, relation: String)
    {
        super.init()
        
        self.funcName = name
        self.relationName = relation
        
        switch name {
            
        case .Sum, .Min, .Max:
            funcType = .Group
            
            
        default:
            funcType = .Standart
        }
    }
    
    // MARK: - Notifications
    
    @objc func objectAdded(notification: Notification)
    {
        if let objects = notification.userInfo?[k.DefaultKeys.NotyKey] as? [BAObject]
        {
            for obj_1 in objects
            {
                var passed = true
                innerLoop: for (key, value) in additionalConditions
                {
                    if (obj_1[key] as! String != value as! String)
                    {
                        passed = false
                        break innerLoop
                    }
                }
                
                if passed
                {
                    obj = obj_1
                    break
                }
            }
        }
    }
    
    @objc func objectRemoved(notification: Notification)
    {
        if let objects = notification.userInfo?[k.DefaultKeys.NotyKey] as? [BAObject], obj != nil
        {
            for obj_1 in objects
            {
                if obj_1._id == (obj as! BAObject)._id
                {
                    obj = nil
                    break;
                }
            }
        }
    }
    
    // MARK: - FILTER
    
    public func addFilter(add property: String, value: Any?) -> BAFunction
    {
        additionalConditions[property] = value
        
        return self
    }
    
    public func returnType<T>(type: T.Type?) -> BAFunction
    {
        if let ty = type as? BAObject.Type
        {
            self.filteredType = ty
        }
        
        return self
    }
    
    // MARK: - GROUP
    
    public func by(property: String) -> BAFunction
    {
        self.property = property
        return self
    }
    
    // MARK: - Autorized
    
    public func authorized() -> BAFunction
    {
        shouldBeAuthorized = true
        return self
    }
}

public class BAObservableType: NSObject
{
    var update: (()->Void)?
}

// MARK: - BABool
// MARK: -

@objc
public final class BABool: BAObservableType
{
    /// The value the optional represents.
    public var value:Bool?{
        didSet{
            update?()
        }
    }
    
    public var cValue: Bool{
        
        get{
            if value != nil
            {
                return value!
            }
            
            return false
        }
        
        set{
            value = newValue
        }
    }
    
    public init(_ val:Bool? = nil)
    {
        super.init()
        
        self.value = val
    }
}

// MARK: - BAInteger
// MARK: -
@objc
public final class BAInteger: BAObservableType
{
    /// The value the optional represents.
    public var value:Int64?{
        didSet{
            update?()
        }
    }
    
    public var cValue: Int64{
        
        get{
            if value != nil
            {
                return value!
            }
            
            return 0
        }
        
        set{
            value = newValue
        }
    }
    
    public init(_ val:Int64? = nil)
    {
        super.init()
        
        self.value = val
    }
    
    var mongoOperationExist = false
    var incrementVal:Int = 0
    public func increment(by value: Int = 1)
    {
        mongoOperationExist = true
        incrementVal += value
    }
}

// MARK: - BANumber
// MARK: -
@objc
public final class BANumber: BAObservableType
{
    /// The value the optional represents.
    
    public var value:Double?{
        didSet{
            update?()
        }
    }
    
    public var cValue: Double{
        
        get{
            if value != nil
            {
                return value!
            }
            
            return 0.0
        }
        
        set{
            value = newValue
        }
    }
    
    public init(_ val:Double? = nil)
    {
        super.init()
        
        self.value = val
    }
}

// MARK: - BAArray
// MARK: -
@objc
public final class BAArray: BAObservableType
{
    var data:Data?{
        didSet{
            update?()
        }
    }
    
    public var value:Array<Any>? {
        get{
            return data?.arrayValue()
        }
        set{
            if newValue != nil
            {
                data = Data.instance(dict: newValue!)
            }
        }
    }
}


// MARK: - BADictionary
// MARK: -
@objc
public final class BADictionary: BAObservableType
{
    var data:Data?{
        
        didSet{
            update?()
        }
    }
    
    public var value:Dictionary<String,Any>? {
        get{
            return data?.dictValue()
        }
        set{
            data = Data.instance(dict: newValue!)
        }
    }
}


// MARK: - BARelation
// MARK: -

enum RelationStatus:String {
    case Stored
    case Add
    case Remove
}


@objc
public final class BARelation: BAObservableType
{
    var storedType:BAObject.Type!
    
    var stored = [String : AnyObject]()
    
    var added = [BAObject](){
        didSet{
            if added.count > 0
            {
                update?()
            }
        }
    }
    
    var removed = [BAObject](){
        didSet{
            if removed.count > 0
            {
                update?()
            }
        }
    }
    
    public var objects:[AnyObject]?{
        get{
            let arr = stored.values.reversed()
            
            if arr.count > 0
            {
                return arr
            }
            
            return nil
        }
    }
    
    required public init<T : BAObject>(_ val:T.Type)
    {
        super.init()
        storedType = val
        
        //Notifications
        let notificationSubscribe = "\(val.mapName() + "ObjectRemoved")"
        NotificationCenter.default.addObserver(self, selector: #selector(BARelation.objectRemoved(notification:)), name: Notification.Name(notificationSubscribe), object: nil)
        
    }
    
    // MARK: - Notifications
    
    @objc func objectRemoved(notification: Notification)
    {
        if let objects = notification.userInfo?[k.DefaultKeys.NotyKey] as? [BAObject]
        {
            for obj in objects
            {
                stored.removeValue(forKey: obj._id)
                removed.remove(object: obj)
                added.remove(object: obj)
            }
        }
    }
    
    // MARK: - Store
    
    func store<T>(object: T) where T:BAObject
    {
        stored[object._id] = object
    }
    
    func removeDeletedKeys(_ deletedObjects: [String])
    {
        self.removed = self.removed.filter({ (object) -> Bool in
            
            if deletedObjects.contains(object._id)
            {
                self.stored.removeValue(forKey: object._id)
                return false
            }
            
            return true
        })
    }
    
    // MARK: - Add/Remove
    
    /// Add object into Relation
    public func add<T>(object: T) where T:BAObject
    {
        //If object already stored
        if stored[object._id] != nil
        {
            removed.remove(object: object)
            return
        }
        
        //If object is created in local/Cloud
        if object.isNewObject == false
        {
            removed.remove(object: object)
        }
        
        added.append(object)
    }
    
    /// Remove object from Relation
    public func remove<T>(object: T) where T:BAObject
    {
        //If object is created in local/Cloud
        if object.isNewObject == false
        {
            added.remove(object: object)
            removed.append(object)
        }
        else
        {
            added.remove(object: object)
        }
    }
}

// MARK: - Data
// MARK: -
extension Data
{
    static func instance(dict: Dictionary<String, Any>) ->Data
    {
        var dictionary = Data()
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
        dictionary = jsonData!
        
        return dictionary
    }
    
    static func instance(dict: Array<Any>) ->Data
    {
        var dictionary = Data()
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
        dictionary = jsonData!
        
        return dictionary
    }
    
    func dictValue() -> Dictionary<String, Any>?
    {
        return JSON(data: self).dictionaryObject
    }
    
    func arrayValue() -> Array<Any>?
    {
        return JSON(data: self).arrayObject
    }
    
}
