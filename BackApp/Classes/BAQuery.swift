//
//  BAFilter.swift
//  Pods
//
//  Created by GeghamHarutyunyan on 2/8/17.
//
//

import Foundation

enum Update: String
{
    case increment  = "$inc"
}

enum Filter: String
{
    case project    = "$project"
    case lookup     = "$lookup"
    case match      = "$match"
    case sort       = "$sort"
    case limit      = "$limit"
    case skip       = "$skip"
    case group      = "$group"
}

//Comparison Query Operators
enum Operators:String
{
    case equal              = "$eq"
    case notEqual           = "$ne"
    case exists             = "$exists"
    case lessThan           = "$lt"
    case greaterThan        = "$gt"
    case lessThanOrEqual    = "$lte"
    case greaterThanOrEqual = "$gte"
    case containedIn        = "$in"
    case notContainedIn     = "$nin"
    case containsAllObjects = "$all"
}

public enum BAFunctionType
{
    case Standart
    case Group
}

public enum BAFunctionName:String
{
    case Size               = "$size"
    case Filter             = "$filter"
    case Sum                = "$sum"
    case Max                = "$max"
    case Min                = "$min"
}

enum QueryMethod:String
{
    case KeyExist
    case KeyNotExist
    case KeyEqual
    case KeyNotEqual
    case KeyLessThan
    case KeyGreaterThan
    case KeyLessThanOrEqual
    case KeyGreaterThanOrEqual
    case KeyContainedIn
    case KeyNotContainedIn
    case KeyContainsAllObjects
    
    static func getMethods() -> [QueryMethod]
    {
        return [QueryMethod.KeyExist, QueryMethod.KeyNotExist, QueryMethod.KeyEqual, QueryMethod.KeyNotEqual,
                QueryMethod.KeyLessThan, QueryMethod.KeyGreaterThan, QueryMethod.KeyLessThanOrEqual, QueryMethod.KeyGreaterThanOrEqual,
                QueryMethod.KeyContainedIn, QueryMethod.KeyNotContainedIn, QueryMethod.KeyContainsAllObjects]
    }
}

enum SortMethod:String
{
    case KeyAscending
    case KeyAddAscending
    case KeyDescending
    case KeyAddDescending
}

struct KeyValueObj: CustomStringConvertible
{
    var key:String
    var value:String
    
    var description: String {
        return "{\(key) : \(value)}"
    }
}

public struct BAQuery
{
    fileprivate var _list = Array<KeyValueObj>()
    var list:[String:String]{
        mutating get{
            
            if isUpdate
            {
                addUpdateFields()
                
                return [k.DefaultKeys.headerUpdateQuery : _list.description]
            }
            else
            {
                //Lookup>Project>Mutch>Sort>Paging>Group
                updateProjectKeysValue()//2'nd
                updateFullMatchValue()
                addGroupFuncions()
                updateFullSortValue()
                updatePaginigs()
                
                return [k.DefaultKeys.headerQuery : _list.description]
            }
        }
    }
    
    var type:BAObject.Type!
    fileprivate lazy var allFilterHeaders = [String : [String : String]]()
    
    lazy var selectKeysHeaders = Set<String>()
    lazy var includeKeyHeaders = Set<String>()
    fileprivate lazy var includeKeyForcedHeaders = Set<String>()
    fileprivate lazy var sortKeyHeaders = [String : String]()
    fileprivate lazy var selectFuncsHeaders = (standart: Array<String>(), group: Array<String>(), ignore: Array<String>())
    fileprivate var orSubqueries:[BAQuery]?
    
    public var limit:Int?
    public var skip :Int?
    
    //update
    var isUpdate = false
    var object: Any?
    
    public init<T>(_ filterType: T.Type) where T:BAObject
    {
        type = filterType
    }
    
    static func defaultInit<T>(_ filterType: T.Type) -> BAQuery where T:BAObject
    {
        var filter = BAQuery(filterType)
        filter.includeAllFunctions()
        
        return filter
    }
    
    static func updateQuery<T>(_ object: T) -> BAQuery where T:BAObject
    {
        var filter = BAQuery(Swift.type(of: object))
        filter.object = object
        filter.isUpdate = true
        
        return filter
    }
    
    /// ------------------------------------------------------------------------------------------------------
    
    /// Fetching object's all Functions data with object
    mutating public func includeAllFunctions()
    {
        let instance = type.self.init()
        let funcKeys = instance.getKeys(of: .BAFunction)
        
        funcKeys.forEach { (key) in
            
            if let function =  instance.value(forKey: key) as? BAFunction
            {
                //If function should be executed only if User Authorized
                if (function.shouldBeAuthorized && !BAUser.isAuthorized())
                {
                    selectFuncsHeaders.ignore.append(key)
                }
                else if function.funcType == .Group
                {
                    includePrivate(key: function.relationName)
                    selectFuncsHeaders.group.append(key)
                }
                else if function.funcType == .Standart
                {
                    selectFuncsHeaders.standart.append(key)
                }
            }
        }
    }
    
    /// Fetching object's Functions data with object
    ///
    /// - parameter keys: - column name of Functions on the object, that need to fetch
    mutating public func includeFunctions(keys: String...)
    {
        let instance = type.self.init()
        for key in keys
        {
            if let function =  instance.value(forKey: key) as? BAFunction
            {
                //If function should be executed only if User Authorized
                if (function.shouldBeAuthorized && !BAUser.isAuthorized())
                {
                    selectFuncsHeaders.ignore.append(key)
                }
                else if function.funcType == .Standart
                {
                    selectFuncsHeaders.standart.append(key)
                }
                else
                {
                    includePrivate(key: function.relationName)
                    selectFuncsHeaders.group.append(key)
                }
            }
        }
    }
    
    /// ------------------------------------------------------------------------------------------------------
    
    /// Fetching object's Pointer/Relation data with object
    ///
    /// - parameter key: - column name of Pointer/Relation on the object
    mutating public func include(key: String)
    {
        includePrivate(key: key)
    }
    
    /// Fetching object's Pointers data with object
    mutating public func includePointers()
    {
        for key in type.self.init().getPointersAndRelations().pointers
        {
            includePrivate(key: key)
        }
    }
    
    /// Fetching object's Relations data with object
    mutating public func includeRelations()
    {
        for key in type.self.init().getPointersAndRelations().relations
        {
            includePrivate(key: key)
        }
    }
    
    mutating fileprivate func includePrivate(key: String, isForced: Bool = false)
    {
        if isForced
        {
            if includeKeyForcedHeaders.contains(key) { return }
            includeKeyForcedHeaders.insert(key)
        }
        else
        {
            if includeKeyHeaders.contains(key) { return }
            includeKeyHeaders.insert(key)
        }
        
        let instance = type.self.init()
        
        if let keyType = instance.getTypeOfProperty(name: key)
        {
            var nestedClassType:BAObject.Type? = nil
            
            //Relation
            if BATypes.BARelation.isEqual(type: keyType)
            {
                let ba_relation = instance.value(forKey: key) as! BARelation
                
                //Get Type
                if let relType = ba_relation.storedType
                {
                    nestedClassType = BAObject.getTypeObject(of: "\(relType)")
                }
            }
            else
            {
                //Pointer
                nestedClassType = BAObject.getTypeObject(of: keyType.clearType())
            }
            
            if let ty = nestedClassType
            {
                var prefix = ""
                if isForced
                {
                    prefix = "_F"
                }
                
                let includeKey = "{from: \"\(ty.mapName())\", localField: \"\(key)\",foreignField: \"\(k.DefaultKeys.ServerUniqueID)\",as: \"\(key)\(prefix)\"}"
                _list.insert(KeyValueObj(key: Filter.lookup.rawValue, value: includeKey), at: 0)
            }
        }
    }
    
    
    ///  Make the query restrict the fields of the returned BAObjects to include only the provided keys.
    ///
    ///  - parameter keys: - The keys to include in the result.
    public mutating func select(keys: String...)
    {
        selectKeysHeaders = selectKeysHeaders.union(Set(keys))
    }
    
    //MARK: - CONSTRUCTOR
    
    private mutating func updateProjectKeysValue()
    {
        let instance = type.self.init()
        
        // Selected keys
        selectKeysHeaders = getFullSelectedKeys()
        
        let pointer = instance.getPointersAndRelations().pointers
        var str = ""
        for key in selectKeysHeaders
        {
            if let function = instance.value(forKey: key) as? BAFunction
            {
                //Get relation name
                var relName = ""
                if includeKeyHeaders.contains(function.relationName)
                {
                    relName = function.relationName
                }
                else if includeKeyForcedHeaders.contains(function.relationName)
                {
                    relName = "\(function.relationName)_F"
                }
                else
                {
                    relName = "\(function.relationName)_F"
                    includePrivate(key: function.relationName, isForced: true)
                }
                
                if function.funcName == .Size
                {
                    // size:{"$size": { $ifNull: [ \"$myFieldArray\", [] ] }}
                    var targetArray = "\"$\(relName)\""
                    if function.additionalConditions.keys.count > 0
                    {
                        targetArray = cunstructorFilter(filters: function.additionalConditions, relName: relName)
                    }
                    str += "\(key):{\((function.funcName?.rawValue)!) : { $ifNull : [ \(targetArray) , [] ]}},"
                }
                else if function.funcName == .Filter
                {
                    // isMeObj  : {$filter: {input: "$authors", as: "item", cond: { $eq: [ "$$item.username", "sds" ] }}}
                    
                    let filterStr = cunstructorFilter(filters: function.additionalConditions, relName: relName)
                    str += "\(key):\(filterStr),"
                }
            }
            else
            {
                if pointer.contains(key)
                {
                    str += "\(key):{ $arrayElemAt: [\"$\(key)\", 0 ] },"
                }
                else
                {
                    str += "\(key):1,"
                }
            }
        }
        
        str.remove(at: str.index(before: str.endIndex))
        let fullProjectrValue = "{\(str)}"
        
        _list.append(KeyValueObj(key: Filter.project.rawValue, value: fullProjectrValue))
    }
    
    private func cunstructorFilter(filters: [String : Any], relName: String) -> String
    {
        var condStr = ""
        if filters.keys.count > 1
        {
            //$and: [ { $gte: [ "$$num", NumberLong("-9223372036854775807") ] }, { $lte: [ "$$num", NumberLong("9223372036854775807") ] } ]
            var subConds = ""
            for (filterKey, filterValue) in filters
            {
                if ((filterValue as? String) != nil)
                {
                    subConds += "{ \(Operators.equal.rawValue): [ \"$$item.\(filterKey)\", \"\(String(describing: filterValue))\" ] },"
                }
                else
                {
                    subConds += "{ \(Operators.equal.rawValue): [ \"$$item.\(filterKey)\", \(String(describing: filterValue)) ] },"
                }
                
            }
            subConds.remove(at: subConds.index(before: subConds.endIndex))
            
            condStr += "$and : [\(subConds)]"
            
            return "{\(BAFunctionName.Filter.rawValue) : {input: \"$\(relName)\", as: \"item\", cond: { \(condStr) }}}"
        }
        else if filters.keys.count == 1
        {
            let (prop, val) = filters.first!
            var valStr = ""
            if ((val as? String) != nil)
            {
                valStr = "\"\(String(describing: val))\""
            }
            else
            {
                valStr = "\(String(describing: val))"
            }
            return "{\(BAFunctionName.Filter.rawValue) : " +
            "{input: \"$\(relName)\", as: \"item\", cond: { \(Operators.equal.rawValue): [ \"$$item.\(prop)\", \(valStr) ] }}}"
        }
        else
        {
            return "{\(BAFunctionName.Filter.rawValue) : " +
            "{input: \"$\(relName)\", as: \"item\", cond: { \(Operators.equal.rawValue): [ \"$$item.\(k.DefaultKeys.ServerUniqueID)\", \"\(BAUser.currentUserID ?? "")\" ] }}}"
        }
    }
    
    private mutating func addGroupFuncions()
    {
        if (selectFuncsHeaders.group.count == 0) {return}
        
        let instance = type.self.init()
        
        //We can add keys in project if we are going to group by that key
        //So we need to remove those keys
        for key in selectFuncsHeaders.group
        {
            if let funcion = instance.value(forKey: key) as? BAFunction
            {
                if let index = selectKeysHeaders.index(of: funcion.relationName)
                {
                    selectKeysHeaders.remove(at: index)
                }
            }
        }
        
        
        var unwindList = Set<String>()
        var newKeys = ""
        var newRootKeys = ""
        for key in selectFuncsHeaders.group
        {
            if let funcion = instance.value(forKey: key) as? BAFunction
            {
                //Get relation name
                var relName = ""
                if includeKeyHeaders.contains(funcion.relationName)
                {
                    relName = funcion.relationName
                }
                else if includeKeyForcedHeaders.contains(funcion.relationName)
                {
                    relName = "\(funcion.relationName)_F"
                }
                else
                {
                    relName = "\(funcion.relationName)_F"
                    includePrivate(key: funcion.relationName, isForced: false)
                }
                
                if unwindList.contains(relName) == false
                {
                    unwindList.insert(relName)
                    _list.append(KeyValueObj(key: "$unwind", value: "{path: \"$\(relName)\", preserveNullAndEmptyArrays: true}"))
                }
                
                newKeys += "\(key) : {\((funcion.funcName?.rawValue)!) : \"$\(relName).\(funcion.property)\"},"
                newRootKeys += "\(key):\"$\(key)\","
            }
        }
        
        //[{$unwind:{path: "$authors", preserveNullAndEmptyArrays: true}},{$group:{_id : {_id :"$_id",title:"$title"}, count: {$sum:"$authors.integerVal"}}},{$replaceRoot:{newRoot: {_id: "$_id._id", title:"$_id.title", count:"$count"}}}]
        
        let oldRoot = selectKeysHeaders.map({ (prop) in
            return "\(prop) : \"$\(prop)\""
        })
        
        let newRoot = selectKeysHeaders.map({ (prop) in
            return "\(prop) : \"$_id.\(prop)\""
        })
        
        let oldRootString = oldRoot.joined(separator: ",")
        let newRootString = newRoot.joined(separator: ",")
        
        
        newKeys.remove(at: newKeys.index(before: newKeys.endIndex))
        newRootKeys.remove(at: newRootKeys.index(before: newRootKeys.endIndex))
        _list.append(KeyValueObj(key: "$group", value: "{_id : {\(oldRootString)}, \(newKeys)}"))
        _list.append(KeyValueObj(key: "$replaceRoot", value: "{newRoot: {\(newRootString), \(newRootKeys)}}"))
    }
    
    
    //MARK: - HELPERS
    func string(object: Any) -> String
    {
        if let val = object as? BAObject
        {
            return "\"\(val._id)\""
        }
        else if let val = object as? Date
        {
            return "\"\(val.iso8601)\""
        }
        else if let val = object as? Int
        {
            return "\(val)"
        }
        else if let val = object as? Double
        {
            return "\(val)"
        }
        else if let val = object as? Bool
        {
            return "\(val)"
        }
        else if object is Array<Any>
        {
            if let ba_array = object as? Array<BAObject>
            {
                let ba_ids = ba_array.map{ $0._id }
                return "\(String(describing: ba_ids))"
            }
            else
            {
                return "\(String(describing: object))"
            }
        }
        else
        {
            return "\"\(String(describing: object))\""
        }
    }
}

//MARK: - Basic Constraints
extension BAQuery
{
    /// ------------------------------------------------------------------------------------------------------
    
    ///  Add a constraint that requires a particular key exists.
    ///
    ///  - parameter key: - The key that should exist.
    public mutating func whereExists(key: String)
    {
        if (allFilterHeaders[QueryMethod.KeyExist.rawValue] != nil)
        {
            allFilterHeaders[QueryMethod.KeyExist.rawValue]?[key] = key
        }
        else
        {
            allFilterHeaders[QueryMethod.KeyExist.rawValue] = [ key : key ]
        }
    }
    
    
    /// ------------------------------------------------------------------------------------------------------
    
    ///  Add a constraint that requires a particular key not exists.
    ///
    ///  - parameter key: - The key that should not exist.
    public mutating func whereDoesNotExist(key: String)
    {
        if (allFilterHeaders[QueryMethod.KeyNotExist.rawValue] != nil)
        {
            allFilterHeaders[QueryMethod.KeyNotExist.rawValue]?[key] = key
        }
        else
        {
            allFilterHeaders[QueryMethod.KeyNotExist.rawValue] = [ key : key ]
        }
        
    }
    
    /// ------------------------------------------------------------------------------------------------------
    
    /// Add a constraint to the query that requires a particular key’s object to be equal to the provided object.
    ///
    ///  - parameter key: - The key to be constrained.
    ///  - parameter obj: - The object that must be equalled.
    public mutating func `where`(key: String, equalTo obj: Any)
    {
        var allFilterHeaders_Copy = allFilterHeaders
        if (allFilterHeaders_Copy[QueryMethod.KeyEqual.rawValue] != nil)
        {
            allFilterHeaders_Copy[QueryMethod.KeyEqual.rawValue]?[key] = string(object: obj)
        }
        else
        {
            allFilterHeaders_Copy[QueryMethod.KeyEqual.rawValue] = [ key : string(object: obj) ]
        }
        allFilterHeaders = allFilterHeaders_Copy
    }
    
    /// ------------------------------------------------------------------------------------------------------
    
    /// Add a constraint to the query that requires a particular key’s object to be not equal to the provided object.
    ///
    ///  - parameter key: - The key to be constrained.
    ///  - parameter obj: - The object that must not be equalled.
    public mutating func `where`(key: String, notEqualTo obj: Any)
    {
        var allFilterHeaders_Copy = allFilterHeaders
        if (allFilterHeaders_Copy[QueryMethod.KeyNotEqual.rawValue] != nil)
        {
            allFilterHeaders_Copy[QueryMethod.KeyNotEqual.rawValue]?[key] = string(object: obj)
        }
        else
        {
            allFilterHeaders_Copy[QueryMethod.KeyNotEqual.rawValue] = [ key : string(object: obj) ]
        }
        allFilterHeaders = allFilterHeaders_Copy
    }
    
    /// ------------------------------------------------------------------------------------------------------
    
    /// Add a constraint to the query that requires a particular key’s object to be less than the provided object.
    ///
    ///  - parameter key: - The key to be constrained.
    ///  - parameter obj: - The object that provides an upper bound.
    public mutating func `where`(key: String, lessThan obj: Any)
    {
        var allFilterHeaders_Copy = allFilterHeaders
        if (allFilterHeaders_Copy[QueryMethod.KeyLessThan.rawValue] != nil)
        {
            allFilterHeaders_Copy[QueryMethod.KeyLessThan.rawValue]?[key] = string(object: obj)
        }
        else
        {
            allFilterHeaders_Copy[QueryMethod.KeyLessThan.rawValue] = [ key : string(object: obj) ]
        }
        allFilterHeaders = allFilterHeaders_Copy
    }
    
    /// ------------------------------------------------------------------------------------------------------
    
    /// Add a constraint to the query that requires a particular key’s object to be greater than the provided object.
    ///
    ///  - parameter key: - The key to be constrained.
    ///  - parameter obj: - The object that provides an lower bound.
    public mutating func `where`(key: String, greaterThan obj: Any)
    {
        var allFilterHeaders_Copy = allFilterHeaders
        if (allFilterHeaders_Copy[QueryMethod.KeyGreaterThan.rawValue] != nil)
        {
            allFilterHeaders_Copy[QueryMethod.KeyGreaterThan.rawValue]?[key] = string(object: obj)
        }
        else
        {
            allFilterHeaders_Copy[QueryMethod.KeyGreaterThan.rawValue] = [ key : string(object: obj) ]
        }
        allFilterHeaders = allFilterHeaders_Copy
    }
    
    /// ------------------------------------------------------------------------------------------------------
    
    /// Add a constraint to the query that requires a particular key’s object to be less than or equal the provided object.
    ///
    ///  - parameter key: - The key to be constrained.
    ///  - parameter obj: - The object that provides an upper bound.
    public mutating func `where`(key: String, lessThanOrEqual obj: Any)
    {
        var allFilterHeaders_Copy = allFilterHeaders
        if (allFilterHeaders_Copy[QueryMethod.KeyLessThanOrEqual.rawValue] != nil)
        {
            allFilterHeaders_Copy[QueryMethod.KeyLessThanOrEqual.rawValue]?[key] = string(object: obj)
        }
        else
        {
            allFilterHeaders_Copy[QueryMethod.KeyLessThanOrEqual.rawValue] = [ key : string(object: obj) ]
        }
        allFilterHeaders = allFilterHeaders_Copy
    }
    
    /// ------------------------------------------------------------------------------------------------------
    
    /// Add a constraint to the query that requires a particular key’s object to be greater than or equal the provided object.
    ///
    ///  - parameter key: - The key to be constrained.
    ///  - parameter obj: - The object that provides an lower bound.
    public mutating func `where`(key: String, greaterThanOrEqual obj: Any)
    {
        var allFilterHeaders_Copy = allFilterHeaders
        if (allFilterHeaders_Copy[QueryMethod.KeyGreaterThanOrEqual.rawValue] != nil)
        {
            allFilterHeaders_Copy[QueryMethod.KeyGreaterThanOrEqual.rawValue]?[key] = string(object: obj)
        }
        else
        {
            allFilterHeaders_Copy[QueryMethod.KeyGreaterThanOrEqual.rawValue] = [ key : string(object: obj) ]
        }
        allFilterHeaders = allFilterHeaders_Copy
    }
    
    /// ------------------------------------------------------------------------------------------------------
    
    /// Add a constraint to the query that requires a particular key’s object to be contained in the provided array.
    ///
    ///  - parameter key: - The key to be constrained.
    ///  - parameter array: - The possible values for the key’s object.
    public mutating func `where`(key: String, containedIn array: Array<Any>)
    {
        var allFilterHeaders_Copy = allFilterHeaders
        if (allFilterHeaders_Copy[QueryMethod.KeyContainedIn.rawValue] != nil)
        {
            allFilterHeaders_Copy[QueryMethod.KeyContainedIn.rawValue]?[key] = string(object: array)
        }
        else
        {
            allFilterHeaders_Copy[QueryMethod.KeyContainedIn.rawValue] = [ key : string(object: array) ]
        }
        allFilterHeaders = allFilterHeaders_Copy
    }
    
    /// ------------------------------------------------------------------------------------------------------
    
    /// Add a constraint to the query that requires a particular key’s object not be contained in the provided array.
    ///
    ///  - parameter key: - The key to be constrained.
    ///  - parameter array: - The list of values the key’s object should not be.
    public mutating func `where`(key: String, notContainedIn array: Array<Any>)
    {
        var allFilterHeaders_Copy = allFilterHeaders
        if (allFilterHeaders_Copy[QueryMethod.KeyNotContainedIn.rawValue] != nil)
        {
            allFilterHeaders_Copy[QueryMethod.KeyNotContainedIn.rawValue]?[key] = string(object: array)
        }
        else
        {
            allFilterHeaders_Copy[QueryMethod.KeyNotContainedIn.rawValue] = [ key : string(object: array) ]
        }
        allFilterHeaders = allFilterHeaders_Copy
    }
    
    /// ------------------------------------------------------------------------------------------------------
    
    /// Add a constraint to the query that requires a particular key’s array contains every element of the provided array.
    ///
    ///  - parameter key: - The key to be constrained.
    ///  - parameter array: - The array of values to search for.
    public mutating func `where`(key: String, containsAllObjectsIn array: Array<Any>)
    {
        var allFilterHeaders_Copy = allFilterHeaders
        if (allFilterHeaders_Copy[QueryMethod.KeyContainsAllObjects.rawValue] != nil)
        {
            allFilterHeaders_Copy[QueryMethod.KeyContainsAllObjects.rawValue]?[key] = string(object: array)
        }
        else
        {
            allFilterHeaders_Copy[QueryMethod.KeyContainsAllObjects.rawValue] = [ key : string(object: array) ]
        }
        allFilterHeaders = allFilterHeaders_Copy
    }
    
    //MARK: - ADDING SUBQUERIES
    
    public static func orQuery(with subqueries: BAQuery...) -> BAQuery
    {
        let ex = subqueries.first
        var query = BAQuery((ex?.type)!)
        query.orSubqueries = subqueries
        
        return query
    }
    
    //MARK: - CONSTRUCTOR
    
    private mutating func constructor_query(of method: QueryMethod) -> (before: [String],after: [String])
    {
        var headersAfter = [String]()
        var headersBefore = [String]()
        if let addedHeaders = allFilterHeaders[method.rawValue]
        {
            for (key, value) in addedHeaders
            {
                let allFuncs = selectFuncsHeaders.standart + selectFuncsHeaders.group + selectFuncsHeaders.ignore
                
                var filterStr = ""
                if allFuncs.contains(key)
                {
                    if selectFuncsHeaders.ignore.contains(key)
                    {
                        continue
                    }
                    
                    switch method
                    {
                        
                    case .KeyExist:
                        filterStr = "\"\(key)\" : { \(Operators.notEqual.rawValue) : [] }"
                    case .KeyNotExist:
                        filterStr = "\"\(key)\" : { \(Operators.equal.rawValue) : [] }"
                    case .KeyEqual:
                        filterStr = "\"\(key)\" : { \(Operators.equal.rawValue) : \(value) }"
                        
                    default:break
                        
                    }
                    
                    headersAfter.append(filterStr)
                }
                else
                {
                    switch method
                    {
                        
                    case .KeyExist:
                        filterStr = "\"\(key)\" : { \(Operators.exists.rawValue) : true }"
                    case .KeyNotExist:
                        filterStr = "\"\(key)\" : { \(Operators.exists.rawValue) : false }"
                    case .KeyEqual:
                        filterStr = "\"\(key)\" : { \(Operators.equal.rawValue) : \(value) }"
                    case .KeyNotEqual:
                        filterStr = "\"\(key)\" : { \(Operators.notEqual.rawValue) : \(value) }"
                    case .KeyLessThan:
                        filterStr = "\"\(key)\" : { \(Operators.lessThan.rawValue) : \(value) }"
                    case .KeyGreaterThan:
                        filterStr = "\"\(key)\" : { \(Operators.greaterThan.rawValue) : \(value) }"
                    case .KeyLessThanOrEqual:
                        filterStr = "\"\(key)\" : { \(Operators.lessThanOrEqual.rawValue) : \(value) }"
                    case .KeyGreaterThanOrEqual:
                        filterStr = "\"\(key)\" : { \(Operators.greaterThanOrEqual.rawValue) : \(value) }"
                    case .KeyContainedIn:
                        filterStr = "\"\(key)\" : { \(Operators.containedIn.rawValue) : \(value) }"
                    case .KeyNotContainedIn:
                        filterStr = "\"\(key)\" : { \(Operators.notContainedIn.rawValue) : \(value) }"
                    case .KeyContainsAllObjects:
                        filterStr = "\"\(key)\" : { \(Operators.containsAllObjects.rawValue) : \(value) }"
                        
                    }
                    
                    headersBefore.append(filterStr)
                }
            }
        }
        
        return (headersBefore,headersAfter)
    }
    
    fileprivate mutating func updateFullMatchValue()
    {
        var headersBefore = [String]()
        var headersAfter = [String]()
        for method in QueryMethod.getMethods()
        {
            let headers:([String],[String]) = constructor_query(of: method)
            headersBefore += headers.0
            headersAfter += headers.1
        }
        
        if headersBefore.count != 0
        {
            _list.insert(KeyValueObj(key: Filter.match.rawValue, value: BAQuery.updateMatchValue(of: headersBefore)), at: 0)
        }
        
        if headersAfter.count != 0
        {
            _list.append(KeyValueObj(key: Filter.match.rawValue, value: BAQuery.updateMatchValue(of: headersAfter)))
        }

        
        updateOrQueryValues()
    }
    
    fileprivate mutating func updateOrQueryValues()
    {
        if let subQ = self.orSubqueries
        {
            var before = [[String]]()
            var after  = [[String]]()
            
            //Or Subqueries
            for var query in subQ
            {
                var hBefore = [String]()
                var hAfter = [String]()
                for method in QueryMethod.getMethods()
                {
                    let headers:([String],[String]) = query.constructor_query(of: method)
                    hBefore += headers.0
                    hAfter += headers.1
                }
                
                if hBefore.count > 0 { before.append(hBefore) }
                
                if hAfter.count > 0 { after.append(hAfter) }
            }
            
            //{ $or: [ { pagesCount: 14, title: "Book 144" }, { pagesCount: 10 } ] }
            if before.count != 0
            {
                var matchValues = [String]()
                for headers in before
                {
                    matchValues.append(BAQuery.updateMatchValue(of: headers))
                }
                
                if matchValues.count > 1
                {
                    let fullMatch = "{$or : [\(matchValues.joined(separator: ","))]}"
                    _list.insert(KeyValueObj(key: Filter.match.rawValue, value: fullMatch), at: 0)
                }
                else
                {
                    _list.insert(KeyValueObj(key: Filter.match.rawValue, value: matchValues.first!), at: 0)
                }
            }
            
            if after.count != 0
            {
                var matchValues = [String]()
                for headers in after
                {
                    matchValues.append(BAQuery.updateMatchValue(of: headers))
                }
                
                if matchValues.count > 1
                {
                    let fullMatch = "{$or : [\(matchValues.joined(separator: ","))]}"
                    _list.append(KeyValueObj(key: Filter.match.rawValue, value: fullMatch))
                }
                else
                {
                    _list.append(KeyValueObj(key: Filter.match.rawValue, value: matchValues.first!))
                }
            }
        }
    }
    
    fileprivate static func updateMatchValue(of headers:[String]) -> String
    {
        let matchValue = "{\(headers.joined(separator: ","))}"
        
        return matchValue
    }
}

//MARK: - SORT
extension BAQuery
{
    /// ------------------------------------------------------------------------------------------------------
    
    ///  Sort the results in ascending order with the given key.
    ///
    ///  - parameter ascending: - The key to order by.
    public mutating func orderBy(ascending: String)
    {
        sortKeyHeaders[SortMethod.KeyAscending.rawValue] = ascending
    }
    
    /// ------------------------------------------------------------------------------------------------------
    
    
    
    ///  Additionally sort in ascending order by the given key.
    ///  The previous keys provided will precedence over this key.
    ///
    ///  - parameter ascending: - The key to order by.
    public mutating func addOrderBy(ascending: String)
    {
        sortKeyHeaders[SortMethod.KeyAddAscending.rawValue] = ascending
    }
    
    /// ------------------------------------------------------------------------------------------------------
    
    ///  Sort the results in descending order with the given key.
    ///
    ///  - parameter descending: - The key to order by.
    public mutating func orderBy(descending: String)
    {
        sortKeyHeaders[SortMethod.KeyDescending.rawValue] = descending
    }
    
    /// ------------------------------------------------------------------------------------------------------
    
    
    
    ///  Additionally sort in descending order by the given key.
    ///  The previous keys provided will precedence over this key.
    ///
    ///  - parameter descending: - The key to order by.
    public mutating func addOrderBy(descending: String)
    {
        sortKeyHeaders[SortMethod.KeyAddDescending.rawValue] = descending
    }
    
    fileprivate mutating func updateFullSortValue()
    {
        if (sortKeyHeaders.count == 0) { return }
        
        var str = ""
        for (key , value) in sortKeyHeaders
        {
            var filterStr = ""
            switch SortMethod(rawValue: key)!
            {
            case .KeyAscending:
                filterStr = "\"\(value)\" : 1"
            case .KeyAddAscending:
                filterStr = "\"\(value)\" : 1"
            case .KeyDescending:
                filterStr = "\"\(value)\" : -1"
            case .KeyAddDescending:
                filterStr = "\"\(value)\" : -1"
            }

            str += "\(filterStr),"
        }
        
        str.remove(at: str.index(before: str.endIndex))
        let fullFilterValue = "{\(str)}"
        
        _list.append(KeyValueObj(key: Filter.sort.rawValue, value: fullFilterValue))
    }
}

//MARK: - Paginating Results
extension BAQuery
{
    fileprivate mutating func updatePaginigs()
    {
        if let ski = skip
        {
            _list.append(KeyValueObj(key: Filter.skip.rawValue, value: "\(ski)"))
        }
        
        if let lim = limit
        {
            _list.append(KeyValueObj(key: Filter.limit.rawValue, value: "\(lim)"))
        }
    }
}

//MARK: - UPDATE
extension BAQuery
{
    fileprivate mutating func addUpdateFields()
    {
        if let instance = object as? BAObject
        {
            let ints = instance.getInts()
            if (ints.count == 0) { return }
            
            var str = ""
            //{ $inc: { quantity: -2, "metrics.orders": 1 } }
            for (prop, value) in ints
            {
                str += "\(prop) : \(value.incrementVal),"
            }
            
            str.remove(at: str.index(before: str.endIndex))
            let fullIncrementValue = "{\(str)}"
            
            _list.append(KeyValueObj(key: Update.increment.rawValue, value: fullIncrementValue))
        }
    }
}

//MARK: - HELPERS
extension BAObject
{
    public class func query() -> BAQuery
    {
        return BAQuery(self)
    }
    
    func getInts() -> [String : BAInteger]
    {
        var ints = [String : BAInteger]()
        for prop in propertyNames() {
            if let val = self.value(forKey: prop) as? BAInteger
            {
                if val.mongoOperationExist
                {
                    ints[prop] = val
                    val.mongoOperationExist = false
                }
            }
        }
        
        return ints
    }
    
    func getFuncions(of type:BAFunctionName?) -> [String : BAFunction]
    {
        var funcs = [String : BAFunction]()
        for prop in propertyNames() {
            if let val = self.value(forKey: prop) as? BAFunction
            {
                if type != nil
                {
                    if val.funcName == type
                    {
                        funcs[prop] = val
                    }
                }
                else
                {
                    if val.funcName != nil
                    {
                        funcs[prop] = val
                    }
                }
            }
        }
        
        return funcs
    }
    
    func getKeys(of type: BATypes) -> [String]
    {
        var keys = [String]()
        for prop in propertyNames() {
            if let ty = self.getTypeOfProperty(name: prop)
            {
                if type.isEqual(type: ty)
                {
                    keys.append(prop)
                }
            }
        }
        
        return keys
    }
}

class BAKeys
{
    var standart: Set<String>
    var pointers: Set<String>
    var relations: Set<String>
    
    init() {
        standart = Set<String>()
        pointers = Set<String>()
        relations = Set<String>()
    }
    
    func allKeys() -> Set<String>
    {
        return standart.union(pointers)
    }
}

extension BAQuery
{
    mutating func getFullSelectedKeys() -> Set<String>
    {
        // Selected keys
        var selectedKeys = [String]()
        if selectKeysHeaders.count == 0
        {
            let propNames = type.self.init().propertyNames(includePointers: false, includeRelations: false,includeLocals: false, includeFunctions: false)
            selectedKeys = propNames
        }
        
        selectedKeys += includeKeyHeaders
        selectedKeys += selectFuncsHeaders.standart
        
        return Set(selectedKeys)
    }
    
    mutating func getIncludedSubdocs() -> (pointer:[String], relation:[String])
    {
        let obj = type.self.init()
        var pointers = [String]()
        var relations = [String]()
        
        /////////////// Include key ///////////////
        var (allPoiters, allRelations) = obj.getPointersAndRelations()
        if includeKeyHeaders.count > 0
        {
            
            allPoiters = allPoiters.filter({ (key) -> Bool in
                if includeKeyHeaders.contains(key)
                {
                    return true
                }
                
                return false
            })
            
            allRelations = allRelations.filter({ (key) -> Bool in
                if includeKeyHeaders.contains(key)
                {
                    return true
                }
                
                return false
            })
            
            pointers = allPoiters
            relations = allRelations
        }
        
        return (pointers, relations)
    }
}
