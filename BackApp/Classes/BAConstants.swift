//
//  BAConstants.swift
//  Pods
//
//  Created by GeghamHarutyunyan on 2/15/17.
//
//

import Foundation

struct k
{
    struct DefaultKeys
    {
        static let ServerUniqueID = "_id"
        static let createdAt = "createdAt"
        static let updatedAt = "updatedAt"
        
        
        static let local_prefix = "Local_"
        static let temp_prefix = "Temp_"
        static let local_PK = "local_id"
        static let local_FieldsToUpdate = "FieldsToUpdate"
        static let status = "ba_status"
        static let anonymous_suffix = "_Anonymous"
        static let eventually_suffix = "_Eventually"
        
        static let headerQuery = "query"
        static let headerUpdateQuery = "update"
        
        static let NotyKey = "Objects"
        
        //User
        static let currentUser_id = "currentUser_id"
        static let currentUser_Name = "currentUser_name"
        
        //Message
        struct Messages
        {
            static let NoInternet = "No internet connection"
            static let LocalClass = "This class is not Local."
            static let CloudClass = "This class is Local."
            static let NewObject  = "You can't delete not created object"
        }
        
        // LocalUsedProperties
        static let Obj_Sync = "syncing"
        static let Active_Keys = "active_keys"
        static let Server_Local = "server_local"
        static let Observing_Block = "Observing_Block"
        static let Eventually_Call = "Eventually_Call"
    }
}
