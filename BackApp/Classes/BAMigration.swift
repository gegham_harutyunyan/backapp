//
//  BAObjectPersistence.swift
//  Pods
//
//  Created by GeghamHarutyunyan on 2/15/17.
//
//

import Foundation
import GRDB

public extension BackApp
{
    public class func registerSubclasses(_ subclasses: [BAObject.Type])
    {
        //Remember registered subclasses
        BackApp.regSubclasses = getAllSubclasses(subclasses)
        
        //Migrate data for subclasses if needed
        for sub in BackApp.regSubclasses
        {
            sub.registerSubclass()
        }
        
        for (_, value) in TablesToMigrate
        {
            value.migrate(type: value)
        }
    }
    
    public class func registerSubclasses(_ subclasses: BAObject.Type...)
    {
        registerSubclasses(subclasses)
    }
    
    private class func getAllSubclasses(_ subclasses: [BAObject.Type]) -> [BAObject.Type]
    {
        var UClass = false
        var IClass = false
        var allSubclasses = subclasses
        for sub in subclasses
        {
            if sub.isSubclass(of: BAUser.self)
            {
                UClass = true
            }
            else if sub.isSubclass(of: BAInstallation.self)
            {
                IClass = true
            }
        }
        
        if !UClass
        {
            allSubclasses.append(BAUser.self)
        }
        
        if !IClass
        {
            allSubclasses.append(BAInstallation.self)
        }
        
        return allSubclasses
    }
}

fileprivate var TablesToMigrate = [String : BAObject.Type]()
extension BAObject
{
    class func registerSubclass()
    {
        let collectionName = self.mapName()
        let instance = self.init()
        
        let columns = try! instance.columnChanges()
        
        // Check if in new columns exists pointer
        let newPointerExists = columns.newColumns.pointer.count > 0
        let newRelationsExists = columns.newColumns.relation.count > 0
        let newCommanColumnExists = columns.newColumns.comman.count > 0

        //update table with new columns if comman/pointer columns exists
        if newPointerExists || newCommanColumnExists
        {
            dbQueue.inDatabase { db in
                
                //If table not exist then create
                if(instance.checkTable(db, tableName: collectionName))
                {
                    try? db.alter(table: collectionName) { t in
                        
                        for col in columns.newColumns.comman + columns.newColumns.pointer
                        {
                            instance.setColumnPerProperty(db, property: col, tAltert: t)
                        }
                    }
                }
            }
        }
        
        // If new pointer column exists
        // then the table was already created with new columns,
        // but need to recreate tables for adding FK.
        // Or if new relation added.
        if newPointerExists || columns.oldColumns.isEmpty == false || newRelationsExists
        {
            migrate(type: self)
        }
    }
    
    class func migrate<T>(type: T.Type) where T:BAObject
    {
        var migrator = DatabaseMigrator()
        let migratorName = "\(type.mapName())+\(Date())"
        if #available(iOS 8.2, *) {
            migrator.registerMigrationWithDeferredForeignKeyCheck(migratorName) { db in
                
                let newTableName = "new_\(type.mapName())"
                let oldTableName = "\(type.mapName())"
                var propertiesToMigrate = [String]()
                try db.create(table: newTableName) { t in
                    
                    let instance = type.init()
                    let columnNames = instance.propertyNames(includeFunctions: false)
                    for property in columnNames
                    {
                        propertiesToMigrate.append(property)
                        instance.setColumnPerProperty(db, property: property, tDefine: t, propertiesToMigrate: &propertiesToMigrate)
                    }
                }
                
                let propertiesToMigrateString = propertiesToMigrate.joined(separator: ",")
                try db.execute("INSERT INTO \(newTableName) SELECT \(propertiesToMigrateString) FROM \(oldTableName)")
                try db.drop(table: oldTableName)
                try db.rename(table: newTableName, to: oldTableName)
            }
        } else {
            // Fallback on earlier versions
        }
        
        try! migrator.migrate(dbQueue)
    }
    
    func columnChanges() throws -> (newColumns: (comman: [String],pointer: [String],relation: [String]),oldColumns:[String])
    {
        let model = type(of:self)
        let tableName = model.mapName()
        
        let instance = self
        var columnNames = instance.propertyNames(includeFunctions: false)
        
        
        let pointersANDrelations = instance.getPointersAndRelations()
        
        var commanColumns = [String]()
        var pointerColumns = [String]()
        var relationColumns = [String]()
        var oldColumns = [String]()
        
        return try dbQueue.inDatabase { db -> (newColumns: (comman: [String],pointer: [String],relation: [String]),oldColumns: [String]) in
            
            for row in try Row.fetchAll(db, "PRAGMA table_info('\(tableName)')")
            {
                let col = row["name"] as! String
                
                if columnNames.contains(col)
                {
                    columnNames.remove(at: columnNames.index(of: col)!)
                }
                else
                {
                    oldColumns.append(col)
                }
            }
            
            for newCol in columnNames
            {
                if pointersANDrelations.pointers.contains(newCol)
                {
                    pointerColumns.append(newCol)
                }
                else if pointersANDrelations.relations.contains(newCol)
                {
                    let ba_relation = instance.value(forKey: newCol) as! BARelation
                    let relationTableName = model.getRelationName(of: ba_relation.storedType)
                    
                    if try! db.tableExists(relationTableName) == false
                    {
                        relationColumns.append(newCol)
                    }
                }
                else
                {
                    commanColumns.append(newCol)
                }
            }
            
            return ((commanColumns,pointerColumns,relationColumns),oldColumns)
        }
    }
    
    //MARK: - CREATE/UPDATE TABLE
    
    private func checkTable(_ db: Database, tableName: String) -> Bool
    {
        if try! db.tableExists(tableName)
        {
            return true
        }
        
        createTable(db, tableName: tableName)
        
        return false
    }
    
    private func createTable(_ db: Database, tableName: String)
    {
        try? db.create(table: tableName, ifNotExists: true) { t in
            
            var tempArr = [String]()
            for property in propertyNames(includeRelations: false, includeFunctions: false)
            {
                setColumnPerProperty(db, property: property, tDefine: t, propertiesToMigrate:&tempArr)
            }
        }
    }
    
    private func createRelationTable(_ db:Database, type_1: BAObject.Type, type_2: BAObject.Type)
    {
        let firstCollection = type_1.mapName()
        let secondCollection = type_2.mapName()
        
        var arr = [type_1.mapName(),type_2.mapName()]
        arr.sort { $0 < $1 }
        let relationTableName = "\(arr[0])_\(arr[1])"
        
        var col1 = ""
        var col2 = ""
        if firstCollection == secondCollection
        {
            col1 = "A_\(firstCollection)_id"
            col2 = "B_\(secondCollection)_id"
        }
        else
        {
            col1 = "\(arr[0])_id"
            col2 = "\(arr[1])_id"
        }
        
        try? db.create(table: relationTableName, ifNotExists: true) { t in
            
            t.column(col1, .text)
            t.foreignKey([col1], references: arr[0], columns: [k.DefaultKeys.ServerUniqueID], onDelete: .cascade, onUpdate: .cascade)
            
            t.column(col2, .text)
            t.foreignKey([col2], references: arr[1], columns: [k.DefaultKeys.ServerUniqueID], onDelete: .cascade, onUpdate: .cascade)
            
            t.column("type", .text)
            t.column("ba_status", .text)
            
            t.uniqueKey([col1,col2, "type"], onConflict: .replace)
        }
    }
    
    private func setColumnPerProperty(_ db: Database, property: String, tDefine: TableDefinition, propertiesToMigrate: inout [String])
    {
        if let type = getTypeOfProperty(name: property)
        {
            if BATypes.BABool.isEqual(type: type)
            {
                tDefine.column(property, .boolean)
            }
            else if BATypes.BAFile.isEqual(type: type)
            {
                tDefine.column(property, .text)
            }
            else if BATypes.BANumber.isEqual(type: type)
            {
                tDefine.column(property, .double)
            }
            else if BATypes.Date.isEqual(type: type)
            {
                tDefine.column(property, .datetime)
            }
            else if BATypes.BAInteger.isEqual(type: type)
            {
                if property == k.DefaultKeys.local_PK
                {
                    tDefine.column(property, .integer).primaryKey(onConflict: Database.ConflictResolution.replace, autoincrement: true)
                }
                else
                {
                    tDefine.column(property, .integer)
                }
            }
            else if BATypes.BAArray.isEqual(type: type) ||
                BATypes.BADictionary.isEqual(type: type)
            {
                tDefine.column(property, .blob)
            }
            else if BATypes.BARelation.isEqual(type: type)
            {
                //Relation
                
                // Create Table With RelatedType-1_relatedType-2
                // ex. User_Book or Book_User
                
                // Columns: 1'st table _id,2'nd table _id, type
                // ex. User_id, Book_id, type
                
                // type will inlude relationName
                // ex. User_Book_myBooks
                
                let selfType = Swift.type(of:self)
                let ba_relation = self.value(forKey: property) as! BARelation
                
                let relationTableName = selfType.getRelationName(of: ba_relation.storedType)
                
                if try! db.tableExists(relationTableName) == false
                {
                    createRelationTable(db, type_1: selfType, type_2: ba_relation.storedType)
                }
                
                //remove relation columns from properties to migrate list
                if propertiesToMigrate.count > 0
                {
                    if let index = propertiesToMigrate.index(of: property)
                    {
                        propertiesToMigrate.remove(at: index)
                    }
                }
            }
            else if BATypes.String.isEqual(type: type)
            {
                if property == k.DefaultKeys.ServerUniqueID
                {
                    tDefine.column(property, .text).unique(onConflict: .replace)
                }
                else
                {
                    tDefine.column(property, .text)
                }
            }
            else
            {
                //pointer
                let ty = BAObject.getTypeObject(of: "\(type.clearType())")
                
                if try! db.tableExists(ty.mapName())
                {
                    tDefine.column(property, .text)
                    tDefine.foreignKey([property], references: ty.mapName(), columns: [k.DefaultKeys.ServerUniqueID], onDelete: .cascade, onUpdate: .cascade)
                }
                else
                {
                    tDefine.column(property, .text)
                    
                    let selfType = Swift.type(of:self)
                    TablesToMigrate[selfType.mapName()] = selfType
                }
            }
        }
    }

    //Add Columns on Existing table
    private func setColumnPerProperty(_ db: Database, property: String, tAltert: TableAlteration)
    {
        if let type = getTypeOfProperty(name: property)
        {
            if BATypes.BABool.isEqual(type: type)
            {
                tAltert.add(column: property, .boolean)
            }
            else if BATypes.BAFile.isEqual(type: type)
            {
                tAltert.add(column: property, .text)
            }
            else if BATypes.BANumber.isEqual(type: type)
            {
                tAltert.add(column: property, .double)
            }
            else if BATypes.Date.isEqual(type: type)
            {
                tAltert.add(column: property, .datetime)
            }
            else if BATypes.BAInteger.isEqual(type: type)
            {
                if property == k.DefaultKeys.local_PK
                {
                    tAltert.add(column: property, .integer).primaryKey(onConflict: Database.ConflictResolution.replace, autoincrement: true)
                }
                else
                {
                    tAltert.add(column: property, .integer)
                }
            }
            else if BATypes.BAArray.isEqual(type: type) ||
                BATypes.BADictionary.isEqual(type: type)
            {
                tAltert.add(column: property, .blob)
            }
            else if BATypes.String.isEqual(type: type)
            {
                if property == k.DefaultKeys.ServerUniqueID
                {
                    tAltert.add(column: property, .text).unique(onConflict: .replace)
                }
                else
                {
                    tAltert.add(column: property, .text)
                }
            }
            else if BATypes.BARelation.isEqual(type: type)
            {
                //Do nothing
            }
            else
            {
                tAltert.add(column: property, .text)
            }
        }
    }
}
