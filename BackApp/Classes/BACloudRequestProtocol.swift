//
//  BACloudRequestProtocol.swift
//  Pods
//
//  Created by GeghamHarutyunyan on 3/7/17.
//
//

import Foundation
import SwiftyJSON

// MARK: - STATIC REQUESTS

public protocol CloudRequestProtocol
{
    static func getAll<T>(items forType: T.Type, filter: BAQuery?, completion: @escaping (BAResult<[T]>) -> Void) where T:BAObject
    
    static func save<T>(instance:T ,completion: @escaping (BAResult<T>) -> Void) where T:BAObject
    static func delete<T>(instance:T ,completion: @escaping (BAResult<T>) -> Void) where T:BAObject
    
    static func get<T>(instance:T, completion: @escaping (BAResult<T>) -> Void) where T:BAObject
}

extension BackApp: CloudRequestProtocol{}

// MARK: - Filter
extension CloudRequestProtocol
{
    public static func getAll<T>(items forType: T.Type,
                                 filter: BAQuery? = nil,
                                 completion: @escaping (BAResult<[T]>) -> Void) where T:BAObject
    {
        //if class local,then throw error
        if T.isLocalClass()
        {
            completion(BAResult.Failure(k.DefaultKeys.Messages.CloudClass, nil))
            return
        }
        
        //Connection chack
        let connectionResult = BackApp.isReachableConnection()
        if connectionResult.isFailure
        {
            completion(BAResult.Failure((connectionResult.error?.message)!, connectionResult.error?.code))
            return
        }
        
        BackApp.addOperation(BackApp.operationsQueue)
        {
            BAObject.request(T.self().getRequestURL(), filters: filter) { result in
                
                switch (result)
                {
                case .Success(_):
                    
                    //to get JSON return value
                    if let result = result.value
                    {
                        let jsonObj = JSON(result!)
                        switch (jsonObj.type)
                        {
                        case Type.array:
                            
                            var array = [T]()
                            for next in jsonObj
                            {
                                let dict = next.1.dictionaryObject!
                                let server_ID = dict[k.DefaultKeys.ServerUniqueID] as! String
                                let obj = T.instance(objType: forType, _id: server_ID, local_id: nil)
                                
                                obj.fillAll(dictonaryObject: dict, query: filter, rType: .get)
                                
                                //Keep In Mind
                                T.keepInMind(obj: obj)
                                
                                //if update on CurrentUser
                                if obj is BAUser
                                {
                                    if (obj as! BAUser).isMe
                                    {
                                        BAUser.setCurrentUser(user: obj as! BAUser)
                                    }
                                }
                                
                                array.append(obj)
                            }
                            
                            completion(BAResult.Success(array))
                            
                        default:break
                        }
                    }
                    
                case .Failure(_, _):
                    
                    //to get Error
                    completion(BAResult.Failure((result.error?.message)!, result.error?.code))
                }
            }
        }
    }
}

// MARK: - Delete/Get
extension CloudRequestProtocol
{
    ///   Delete an object in the Cloud async
    ///
    /// - parameter instance: - The object that you are trying to delete
    /// - parameter completion: - In compilation you will receive either nothing(Success) or error message
    
    public static func delete<T>(instance:T ,completion: @escaping (BAResult<T>) -> Void) where T:BAObject
    {
        //if class local,then throw error
        if T.isLocalClass()
        {
            completion(BAResult.Failure(k.DefaultKeys.Messages.CloudClass, nil))
        }
        
        //If Object was not created
        if instance.isNewObject
        {
            completion(BAResult.Failure(k.DefaultKeys.Messages.NewObject, nil))
        }
        
        //if delete on CurrentUser
        if instance is BAUser
        {
            completion(BAResult.Failure("You are not allowed to delete User objcet.", nil))
        }
        else if instance is BAInstallation
        {
            completion(BAResult.Failure("You are not allowed to delete Installation objcet.", nil))
        }
        
        //Connection chack
        let connectionResult = BackApp.isReachableConnection()
        if connectionResult.isFailure
        {
            completion(BAResult.Failure((connectionResult.error?.message)!, connectionResult.error?.code))
        }
        
        //Choose rigth thread
        var queue = BackApp.operationsQueue
        if instance.isSyncProcess()
        {
            queue = BackApp.syncQueue
        }
        
        BackApp.addOperation(queue)
        {
            // semaphore with count equal to zero is useful for synchronizing completion of work, in our case the renewal of auth token
            let sema = DispatchSemaphore(value: 0);
            
            BAObject.request(instance.getRequestURL()+"\(instance._id)", method: .delete) { result in
                
                let type = Swift.type(of:instance)
                instance.prepareResultGeneric(type: type, result: result, rType: .delete, completion: { (finalResult) in
                    
                    completion(finalResult)
                    
                    if finalResult.isSuccess
                    {
                        let notificationSubscribe = "\(type.mapName() + "ObjectRemoved")"
                        NotificationCenter.default.post(name: Notification.Name(notificationSubscribe),
                                                        object: nil,
                                                        userInfo: [k.DefaultKeys.NotyKey : [instance]])
                    }
                    
                    // Signal that we are done
                    sema.signal();
                })
            }
            
            // Now we wait until the response block will send send a signal
            _ = sema.wait(timeout: DispatchTime.distantFuture)
        }
    }
    
    ///   Delete an object in the Cloud sync
    ///
    /// - parameter instance: - The object that you are trying to delete
    /// - parameter completion: - In compilation you will receive either nothing(Success) or error message
    
    public static func deleteSync<T>(instance:T) -> BAResult<T> where T:BAObject
    {
        // semaphore with count equal to zero is useful for synchronizing completion of work, in our case the renewal of auth token
        let sema = DispatchSemaphore(value: 0);
        
        var finalResult:BAResult<T>!
        BackApp.delete(instance: instance) { (result) in
            
            finalResult = result
            
            // Signal that we are done
            sema.signal();
        }
        
        // Now we wait until the response block will send send a signal
        _ = sema.wait(timeout: DispatchTime.distantFuture)
        
        return finalResult
    }
    
    
    /// Get an object from the Cloud
    ///
    /// - parameter instance: - The object that you are trying to get
    /// - parameter completion: - In compilation you will receive either full object or error message
    
    public static func get<T>(instance:T, completion: @escaping (BAResult<T>) -> Void) where T:BAObject
    {
        get(instance: instance) { (result) in completion(result)}
    }
    
    static func get<T>(instance:T, filter: BAQuery? = nil, completion: @escaping (BAResult<T>) -> Void) where T:BAObject
    {
        //if class local,then throw error
        if T.isLocalClass()
        {
            completion(BAResult.Failure(k.DefaultKeys.Messages.CloudClass, nil))
            return
        }
        
        //If Object was not created
        if instance.isNewObject
        {
            completion(BAResult.Failure(k.DefaultKeys.Messages.NewObject, nil))
            return
        }
        
        //Connection chack
        let connectionResult = BackApp.isReachableConnection()
        if connectionResult.isFailure
        {
            completion(BAResult.Failure((connectionResult.error?.message)!, connectionResult.error?.code))
            return
        }
        
        BackApp.addOperation(BackApp.operationsQueue)
        {
            var newFilter: BAQuery!
            if let query = filter
            {
                newFilter = query
            }
            else
            {
                let SELF = type(of:instance as BAObject)
                newFilter = BAQuery.defaultInit(SELF)
            }
            
            BAObject.request(instance.getRequestURL()+"\(instance._id)", filters: newFilter) { result in
                
                let type = Swift.type(of:instance)
                instance.prepareResultGeneric(type: type, result: result, query:newFilter, rType: .get, completion: { (finalResult) in
                    completion(finalResult)
                })
            }
        }
    }
}

// MARK: - Save
extension CloudRequestProtocol
{
    /// Create or Update an object in the Cloud sync
    ///
    /// - parameter instance: - The object that you are trying to create/update
    /// - parameter completion: - In compilation you will receive either created/updated object or error message
    
    public static func saveSync<T>(instance:T) -> BAResult<T> where T:BAObject
    {
        // semaphore with count equal to zero is useful for synchronizing completion of work, in our case the renewal of auth token
        let sema = DispatchSemaphore(value: 0);
        
        var finalResult:BAResult<T>!
        BackApp.save(instance: instance) { (result) in
            
            finalResult = result
            
            // Signal that we are done
            sema.signal();
        }
        
        // Now we wait until the response block will send send a signal
        _ = sema.wait(timeout: DispatchTime.distantFuture)
        
        return finalResult
    }
    
    /// Create or Update an object in the Cloud async
    ///
    /// - parameter instance: - The object that you are trying to create/update
    /// - parameter completion: - In compilation you will receive either created/updated object or error message
    
    public static func save<T>(instance:T ,completion: @escaping (BAResult<T>) -> Void) where T:BAObject
    {
        //if class local,then throw error
        if T.isLocalClass()
        {
            completion(BAResult.Failure(k.DefaultKeys.Messages.CloudClass, nil))
        }
        
        //if create a new User
        if instance is BAUser, instance.isNewObject
        {
            completion(BAResult.Failure("Please use BackApp.registration action to create User object properly.", nil))
            return
        }
        
        //Connection chack
        let connectionResult = BackApp.isReachableConnection()
        if connectionResult.isFailure
        {
            completion(BAResult.Failure((connectionResult.error?.message)!, connectionResult.error?.code))
            return
        }
        
        //Add new state
        let state:BAStateObject!
        if (instance.LocalUsedProperties[k.DefaultKeys.Eventually_Call] != nil)
        {
            state = instance.addNewState(.Eventually)
            instance.LocalUsedProperties.removeValue(forKey: k.DefaultKeys.Eventually_Call)
        }
        else
        {
            state = instance.addNewState()
        }
        
        var queue = BackApp.operationsQueue
        if instance.isSyncProcess()
        {
            queue = BackApp.syncQueue
        }
        
        BackApp.addOperation(queue)
        {
            // semaphore with count equal to zero is useful for synchronizing completion of work, in our case the renewal of auth token
            let sema = DispatchSemaphore(value: 0);
            
            BAObject.preRequests(instance) { (result) in
                
                if result.isSuccess
                {
                    if !instance.isCloudObject
                    {
                        create(instance: instance, state: state, completion: { (result) in
                            
                            completion(result)
                            
                            // Signal that we are done
                            sema.signal();
                        })
                    }
                    else
                    {
                        update(instance: instance, state: state, completion: { (result) in
                            
                            completion(result)
                            
                            // Signal that we are done
                            sema.signal();
                        })
                    }
                }
                else
                {
                    completion(BAResult.Failure((result.error?.message)!, result.error?.code))
                    
                    // Signal that we are done
                    sema.signal();
                }
            }
            
            // Now we wait until the response block will send send a signal
            _ = sema.wait(timeout: DispatchTime.distantFuture)
        }
    }
    
    /// Create an object in the Cloud
    ///
    /// - parameter instance: - The object that you are trying to create
    /// - parameter completion: - In compilation you will receive either created object or error message
    
    static func create<T>(instance:T, state: BAStateObject,completion: @escaping (BAResult<T>) -> Void) where T:BAObject
    {
        BAObject.request(instance.getRequestURL(), method: .post, parameters: state.encodedParams) { result in
            
            let type = Swift.type(of:instance)
            instance.prepareResultGeneric(type: type, result: result, rType: .post, completion: { (finalResult) in
                completion(finalResult)
            })
        }
    }
    
    /// Update an object in the Cloud
    ///
    /// - parameter instance: - The object that you are trying to update
    /// - parameter completion: - In compilation you will receive either updated object or error message
    
    static func update<T>(instance:T, state: BAStateObject,completion: @escaping (BAResult<T>) -> Void) where T:BAObject
    {
        var params = state.encodedParams
        params![k.DefaultKeys.ServerUniqueID] = instance._id
        
        BAObject.request(instance.getRequestURL()+"\(instance._id)", method: .put, parameters: params, filters: BAQuery.updateQuery(instance)) { result in
            
            let type = Swift.type(of:instance)
            instance.prepareResultGeneric(type: type, result: result, rType: .put, completion: { (finalResult) in
                
                completion(finalResult)
            })
        }
    }
}

//MARK: - Easier Accesses
public protocol CloudRequestProtocol_Easy
{
    //Easier Access
    func fetchFull(completion: @escaping (BAResult<Bool>) -> Void)
    func fetch(keys: [String]?,completion: @escaping (BAResult<Bool>) -> Void)
}

extension BAObject: CloudRequestProtocol_Easy
{
    public func fetchFull(completion: @escaping (BAResult<Bool>) -> Void)
    {
        let (pointers, relations) = self.getPointersAndRelations()
        let keys = pointers + relations
        
        self.fetch(keys: keys) { (result) in
            
            completion(result)
        }
    }
    
    public func fetch(keys: [String]? = nil,completion: @escaping (BAResult<Bool>) -> Void)
    {
        var filter: BAQuery? = nil
        if keys != nil
        {
            let type = Swift.type(of:self)
            filter = BAQuery(type)
            for key in keys!
            {
                filter?.include(key: key)
            }
        }
        
        BackApp.get(instance: self, filter: filter) { (result) in
            
            if result.isSuccess
            {
                completion(.Success(true))
            }
            else
            {
                completion(.Failure(result.error!.message, result.error!.code))
            }
        }
    }
}
