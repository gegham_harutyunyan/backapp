//
//  BAFile.swift
//  BackApp
//
//  Created by GeghamHarutyunyan on 3/16/17.
//  Copyright © 2017 WAY4APP. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

@objc
final public class BAFile: NSObject
{
    // MARK: - Properties
    @objc internal(set) public dynamic var url      : String? = nil
    @objc internal(set) public dynamic var name     : String? = nil
    fileprivate var imageData   : Data!
    var isNew = true
    
    @objc public dynamic var absoluteURL: URL?{
        get{
            if let strongUrl = url
            {
                let type = Swift.type(of: self)
                let url = "\(type.filesURL())/\(strongUrl)"
                return URL(string: url)
            }
            
            return nil
        }
    }
    
    // MARK: - Base
    static func mapName() -> String
    {
        return "File"
    }
    
    override init() {
        super.init()
    }
    
    public init(_ data: Data?)
    {
        super.init()
        imageData = data
    }
    
    public init(_ image: UIImage, _ compressionQuality: CGFloat)
    {
        super.init()
        imageData = UIImageJPEGRepresentation(image, compressionQuality)
    }
    
    class func createFile(with fileURL: String) -> BAFile
    {
        let file = BAFile()
        file.updateURL(fileURL)
        
        return file
    }
    
    func updateURL(_ url: String)
    {
        self.url = url
        var strArray = url.components(separatedBy: "==")
        
        if self.name == nil
        {
            if strArray.count > 1
            {
                self.name = strArray[1]
            }
        }
        
        self.isNew = false
    }
}

// MARK: - Action
public extension BAFile
{
    // MARK: - Constructor
    public func saveFile(completion: @escaping (BAResult<BAFile>) -> Void)
    {
        BAFile.save([self]) { (result) in
    
            if result.isSuccess
            {
                completion(BAResult.Success(result.value![0]))
            }
            else
            {
                completion(BAResult.Failure((result.error?.message)!, result.error?.code))
            }
        }
    }
    
    static func saveFiles<T>(of instance:T, completion: @escaping (BAResult<[BAFile]>) -> Void) where T:BAObject
    {
        let newFiles = instance.getNewFiles()
        BAFile.save(newFiles) { (result) in
            
            completion(result)
        }

    }
    
    private static func save(_ files: [BAFile] ,completion: @escaping (BAResult<[BAFile]>) -> Void)
    {
        var filesDict = [String : BAFile]()
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                for i in 0..<files.count
                {
                    let file = files[i]
                    let fileName = "file\(i).jpg"
                    filesDict[fileName] = file
                    multipartFormData.append(file.imageData, withName: "name", fileName: fileName, mimeType: "")
                }
        },
            to: filesURL(),
            headers: BackApp.getRequestHeaders(),
            encodingCompletion: { encodingResult in
                
                switch encodingResult
                {
                    
                case .success(let upload, _, _):
                    upload.response { response in
                        
                        let result = BackApp.checkResponseResult(res: response)
                        switch (result)
                        {
                        case .Success(_):
                            
                            //to get JSON return value
                            if let result = result.value
                            {
                                let jsonObj = JSON(result!)
                                
                                switch (jsonObj.type)
                                {
                                    
                                case Type.array:
                                    
                                    for (_, json) in jsonObj
                                    {
                                        if let fileName = json["name"].string
                                        {
                                            if let file = filesDict[fileName]
                                            {
                                                file.name = fileName
                                                
                                                if let urlStr = json["url"].string
                                                {
                                                    file.updateURL(urlStr)
                                                }
                                            }
                                        }
                                    }
                                    
                                default:break
                                    
                                }
                            }
                            
                            completion(BAResult.Success(files))
                            
                        case .Failure(_, _):
                            
                            //to get Error
                            completion(BAResult.Failure((result.error?.message)!, result.error?.code))
                        }
                        
                    }
                case .failure(let encodingError):
                    
                    //to get Error
                    completion(BAResult.Failure(encodingError.localizedDescription, nil))
                    
                }
        }
        )
    }
    
    // MARK: - Constructor Elements
    
    fileprivate static func filesURL() -> String
    {
        return BARequestType.URL(of: self.mapName())
    }
}

extension BAObject
{
    static func preRequests<T>(_ instance: T, completion: @escaping (BAResult<T>) -> Void) where T:BAObject
    {
        let files = instance.getNewFiles()
        if files.count > 0
        {
            BAFile.saveFiles(of: instance) { (result) in
                
                if result.isSuccess
                {
                    completion(BAResult.Success(instance))
                }
                else
                {
                    completion(BAResult.Failure((result.error?.message)!, result.error?.code))
                }
            }
        }
        else
        {
            completion(BAResult.Success(instance))
        }
    }
    
    func getNewFiles() -> [BAFile]
    {
        var files = [BAFile]()
        for prop in propertyNames() {
            if let val = self.value(forKey: prop) as? BAFile
            {
                if val.isNew == true
                {
                    files.append(val)
                }
            }
        }
        
        return files
    }
}
