//
//  Extentions.swift
//  Pods
//
//  Created by GeghamHarutyunyan on 1/18/17.
//
//

import Foundation

extension Dictionary
{
    func combinedWith(_ other: Dictionary<Key,Value>) -> Dictionary<Key,Value> {
        var other = other
        for (key, value) in self {
            other[key] = value
        }
        return other
    }
    
    /// Mutating version.
    mutating func combineWith(other: Dictionary<Key,Value>) {
        self = self.combinedWith(other)
    }

    init(keyValuePairs: Array<(Key, Value)>) {
        self.init()
        for pair in keyValuePairs {
            self[pair.0] = pair.1
        }
    }
    
    var json: String {
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }
}

extension Date
{
    static let iso8601Formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        return formatter
    }()
    
    var iso8601: String
    {
        return Date.iso8601Formatter.string(from: self)
    }
}

extension String
{
    var dateFromISO8601: Date?
    {
        return Date.iso8601Formatter.date(from: self)
    }

    func toBool() -> Bool? {
        switch self {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return nil
        }
    }
    
    func clearType() -> String
    {
        if let rangeStart = self.range(of: "<")
        {
            let lo = self.index(rangeStart.lowerBound, offsetBy: 1)
            
            if let rangeEnd = self.range(of: ">")
            {
                let hi = self.index(rangeEnd.lowerBound, offsetBy: 0)
                
                let subRange = lo..<hi
                return String(self[subRange])
            }
        }
        
        return self
    }
}

extension Array where Element:BAObject
{
    func saveLocal()
    {
        for item in self
        {
            try! item.saveLocal()
        }
    }
}

extension Array where Element: Equatable
{
    
    mutating func remove(object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}

extension Sequence where Iterator.Element: Hashable
{
    func unique() -> [Iterator.Element] {
        var seen: [Iterator.Element: Bool] = [:]
        return self.filter { seen.updateValue(true, forKey: $0) == nil }
    }
}
