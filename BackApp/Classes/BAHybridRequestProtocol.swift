//
//  BAHybridRequestProtocol.swift
//  Pods
//
//  Created by GeghamHarutyunyan on 3/7/17.
//
//

import Foundation


public protocol HybridRequestProtocol: CloudRequestProtocol
{
    static func save_Eventually<T>(instance:T ,completion: @escaping (BAResult<T>) -> Void) where T:BAObject
    static func delete_Eventually<T>(instance:T ,completion: @escaping (BAResult<T>) -> Void) where T:BAObject
}

extension BackApp: HybridRequestProtocol{}

extension BAObject
{
    var isEventuallyCall:Bool {
        
        let state = self.stateObjects.first
        
        if state?.type == .Eventually
        {
            return true
        }
        
        return false
    }
}

// MARK: - CRUD
extension HybridRequestProtocol
{
    ///   Create or Update an object in the Cloud Eventually
    ///
    /// - parameter instance: - The object that you are trying to create/update
    /// - parameter completion: - In compilation you will receive either created/updated object or error message
    
    public static func save_Eventually<T>(instance:T ,completion: @escaping (BAResult<T>) -> Void) where T:BAObject
    {
        if T.isLocalClass()
        {
            completion(BAResult.Failure(k.DefaultKeys.Messages.CloudClass, nil))
            return
        }
        
        save_Eventually(instance: instance, serverCallback: false) { (result) in
            completion(BAResult.Success(instance))
        }
    }
    
    static func save_Eventually<T>(instance:T , serverCallback: Bool, completion: @escaping (BAResult<T>) -> Void) where T:BAObject
    {
        if !instance.isCloudObject
        {
            instance.ba_status = BAObjectStatus.New_Eventually.rawValue
        }
        else
        {
            instance.ba_status = BAObjectStatus.Update_Eventually.rawValue
            instance.addNeedToSync(instance.propertiesToUpdate.sorted())
            
            //Sync all changes on ongoing state
            let arr = Set(instance.FieldsToUpdate.components(separatedBy: ","))
            instance.propertiesToUpdate = instance.propertiesToUpdate.union(arr)
        }
        
        let local_result = BackApp.save_Local(instance: instance, preChecks: false)
        //If local creted successfully, try to save already local saved object in to the Cloud
        if local_result.isSuccess
        {
            if !serverCallback
            {
                completion(BAResult.Success(instance))
            }
            
            //Saving the already local saved object into the Cloud
            saveLocalIntoCloud(instance: instance, completion: {_ in
                
                print(instance.FieldsToUpdate)
                if serverCallback
                {
                    completion(BAResult.Success(instance))
                }
            })
        }
        else
        {
            completion(BAResult.Failure("Local create faild", nil))
        }
    }
    
    ///   Delete an object in the Cloud Eventually
    ///
    /// - parameter instance: - The object that you are trying to delete
    /// - parameter completion: - In compilation you will receive either nothing(Success) or error message
    
    public static func delete_Eventually<T>(instance:T, completion: @escaping (BAResult<T>) -> Void) where T:BAObject
    {
        if T.isLocalClass()
        {
            completion(BAResult.Failure(k.DefaultKeys.Messages.CloudClass, nil))
            return
        }
        
        if instance.isNewObject
        {
            completion(BAResult.Failure(k.DefaultKeys.Messages.NewObject, nil))
            return
        }
        
        // If object is Local object only
        if instance.isLocalObject
        {
            completion(BackApp.delete_Local(instance: instance, preChecks: false))
        }
        else
        {
            // Create Local objcet befor trying send to the cloud
            instance.ba_status = BAObjectStatus.Delete.rawValue
            
            let local_result = BackApp.update_Local(instance: instance)
            
            //If local creted successfully, try to save already local saved object in to the Cloud
            if local_result.isSuccess
            {
                completion(BAResult.Success(instance))
                
                //Updating the already local updated object into the Cloud
                deleteLocalFromCloud(instance: instance, completion: {_ in})
            }
            else
            {
                completion(BAResult.Failure("Local Delete faild", nil))
            }
        }
    }
}

extension HybridRequestProtocol
{
    static func saveLocalIntoCloud<T>(instance: T, completion: @escaping (BAResult<T>) -> Void) where T:BAObject
    {
        //Set call Eventually
        instance.LocalUsedProperties[k.DefaultKeys.Eventually_Call] = true
        
        //Creating the already local created object into the Cloud
        save(instance: instance, completion: { cloud_Result in
            
            // If object Cloud save success
            if cloud_Result.isSuccess
            {
                instance.ba_status = BAObjectStatus.Sync.rawValue
                instance.FieldsToUpdate = ""
                
                let eventually_Result = BackApp.update_Local(instance: instance)
                if eventually_Result.isSuccess
                {
                    print(instance)
                    print("\(instance._id): SYNC")
                }
            }
            else
            {
                print(cloud_Result)
                if instance.isLocalObject
                {
                    instance.ba_status = BAObjectStatus.New.rawValue
                }
                else
                {
                    instance.ba_status = BAObjectStatus.Update.rawValue
                }
                
                let eventually_Result = BackApp.update_Local(instance: instance)
                if eventually_Result.isSuccess
                {
                    print("Unable to Create/Update object, will try again later.At the mement it was saved localy.")
                }
            }
            
            completion(cloud_Result)
        })
    }
    
    static func deleteLocalFromCloud<T>(instance: T, completion: @escaping (BAResult<T>) -> Void) where T:BAObject
    {
        //Delete the already local created object from the Cloud
        delete(instance: instance, completion: { cloud_Result in
            
            // If object Cloud save success
            if cloud_Result.isSuccess
            {
                _ = BackApp.delete_Local(instance: instance, preChecks: false)
                print("\(instance._id): DELETED")
            }
            else
            {
                //
            }
            
            completion(cloud_Result)
        })
    }
}
