//
//  BAUserManager.swift
//  Pods
//
//  Created by GeghamHarutyunyan on 2/8/17.
//
//

import Foundation
import GRDB

extension BAUser
{
    class func setCurrentUser<T>(user:T) where T:BAUser
    {
        UserDefaults.standard.setValue(user._id, forKey: k.DefaultKeys.currentUser_id)
        UserDefaults.standard.setValue(user.username, forKey: k.DefaultKeys.currentUser_Name)
        UserDefaults.standard.synchronize()
        
        if !user.isEventuallyCall
        {
            try! user.saveLocal()
        }
    }
    
    class func removeUser()
    {
        UserDefaults.standard.removeObject(forKey: "token")
        BAObject.MindData.removeAll()
        if let user_id = BAUser.currentUserID
        {
            UserDefaults.standard.removeObject(forKey: k.DefaultKeys.currentUser_id)
            UserDefaults.standard.removeObject(forKey: k.DefaultKeys.currentUser_Name)
            UserDefaults.standard.synchronize()
            
            let user = BAUser()
            user._id = user_id
            _ = try! user.deleteLocal()
        }
    }
    
    public class func currentUser() -> Self? {
        return  currentUser(type: self)
    }
    
    private class func currentUser<T>(type: T.Type) -> T? where T:BAUser
    {
        // By key
        if let user_id = BAUser.currentUserID
        {
            let user = T.instance(objType: type, _id: user_id, local_id: nil)
            if !user.isNewObject
            {
                return user
            }
            
            user.ignoreObserving {
                user._id = user_id
            }
            
            let query = BAQuery(type)
            if user.fetchSingle(query)
            {
                return user
            }
            
            return nil
        }
        else if BackApp.isAnonymous
        {
            BAUser.createAnonymous()
            return T.currentUser()
        }
        
        return nil
    }
    
    fileprivate class func createAnonymous()
    {
        let uuid = UUID().uuidString
        
        let type = BackApp.getRegisteredUserType()
        let user = type.init()
        user.username = "\(uuid)\(k.DefaultKeys.anonymous_suffix)"
        user.password = uuid
        user.ba_status = BAObjectStatus.New_Eventually.rawValue
        
        let result = BackApp.create_Local(instance: user)
        if result.isSuccess
        {
            BAUser.setCurrentUser(user: user)
        }
        
        BackApp.registration(newUser: user) { (result) in
            
            if result.isSuccess
            {
                user.ba_status = BAObjectStatus.Sync.rawValue
            }
            else
            {
                user.ba_status = BAObjectStatus.New.rawValue
            }
            
            _ = BackApp.update_Local(instance: user)
        }
    }
}


extension BackApp
{
    //MARK: - Configurations
    
    public class func enableAnonymous()
    {
        BackApp.isAnonymous = true
        
        if (BAUser.currentUserID == nil)
        {
            BAUser.createAnonymous()
        }
    }
    
    //MARK: - Helpers
    
    class func getRegisteredUserType() -> BAUser.Type
    {
        for reg in BackApp.regSubclasses {
            if reg.isSubclass(of: BAUser.self)
            {
                return reg as! BAUser.Type
            }
        }
        
        return BAUser.self
    }
}
