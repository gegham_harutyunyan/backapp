//
//  Database.swift
//  Test_Swift
//
//  Created by GeghamHarutyunyan on 2/13/17.
//  Copyright © 2017 WAY4APP. All rights reserved.
//

import GRDB
import UIKit

// The shared database queue.
var dbQueue: DatabaseQueue!

func setupDatabase(_ application: UIApplication) throws {
    
    // Connect to the database
    // See https://github.com/groue/GRDB.swift/#database-connections
    
    let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! as NSString
    let databasePath = documentsPath.appendingPathComponent("db.sqlite")
    print(databasePath)
    
    var config = Configuration()
    config.foreignKeysEnabled = false
    //config.trace = { print($0) }     // Prints all SQL statements
    
    dbQueue = try DatabaseQueue(path: databasePath, configuration: config)
    
    
    // Be a nice iOS citizen, and don't consume too much memory
    // See https://github.com/groue/GRDB.swift/#memory-management
    
    dbQueue.setupMemoryManagement(in: application)
}


func enableFK(_ enable: Bool) throws
{
    //sqlite3_exec(connection, "PRAGMA foreign_keys = on", NULL, NULL, NULL);
    try dbQueue.write { db in
        try enableFK(enable, db: db)
    }
}

func enableFK(_ enable: Bool, db: Database) throws
{
    if enable
    {
        try db.execute("PRAGMA foreign_keys = ON")
    }
    else
    {
        try db.execute("PRAGMA foreign_keys = OFF")
    }
}
