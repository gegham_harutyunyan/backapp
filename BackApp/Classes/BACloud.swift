//
//  BACloud.swift
//  Pods
//
//  Created by GeghamHarutyunyan on 5/23/17.
//
//

import Foundation
import Alamofire

@objc
public class BACloud : NSObject
{
    public class func callFunction(with name: String, with parameters: Array<Any>, completion: @escaping (BAResult<Bool>) -> Void)
    {
        if var request = try? URLRequest(url: BARequestType.FunctionURL(of: name), method: .post, headers: BackApp.getRequestHeaders())
        {
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = try! JSONSerialization.data(withJSONObject: parameters)
            
            Alamofire.request(request)
                .responseJSON { response in
                    
                    
                    switch response.result {
                    case .failure(let error):
                        print(error)
                        
                        if let data = response.data, let responseString = String(data: data, encoding: .utf8) {
                            print(responseString)
                            completion(.Failure(responseString, response.response?.statusCode))
                        }
                    case .success(let responseObject):
                        print(responseObject)
                        completion(.Success(true))
                    }
            }
        }
    }
}
