//
//  BackApp.swift
//  BackApp
//
//  Created by GeghamHarutyunyan on 1/12/17.
//  Copyright © 2017 WAY4APP. All rights reserved.
//

import UIKit

fileprivate let sharedApp = BackApp()

@objc
public class BackApp: NSObject
{
    // RootURL/collectionName - native classes
    // RootURL/classes/collectionName - dynamic classes
    // RootURL/actionName - actions (user register is post)
    
    fileprivate static let RootURL = "http://84.22.110.39:9999/api/\(BackApp.appVersion)"
    static var applicationKey: String!
    static var applicationID: String!
    static var appVersion = 1
    
    // Registerd Subclasses
    static var regSubclasses: [BAObject.Type]!
    
    // Reachability
    public let reachability = Reachability()!
    
    // Configurations
    static var isAnonymous = false
    
    // Sync process
    static var savedObjects = Dictionary<String, BAObject>()
    static let cloudQueue = DispatchQueue(label: "com.BackApp.Queue.Cloud", qos: .utility)
    static let operationsQueue = DispatchQueue(label: "com.BackApp.Queue.Operations", qos: .utility)
    static let syncQueue = DispatchQueue(label: "com.BackApp.Queue.Sync", qos: .utility)
    static var isSync = false
    
    public class var sharedInstance: BackApp {
        return sharedApp
    }
    
    public class func set(_ application:UIApplication, applicationID: String, applicationKey: String, version: Int)
    {
        BackApp.applicationKey = applicationKey
        BackApp.applicationID = applicationID
        BackApp.appVersion = version
        try! setupDatabase(application)
        
        //Start sync process
        BackApp.sharedInstance.startSyncProcess()
    }
}

enum BAActions: String
{
    case Register, Login, ResetPassword, SendPush
}

enum BANativeClasses: String
{
    case User
    case File
    
    static let nativeClasses = [BANativeClasses.User.rawValue,
                                BANativeClasses.File.rawValue]
}

enum BARequestType: String
{
    case Native, Dynamic, Action, Function
    
    static func FunctionURL(of name: String) -> String
    {
        return "\(BackApp.RootURL)/function/\(name)"
    }
    
    static func URL(of `class`: String? = nil, action: BAActions? = nil) -> String
    {
        if let className = `class`
        {
            if BANativeClasses.nativeClasses.contains(className)
            {
                return "\(BackApp.RootURL)/\(className)/"
            }
            else
            {
                return "\(BackApp.RootURL)/classes/\(className)/"
            }
        }
        else if let actionType = action
        {
            if actionType == .Register
            {
                return "\(BackApp.RootURL)/\(BANativeClasses.User.rawValue)/"
            }
            else
            {
                return "\(BackApp.RootURL)/action/\(actionType.rawValue)/"
            }
        }
        else
        {
            return ""
        }
    }
}
