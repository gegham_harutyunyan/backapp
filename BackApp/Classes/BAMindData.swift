//
//  BAGlobalData.swift
//  Pods
//
//  Created by GeghamHarutyunyan on 3/9/17.
//
//

import Foundation

extension BAObject
{
    class func keepInMind<T>(obj: T) where T: BAObject
    {
        let SELF = type(of:obj as BAObject)
        
        if obj.isCloudObject
        {
            if (BAObject.MindData["\(SELF.mapName()).\(obj._id)"] as? T) == nil
            {
                //When syncing localDB->Cloud
                if  let local_id = obj.local_id?.value,
                    let mem_obj = BAObject.MindData["\(SELF.mapName()).\(local_id)"] as? T
                {
                    BAObject.MindData.removeValue(forKey: "\(SELF.mapName()).\(local_id)")
                    BAObject.MindData["\(SELF.mapName()).\(obj._id)"] = mem_obj
                }
                else
                {
                    BAObject.MindData["\(SELF.mapName()).\(obj._id)"] = obj
                }
            }
        }
        else
        {
            if let local_id = obj.local_id?.value
            {
                if (BAObject.MindData["\(SELF.mapName()).\(local_id)"] as? T) == nil
                {
                    BAObject.MindData["\(SELF.mapName()).\(local_id)"] = obj
                }
            }
        }
    }
    
    class func instance<T>(objType: T.Type, _id: String, local_id: BAInteger?) -> T where T: BAObject
    {
        if let mem_obj = BAObject.MindData["\(objType.mapName()).\(_id)"] as? T
        {
            return mem_obj
        }
        
        var local_id = local_id
        if local_id == nil
        {
            local_id = BAObject.getLocalID(of: objType, serverID: _id)
        }
            
        if let loc_id = local_id?.value
        {
            let key = "\(objType.mapName()).\(loc_id)"
            if let mem_obj = BAObject.MindData[key] as? T
            {
                return mem_obj
            }
        }
        
        return T()
    }
}
