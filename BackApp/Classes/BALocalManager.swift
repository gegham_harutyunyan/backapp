//
//  File.swift
//  Pods
//
//  Created by GeghamHarutyunyan on 3/3/17.
//
//

import Foundation
import GRDB

//MARK: - LOCAL STATMENT

extension BAQuery
{
    mutating func getSQLStatment() -> String
    {
        let obj = type.self.init()
        
        /////////////// Include key ///////////////
        var pointers = self.getIncludedSubdocs().pointer
        ///////////////////////////////////////////
        
        var leftJoins = [String : String]()
        pointers = pointers.map { (element) in
            
            if let pointerTypeName = obj.getTypeOfProperty(name: element)?.clearType()
            {
                let typeObj = type.getTypeObject(of: pointerTypeName)
                var join = ""
                if leftJoins.keys.contains(pointerTypeName)
                {
                    join = leftJoins[pointerTypeName]!
                    join += " OR \(typeObj.mapName())._id = \(type.mapName()).\(element)"
                }
                else
                {
                    join = "LEFT JOIN \(typeObj.mapName()) ON \(typeObj.mapName())._id = \(type.mapName()).\(element)"
                }
                
                if !join.isEmpty
                {
                    leftJoins[pointerTypeName] = join
                }
                
                return typeObj.mapName()+".*"
            }
            
            return ""
        }
        
        pointers = pointers.unique()
        if !pointers.contains(type.mapName())
        {
            pointers.insert("\(type.mapName()).*", at: 0)
        }
        if let index = pointers.index(of: "")
        {
            pointers.remove(at: index)
        }
        
        let tablesToSelect = pointers.joined(separator: ",")
        let tablesToJoins = leftJoins.values.sorted().joined(separator: " ")
        
        let sql = "SELECT \(tablesToSelect) " +
            "FROM \(type.mapName()) " +
            tablesToJoins + " "
        
        return sql
    }
}

extension BackApp
{
    class func fetchAll<T>(of type: T.Type, query: BAQuery?) -> BAResult<[T]> where T:BAObject
    {
        var query = query
        
        if (query == nil)
        {
            query = BAQuery(type)
        }
        
        let whereSQL =  "WHERE NOT \(type.mapName()).\(k.DefaultKeys.status) = \"\(BAObjectStatus.Delete.rawValue)\" "
        let sql = query!.getSQLStatment() + whereSQL
        if let rows = try? dbQueue.inDatabase { db in try Row.fetchAll(db, sql) }
        {
            var array = [T]()
            for row in rows
            {
                if let obj_ID = row[k.DefaultKeys.ServerUniqueID] as? String
                {
                    let loc_ID = BAInteger((row[k.DefaultKeys.local_PK] as! Int64))
                    
                    let obj = type.instance(objType: type, _id: obj_ID, local_id: loc_ID)
                    obj.localInstance(rows: [row],query: query)
                    
                    //Keep In Mind
                    type.keepInMind(obj: obj)
                    
                    array.append(obj)
                }
            }
            
            return BAResult.Success(array)
            
        }
        else
        {
            return BAResult.Failure("Get Faild.", nil)
        }
    }
}

extension BAObject
{
    //MARK: - Defaults
    
    func setLocalDefaults()
    {
        self.ignoreObserving {
            
            self._id = self._id.replacingOccurrences(of: k.DefaultKeys.temp_prefix, with: k.DefaultKeys.local_prefix)
            let date = Date()
            self.createdAt = date
            self.updatedAt = date
            if self.ba_status == BAObjectStatus.None.rawValue
            {
                self.ba_status = BAObjectStatus.Local.rawValue
            }
        }
    }
    
    //MARK: - Operations: Save/...
    
    func saveLocal() throws
    {
        dbQueue.inDatabase { db in
            
            do{
                //object exists in db
                if self.local_id == nil
                {
                    self.local_id = try getLocalID(db)
                }
                
                // update/create
                if self.local_id == nil
                {
                    try insert(db)
                }
                else
                {
                    if let activeKeys = LocalUsedProperties[k.DefaultKeys.Active_Keys] as? BAKeys
                    {
                        //enable FK, if object is not from sync process
                        if !self.isSyncProcess()
                        {
                            try enableFK(true, db: db)
                        }
                        
                        try update(db, columns: activeKeys.allKeys())
                        
                        //desable FK, if object is not from sync process
                        if !self.isSyncProcess()
                        {
                            try enableFK(false, db: db)
                        }
                    }
                }
            }
            catch
            {
                print(error)
            }
        }
    }
    
    private func update(_ db: Database, columns: Set<String>) throws
    {
        //Save Pointers
        let activeKeys = LocalUsedProperties[k.DefaultKeys.Active_Keys] as! BAKeys
        try savePointers(db, ponters: activeKeys.pointers)
        
        let objType = Swift.type(of:self)
        
        try performUpdate(db, columns: columns.union(objType.onlyLocalProperties()))
        
        // SAVE RELATIONS
        try saveRelations(db)
    }
    
    private func insert(_ db: Database) throws
    {
        //Save Pointers
        if !isSyncProcess()
        {
            try savePointers(db, ponters: Set(getPointersAndRelations().pointers))
        }
        
        try performInsert(db)
        
        //The object saved first time
        self.local_id = BAInteger(db.lastInsertedRowID)
        
        //Keep IN Mind
        let objType = type(of:self)
        objType.keepInMind(obj: self)
        
        // SAVE RELATIONS
        try saveRelations(db)
    }
    
    func savePointers(_ db: Database, ponters: Set<String>) throws
    {
        for pointerName in ponters
        {
            if let pointer = self[pointerName] as? BAObject
            {
                if pointer.local_id == nil
                {
                    pointer.local_id = try pointer.getLocalID(db)
                }
                
                //Object local status
                self.setLocalStatus(of: pointer)
                
                //Insert || update
                if pointer.local_id == nil
                {
                    if pointer.isNewObject
                    {
                        pointer.setLocalDefaults()
                    }
                    
                    try pointer.insert(db)
                }
                else
                {
                    if let activeKeys = pointer.LocalUsedProperties[k.DefaultKeys.Active_Keys] as? BAKeys
                    {
                        let objType = Swift.type(of:pointer)
                        try pointer.update(db, columns: activeKeys.allKeys().union(objType.onlyLocalProperties()))
                    }
                }
            }
        }
    }

    func saveRelations(_ db: Database) throws
    {
        // User_myBooks
        if let activeKeys = self.LocalUsedProperties[k.DefaultKeys.Active_Keys] as? BAKeys
        {
            if activeKeys.relations.count == 0
            {
                if (LocalUsedProperties[k.DefaultKeys.Server_Local] as? Bool) != nil
                {
                    LocalUsedProperties.removeValue(forKey: k.DefaultKeys.Server_Local)
                }
            }
            else
            {
                for relName in activeKeys.relations
                {
                    if (LocalUsedProperties[k.DefaultKeys.Server_Local] as? Bool) != nil
                    {
                        try saveStoredRelations(db, relationName: relName)
                        LocalUsedProperties.removeValue(forKey: k.DefaultKeys.Server_Local)
                    }
                    else
                    {
                        let ba_relation = self.value(forKey: relName) as! BARelation
                        
                        if self.ba_status != BAObjectStatus.Local.rawValue,
                            !self.ba_status.contains(k.DefaultKeys.eventually_suffix)
                        {
                            //Update not added relations statuses, in case if save_eventually faild saving into the cloud
                            if let relObjects = (self[relName] as? Array<BAObject>), relObjects.count > 0
                            {
                                for obj in relObjects
                                {
                                    if obj.ba_status.contains(k.DefaultKeys.eventually_suffix)
                                    {
                                        //Object local status
                                        self.setLocalStatus(of: obj)
                                        
                                        if let activeKeys = obj.LocalUsedProperties[k.DefaultKeys.Active_Keys] as? BAKeys
                                        {
                                            let objType = Swift.type(of:obj)
                                            try obj.update(db, columns: activeKeys.allKeys().union(objType.onlyLocalProperties()))
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            //Add relations
                            let relationState = self.relationState(relName)
                            let added = ba_relation.added.filter({ !relationState.added.contains($0._id) })
                            for obj in added
                            {
                                //var obj = obj
                                if obj.local_id == nil
                                {
                                    obj.local_id = try obj.getLocalID(db)
                                }
                                
                                //Object local status
                                self.setLocalStatus(of: obj)
                                
                                // Insert || Update
                                if obj.local_id == nil
                                {
                                    if obj.isNewObject
                                    {
                                        obj.setLocalDefaults()
                                    }
                                    
                                    try obj.insert(db)
                                }
                                else
                                {
                                    if let activeKeys = obj.LocalUsedProperties[k.DefaultKeys.Active_Keys] as? BAKeys
                                    {
                                        let objType = Swift.type(of:obj)
                                        try obj.update(db, columns: activeKeys.allKeys().union(objType.onlyLocalProperties()))
                                    }
                                }
                                
                                let objType = type(of:self)
                                self.insertRelationData(db, objB: obj, relationTypeName: "\(objType.mapName())_\(relName)", status: self.ba_status)
                                
                                ba_relation.storedType.keepInMind(obj: obj)
                                ba_relation.store(object: obj)
                            }
                            
                            //Remove Relations
                            let deleted = ba_relation.removed.filter({ !relationState.deleted.contains($0._id) })
                            let deleted_IDS = deleted.flatMap({ (obj) -> String? in return obj._id })
                            
                            if deleted.count > 0
                            {
                                self.removeRelations(of: relName, relation: ba_relation, deletedObjects: deleted_IDS, db: db)
                                
                                //Notification about remove relation objects
                                let relationDeleteSubscribe = "\(String(ObjectIdentifier(self).hashValue) + relName + "RelationObjectRemoved")"
                                NotificationCenter.default.post(name: Notification.Name(relationDeleteSubscribe),
                                                                object: nil,
                                                                userInfo: [k.DefaultKeys.NotyKey : ba_relation.removed])
                                
                            }
                            
                            
                            //Remove just added elements
                            //Maybe save and save_eventually were called in row, so we don't mixup two different save relations
                            ba_relation.added = ba_relation.added.filter({ !added.contains($0) })
                            
                            //Remove deleted relation objects from Object
                            ba_relation.removeDeletedKeys(deleted_IDS)
                        }
                    }
                }
            }
        }
    }
    
    func saveStoredRelations(_ db: Database,relationName: String) throws
    {
        if let relObjects = (self[relationName] as? Array<BAObject>), relObjects.count > 0
        {
            for obj in relObjects
            {
                if obj.local_id == nil
                {
                    obj.local_id = try obj.getLocalID(db)
                }
                
                //Object local status
                self.setLocalStatus(of: obj)
                
                if obj.local_id == nil
                {
                    if obj.isNewObject
                    {
                        obj.setLocalDefaults()
                    }
                    try obj.insert(db)
                }
                else
                {
                    if let activeKeys = obj.LocalUsedProperties[k.DefaultKeys.Active_Keys] as? BAKeys
                    {
                        let objType = Swift.type(of:obj)
                        try obj.update(db, columns: activeKeys.allKeys().union(objType.onlyLocalProperties()))
                    }
                }
                
                let objType = type(of:self)
                self.insertRelationData(db, objB: obj, relationTypeName: "\(objType.mapName())_\(relationName)", status: BAObjectStatus.Sync.rawValue)
            }
        }
    }
    
    func setLocalStatus(of obj: BAObject)
    {
        if obj.local_id == nil || obj.isLocalObject
        {
            if self.ba_status == BAObjectStatus.Update_Eventually.rawValue
            {
                obj.ba_status = BAObjectStatus.New_Eventually.rawValue
            }
            else if self.ba_status == BAObjectStatus.Update.rawValue
            {
                obj.ba_status = BAObjectStatus.New.rawValue
            }
            else if self.ba_status != BAObjectStatus.Delete.rawValue
            {
                obj.ba_status = self.ba_status
            }
        }
        else
        {
            if self.ba_status != BAObjectStatus.Delete.rawValue
            {
                obj.ba_status = self.ba_status
            }
        }
    }
    
    func deleteLocal() throws -> Bool
    {
        try! enableFK(true)
        let isSuccess = try! dbQueue.inDatabase { db -> Bool in
            
            if self.local_id == nil
            {
                self.local_id = try self.getLocalID(db)
            }
            
            return try self.delete(db)
        }
        
        try! enableFK(false)
        
        return isSuccess
    }
    
    func fetchSingle(_ query: BAQuery? = nil) -> Bool
    {
        let selfType = type(of:self)
        var query = query
        
        if (query == nil)
        {
            query = BAQuery(selfType)
        }
        
        let whereSQL =  "WHERE \(selfType.mapName())._id = \"\(self._id)\" " +
                        "AND NOT \(selfType.mapName()).\(k.DefaultKeys.status) = \"\(BAObjectStatus.Delete.rawValue)\" "
        let sql = query!.getSQLStatment() + whereSQL
        
        if let rows = try? dbQueue.inDatabase { db in try Row.fetchAll(db, sql) }, rows.count > 0
        {
            self.localInstance(rows: rows, query: query!)
            
            //Keep In mind
            BAObject.keepInMind(obj: self)
            
            return true
        }
        
        return false
    }
    
    private func delete(_ db: Database) throws -> Bool
    {
        return try performDelete(db)
    }
    
    //MARK: - Relations/Pointers
    
    func selectRelationData<T>(_ relType:T.Type ,relationName: String) -> [T] where T:BAObject
    {
        let selfType = type(of:self)
        
        let relationTableName = selfType.getRelationName(of: relType)
        let relColumns = selfType.getRelationColumnNames(of: relType)
        
        let whereSQL =  "LEFT JOIN \(relationTableName) " +
                        "ON \(relationTableName).\(relColumns.relCol) = \(relType.mapName())._id " +
                        "WHERE \(relationTableName).type = \"\(selfType.mapName())_\(relationName)\" " +
                        "AND \(relationTableName).\(relColumns.selfCol) = \"\(self._id)\" " +
                        "AND NOT \(relType.mapName()).\(k.DefaultKeys.status) = \"\(BAObjectStatus.Delete.rawValue)\" "
        
        var filter = BAQuery(relType)
        let sql = filter.getSQLStatment() + whereSQL
        
        var relData = Array<T>()
        if let relRows = try? dbQueue.inDatabase { db in try Row.fetchAll(db, sql) }
        {
            
            for row in relRows
            {
                if (row[k.DefaultKeys.ServerUniqueID] as? String) != nil
                {
                    //Try to get it from memory
                    let server_id = row[k.DefaultKeys.ServerUniqueID] as! String
                    let loc_ID = BAInteger((row[k.DefaultKeys.local_PK] as! Int64))
                    let object = relType.instance(objType: relType, _id: server_id, local_id: loc_ID)
                    
                    if !object.isNewObject
                    {
                        relData.append(object)
                    }
                    else
                    {
                        object.localInstance(rows: [row],isNested: true)
                        relData.append(object)
                        
                        //Keep In mind
                        BAObject.keepInMind(obj: object)
                    }
                }
            }
        }
        
        return relData
    }
    
    func insertRelationData(_ db:Database, objB: BAObject, relationTypeName: String, status: String)
    {
        let typeA = type(of:self)
        let typeB = type(of:objB)
        let relationTableName = typeA.getRelationName(of: typeB)
        let relColumns = typeA.getRelationColumnNames(of: typeB)
        
        var relStatus = BAObjectStatus.New.rawValue
        if status == BAObjectStatus.Sync.rawValue || status == BAObjectStatus.Sync.rawValue
        {
            relStatus = status
        }
        
        try! db.execute(
            "INSERT INTO \(relationTableName) (\(relColumns.selfCol), \(relColumns.relCol), type, \(k.DefaultKeys.status)) " +
            "VALUES (?, ?, ?, ?)",
            arguments: [self._id, objB._id, relationTypeName, relStatus])
    }
    
    func selectOnlyLocalRelations(typeB: BAObject.Type, relationName: String) -> (addRel: [String], remRel: [String])
    {
        let typeA = type(of:self)
        let relationTableName = typeA.getRelationName(of: typeB)
        let relColumns = typeA.getRelationColumnNames(of: typeB)
        var addRel = [String]()
        var remRel = [String]()
        
        let sql = "SELECT * FROM \(relationTableName) WHERE type = \"\(typeA.mapName())_\(relationName)\""
            + " AND \(relColumns.selfCol) = \"\(self._id)\""
            + " AND \(k.DefaultKeys.status) != \"\(BAObjectStatus.Sync.rawValue)\""
        if let relRows = try? dbQueue.inDatabase { db in try Row.fetchAll(db, sql) }
        {
            for row in relRows
            {
                let status = row[k.DefaultKeys.status] as! String
                let val = row[relColumns.relCol] as! String
                if status == BAObjectStatus.New.rawValue
                {
                    addRel.append(val)
                }
                else
                {
                    remRel.append(val)
                }
            }
        }
        
        return (addRel, remRel)
    }
    
    //MARK: - Remove Relation
    
    func removeRelations(of relationName: String, relation: BARelation, deletedObjects: [String])
    {
        dbQueue.inDatabase { db in
            
            removeRelations(of: relationName, relation: relation, deletedObjects: deletedObjects, db: db)
        }
    }
    
    func removeRelations(of relationName: String, relation: BARelation, deletedObjects: [String], db: Database)
    {
        // relationName     : myBooks
        // relationTypeName : User_myBooks
        // selfType         : MyUser.Type
        // relationObjType  : Book.Type
        
        let selfType = type(of: self)
        let relationObjType = relation.storedType
        let relationTypeName = "\(selfType.mapName())_\(relationName)"
        let relationTableName = selfType.getRelationName(of: relationObjType!)
        let relColumns = selfType.getRelationColumnNames(of: relationObjType!)
        
        if deletedObjects.count > 0
        {
            var objects = Set<String>()
            for obj_ID in deletedObjects
            {
                objects.insert("\"\(obj_ID)\"")
            }
            
            do{
                if self.isEventuallyCall
                {
                    for obj_ID in deletedObjects
                    {
                        try! db.execute(
                            "INSERT INTO \(relationTableName) (\(relColumns.selfCol), \(relColumns.relCol), type, \(k.DefaultKeys.status)) " +
                            "VALUES (?, ?, ?, ?)",
                            arguments: [self._id, obj_ID, relationTypeName, BAObjectStatus.Delete.rawValue])
                    }
                }
                else
                {
                    try db.execute("DELETE FROM \(relationTableName) WHERE \(relColumns.selfCol) = \"\(self._id)\"" +
                        " AND type = \"\(relationTypeName)\"" +
                        " AND \(relColumns.relCol) IN (\(objects.joined(separator: ",")))")
                }
            }
            catch
            {
                print(error)
            }
        }
    }
    
    func removeAllRelations(of relationName: String, relation: BARelation)
    {
        let selfType = type(of: self)
        let relationObjType = relation.storedType
        let relationTypeName = "\(selfType.mapName())_\(relationName)"
        let relationTableName = selfType.getRelationName(of: relationObjType!)
        let relColumns = selfType.getRelationColumnNames(of: relationObjType!)
        
        
        dbQueue.inDatabase { db in
            
            do{
                try db.execute("DELETE FROM \(relationTableName) WHERE \(relColumns.selfCol) = \"\(self._id)\"" +
                    " AND type = \"\(relationTypeName)\"")
            }
            catch
            {
                print(error)
            }
        }
    }

    //MARK: - Simple Operations
    
    func getLocalID(_ db:Database) throws -> BAInteger?
    {
        if let local_id = try Int.fetchOne(db, "SELECT \(k.DefaultKeys.local_PK) FROM \(type(of:self).mapName()) WHERE _id = \"\(self._id)\"")
        {
            return BAInteger(Int64(local_id))
        }

        return nil
    }
    
    class func getLocalID(of type:BAObject.Type, serverID: String) -> BAInteger?
    {
        if let local_id = try? dbQueue.inDatabase { db in
            
            try Int.fetchOne(db, "SELECT \(k.DefaultKeys.local_PK) FROM \(type.mapName()) WHERE _id = \"\(serverID)\"")
        }
        {
            if local_id != nil
            {
                return BAInteger(Int64(local_id!))
            }
        }
        
        return nil
    }
}
