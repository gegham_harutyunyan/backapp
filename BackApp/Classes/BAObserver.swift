//
//  BAObserver.swift
//  Pods
//
//  Created by GeghamHarutyunyan on 6/7/17.
//
//

import Foundation

final class BAObserver:NSObject
{
    private var kvoContext: UInt8 = 1
    
    required override init()
    {
        super.init()
    }
    
    func startObserving<T>(_ object: T) where T: BAObject
    {
        for prop in object.propertyNames(includeLocals: false)
        {
            object.addObserver(self, forKeyPath: prop,
                               options: [.new,.old], context: &kvoContext)
            
            //BAObservable types
            startObservingBAObservableType(prop, of: object)
        }
    }
    
    func startObservingBAObservableType<T>(_ property: String, of object: T) where T: BAObject
    {
        //BAObservable types
        if let val = object.value(forKey: property) as? BAObservableType
        {
            val.update = {
                
                if (object.LocalUsedProperties[k.DefaultKeys.Observing_Block] == nil)
                {
                    object.propertiesToUpdate.insert(property)
                    
                    object.notify(property: property)
                }
                
                //Add active property
                let keys = object.LocalUsedProperties[k.DefaultKeys.Active_Keys] as? BAKeys
                if object.getPointersAndRelations().relations.contains(property)
                {
                    keys?.relations.insert(property)
                }
                else
                {
                    keys?.standart.insert(property)
                }
            }
        }
    }
    
    func stopObserving<T>(_ object: T) where T:BAObject
    {
        for prop in object.propertyNames(includeLocals: false)
        {
            object.removeObserver(self, forKeyPath: prop)
        }
    }
    
    @objc
    open override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        if (change?[NSKeyValueChangeKey.newKey]) != nil
        {
            if let obj = object as? BAObject
            {
                //BAObservable types
                startObservingBAObservableType(keyPath!, of: obj)
                
                if (obj.LocalUsedProperties[k.DefaultKeys.Observing_Block] == nil)
                {
                    obj.propertiesToUpdate.insert(keyPath!)
                    obj.notify(property: keyPath)
                }
                
                //Add active property
                let keys = obj.LocalUsedProperties[k.DefaultKeys.Active_Keys] as? BAKeys
                if obj.getPointersAndRelations().pointers.contains(keyPath!)
                {
                    keys?.pointers.insert(keyPath!)
                }
                else
                {
                    keys?.standart.insert(keyPath!)
                }
                
            }
        }
        else if (change?[NSKeyValueChangeKey.oldKey]) != nil
        {
            
        }
        else
        {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
}
