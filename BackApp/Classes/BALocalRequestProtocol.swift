//
//  BALocalRequestProtocol.swift
//  Pods
//
//  Created by GeghamHarutyunyan on 3/7/17.
//
//

import Foundation

//local, synced, deleted, updated, new
enum BAObjectStatus: String
{
    case None
    case Local
    case New_Eventually
    case New
    case Update_Eventually
    case Update
    case Delete
    case Sync
}

public protocol LocalRequestProtocol
{
    static func getAll_Local<T>(items forType: T.Type, filter: BAQuery?) -> BAResult<[T]> where T:BAObject
    
    static func save_Local<T>(instance:T) -> BAResult<T> where T:BAObject
    
    static func delete_Local<T>(instance:T) -> BAResult<T> where T:BAObject
    static func get_Local<T>(instance:T, query: BAQuery?) -> BAResult<T> where T:BAObject
}

extension BackApp :LocalRequestProtocol{}

// MARK: - Filter
extension LocalRequestProtocol
{
    public static func getAll_Local<T>(items forType: T.Type, filter: BAQuery? = nil) -> BAResult<[T]> where T:BAObject
    {
        if !T.isLocalClass()
        {
            return BAResult.Failure(k.DefaultKeys.Messages.LocalClass, nil)
        }
        else
        {
            return BackApp.fetchAll(of: forType, query: filter)
        }
    }
}

extension LocalRequestProtocol
{
    ///   Delete an object in the LocalDB
    ///
    /// - parameter instance: - The object that you are trying to delete
    /// - parameter completion: - In compilation you will receive either nothing(Success) or error message
    
    public static func delete_Local<T>(instance:T) -> BAResult<T> where T:BAObject
    {
        return delete_Local(instance: instance, preChecks: true)
    }
    
    static func delete_Local<T>(instance:T, preChecks: Bool) -> BAResult<T> where T:BAObject
    {
        //if object is not local class
        if preChecks
        {
            if !T.isLocalClass()
            {
                return BAResult.Failure(k.DefaultKeys.Messages.LocalClass, nil)
            }
        }
        
        do {
            if(try instance.deleteLocal())
            {
                return BAResult.Success(instance)
            }
            else
            {
                return BAResult.Failure("Delete Faild.", nil)
            }
            
        } catch
        {
            return BAResult.Failure("Delete Faild.", nil)
        }
    }
    
    
    /// Get an object from the LocalDB
    ///
    /// - parameter instance: - The object that you are trying to get
    /// - parameter completion: - In compilation you will receive either full object or error message
    
    public static func get_Local<T>(instance:T, query: BAQuery? = nil) -> BAResult<T> where T:BAObject
    {
        if !T.isLocalClass()
        {
            return BAResult.Failure(k.DefaultKeys.Messages.LocalClass, nil)
        }
        else
        {
            if instance.fetchSingle(query)
            {
                return (BAResult.Success(instance))
            }
            else
            {
                return (BAResult.Failure("There is no object with this ID.", nil))
            }
        }
    }
}

extension LocalRequestProtocol
{
    /// Create or Update an object in the LocalDB
    ///
    /// - parameter instance: - The object that you are trying to create/update
    /// - parameter completion: - In compilation you will receive either created/updated object or error message
    
    public static func save_Local<T>(instance:T) -> BAResult<T> where T:BAObject
    {
        return save_Local(instance: instance, preChecks: true)
    }
    
    static func save_Local<T>(instance:T, preChecks: Bool) -> BAResult<T> where T:BAObject
    {
        //if object is not local class
        if preChecks
        {
            if !T.isLocalClass()
            {
                return BAResult.Failure(k.DefaultKeys.Messages.LocalClass, nil)
            }
        }
        
        if instance.isNewObject
        {
            let result = create_Local(instance: instance)
            return result
        }
        else
        {
            let result = update_Local(instance: instance)
            return result
        }
    }
    
    /// Update an object in the LocalDB
    ///
    /// - parameter instance: - The object that you are trying to update
    /// - parameter completion: - In compilation you will receive either updated object or error message
    
    static func update_Local<T>(instance:T) -> BAResult<T> where T:BAObject
    {
        let date = Date()
        
        if instance.ba_status == BAObjectStatus.Local.rawValue ||
            instance.ba_status == BAObjectStatus.New_Eventually.rawValue
        {
            instance.updatedAt = date
        }
        
        do {
            try instance.saveLocal()
            return BAResult.Success(instance)
        }
        catch
        {
            return BAResult.Failure("Update Faild.", nil)
        }
    }
    
    /// Create an object in the LocalDB
    ///
    /// - parameter instance: - The object that you are trying to create
    /// - parameter completion: - In compilation you will receive either created object or error message
    
    static func create_Local<T>(instance:T) -> BAResult<T> where T:BAObject
    {
        instance.setLocalDefaults()
        
        do {
            try instance.saveLocal()
            return BAResult.Success(instance)
        }
        catch
        {
            return BAResult.Failure("Object creations was faild.", nil)
        }
    }
}

//MARK: - Easier Accesses
public protocol LocalRequestProtocol_Easy
{
    //Easier Access
    func fetchFull_Local() -> BAResult<Bool>
    func fetch_Local(keys: [String]?) -> BAResult<Bool>
}

extension BAObject: LocalRequestProtocol_Easy
{
    public func fetchFull_Local() -> BAResult<Bool>
    {
        let (pointers, relations) = self.getPointersAndRelations()
        let keys = pointers + relations
        
        let result = self.fetch_Local(keys: keys)
        return result
    }
    
    public func fetch_Local(keys: [String]? = nil) -> BAResult<Bool>
    {
        var filter: BAQuery? = nil
        if keys != nil
        {
            let type = Swift.type(of:self)
            filter = BAQuery(type)
            for key in keys!
            {
                filter?.include(key: key)
            }
        }
        
        let result = BackApp.get_Local(instance: self)
        if (result.isSuccess)
        {
            return (.Success(true))
        }
        else
        {
            return (BAResult.Failure("Unable to get object.", nil))
        }
    }
}

