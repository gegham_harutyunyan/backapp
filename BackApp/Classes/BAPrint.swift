//
//  BAPrint.swift
//  Pods
//
//  Created by GeghamHarutyunyan on 3/3/17.
//
//

import Foundation
import SwiftyJSON

// MARK: - Pintable Description
extension BAObject
{
    override open var description: String {
        
        return self.toString()
    }
    
    func toString(_ deepLevel: Int = 0, alreadyPrintedObjs: [String:String] = [:]) -> String
    {
        let deep = deepLevel + 1
        
        var currentDeepLevelSpaces = ""
        var previousDeepLevelSpaces = ""
        let levelSpace = "  "
        for index in 1...deep
        {
            currentDeepLevelSpaces += levelSpace
            
            if index != 1
            {
                previousDeepLevelSpaces += levelSpace
            }
        }
        
        var params = "{"
        
        var printedObjs:[String:String] = [:]
        if (alreadyPrintedObjs[self._id] != nil)
        {
            params += "\n\(currentDeepLevelSpaces)\"_id\" : \(self._id),"
            params += "\n\(currentDeepLevelSpaces)\"loop...\","
        }
        else
        {
            printedObjs[self._id] = self._id
            printedObjs.combineWith(other: alreadyPrintedObjs)
            
            let properties = self.propertyNames(includeLocals: true)
            for prop in properties
            {
                if let val = self[prop]
                {
                    if val is BAObject
                    {
                        if deep > 3
                        {
                            let value = val as! BAObject
                            let type = Swift.type(of:val) as! BAObject.Type
                            let newVal = type.init()
                            newVal.local_id = value.local_id
                            newVal._id = value._id
                            
                            params += "\n\(currentDeepLevelSpaces)\"\(prop)\" : \((newVal).toString(deep, alreadyPrintedObjs: printedObjs)),"
                        }
                        else
                        {
                            params += "\n\(currentDeepLevelSpaces)\"\(prop)\" : \((val as! BAObject).toString(deep, alreadyPrintedObjs: printedObjs)),"
                        }
                    }
                    else if val is Array<BAObject>
                    {
                        var arrStr = ""
                        arrStr += "\n\(currentDeepLevelSpaces)\"\(prop)\" : ["
                        
                        for value in (val as! Array<BAObject>)
                        {
                            if deep > 3
                            {
                                let type = Swift.type(of:value)
                                let newVal = type.init()
                                newVal.local_id = value.local_id
                                newVal._id = value._id
                                
                                arrStr += "\n\(currentDeepLevelSpaces + levelSpace)\(newVal.toString(deep+1,alreadyPrintedObjs: printedObjs)),"
                            }
                            else
                            {
                                arrStr += "\n\(currentDeepLevelSpaces + levelSpace)\(value.toString(deep+1, alreadyPrintedObjs: printedObjs)),"
                            }
                        }
                        
                        
                        arrStr = String(arrStr[..<arrStr.index(before: arrStr.endIndex)])
                        arrStr += "\n\(currentDeepLevelSpaces)],"
                        
                        params += arrStr
                    }
                    else
                    {
                        params += "\n\(currentDeepLevelSpaces)\"\(prop)\" : \(val),"
                    }
                }
            }
        }
        
        params = String(params[..<params.index(before: params.endIndex)])
        params += "\n\(previousDeepLevelSpaces)}"
        
        return params
    }
}
