//
//  BASyncProcess.swift
//  Pods
//
//  Created by GeghamHarutyunyan on 3/8/17.
//
//

import Foundation
import GRDB

//MARK: - Preparing process
extension BackApp
{
    func startSyncProcess()
    {
        networkReachability()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationDidBecomeActive(_:)),
            name: NSNotification.Name.UIApplicationDidBecomeActive,
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationDidEnterBackground(_:)),
            name: NSNotification.Name.UIApplicationDidEnterBackground,
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationWillTerminate(_:)),
            name: NSNotification.Name.UIApplicationWillTerminate,
            object: nil)
    }
    
    func networkReachability()
    {
        reachability.whenReachable = { reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            DispatchQueue.main.async {
                
                if reachability.isReachable
                {
                    print("Reachable")
                    BackApp.isSync = true
                    self.syncNow({
                        BackApp.isSync = false
                    })
                }
                
                if reachability.isReachableViaWiFi {
                    
                    
                } else {
                    
                }
            }
        }
        reachability.whenUnreachable = { reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            DispatchQueue.main.async {
                print("Not reachable")
            }
        }
    }
    
    @objc func applicationDidBecomeActive(_ notification: NSNotification)
    {
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    @objc func applicationDidEnterBackground(_ notification: NSNotification)
    {
        // do something
        reachability.stopNotifier()
    }
    
    @objc func applicationWillTerminate(_ notification: NSNotification)
    {
        // do something
        reachability.stopNotifier()
    }
}

//MARK: - Main process
extension BackApp
{
    func syncNow(_ completion: @escaping () -> Void)
    {
        BackApp.addOperation(BackApp.operationsQueue) {
            
            //In this step new created or updated objects status will be Sync, but they may have unsynced pointers
            let sema = DispatchSemaphore(value: 0);
            
            //check if currentUser local, register
            if BAUser.isCurrentUser_Local
            {
                // First time run
                // Creating user through, BackApp.enableAnonymous()
                let user = BAUser.currentUser()!
                if user.ba_status != BAObjectStatus.New_Eventually.rawValue
                {
                    //fetch pointers, may be needed to update
                    var query = type(of: user).query()
                    query.includePointers()
                    _ = user.fetchSingle(query)
                    
                    user.LocalUsedProperties[k.DefaultKeys.Obj_Sync] = true
                    
                    BackApp.registration(newUser: user) { (result) in
                        
                        if result.isSuccess
                        {
                            user.ba_status = BAObjectStatus.Sync.rawValue
                            
                            _ = BackApp.update_Local(instance: user)
                            
                            BackApp.savedObjects[user._id] = user
                            
                            sema.signal()
                        }
                        else
                        {
                            completion()
                            
                            sema.signal()
                        }
                    }
                    
                    _ = sema.wait(timeout: DispatchTime.distantFuture)
                    
                    //Sync tables
                    self.syncTablesAsync()
                    
                    completion()
                }
            }
            else
            {
                //Sync tables
                self.syncTablesAsync()
                
                completion()
            }
            
        }
        
    }
    
    func syncTablesAsync()
    {
        //Sync all tables main objects (without pointers/ relations)
        for sub in BackApp.regSubclasses
        {
            var query = sub.query()
            query.includePointers()
            
            let sql = query.getSQLStatment()
            
            if let rows = try? dbQueue.inDatabase { db in
                try Row.fetchAll(db, sql +
                    " WHERE \(sub.mapName()).\(k.DefaultKeys.status) IN (\"\(BAObjectStatus.New.rawValue)\",\"\(BAObjectStatus.Update.rawValue)\",\"\(BAObjectStatus.Delete.rawValue)\")")
                }
            {
                self.syncObjectsSync(rows, type: sub, query: query)
            }
        }
        
        //Sync pointers
        let sema = DispatchSemaphore(value: 0);
        for (_, obj) in BackApp.savedObjects
        {
            if obj.ba_status == BAObjectStatus.Update.rawValue
            {
                BackApp.save_Eventually(instance: obj, serverCallback: true, completion: {_ in
                    
                    obj.LocalUsedProperties.removeValue(forKey: k.DefaultKeys.Obj_Sync)
                    sema.signal()
                })
                
                _ = sema.wait(timeout: DispatchTime.distantFuture)
            }
            else
            {
                obj.LocalUsedProperties.removeValue(forKey: k.DefaultKeys.Obj_Sync)
            }
        }
        
        BackApp.savedObjects.removeAll()
    }
    
    func syncObjectsSync(_ rows: [Row], type: BAObject.Type, query: BAQuery)
    {
        guard rows.count > 0 else {
            return
        }
        
        let sema = DispatchSemaphore(value: 0);
        
        for row in rows
        {
            //get from Mind
            let obj = type.init()
            obj.localInstance(rows: [row], query: query)
            
            if obj.ba_status == BAObjectStatus.New.rawValue
            {
                BackApp.syncLocalWithCloud(instance: obj, completion: {_ in sema.signal()})
            }
            else if obj.ba_status == BAObjectStatus.Delete.rawValue
            {
                BackApp.syncLocalDeletedWithCloud(instance: obj, completion: {_ in sema.signal()})
            }
            else if obj.ba_status == BAObjectStatus.Update.rawValue
            {
                BackApp.syncLocalWithCloud(instance: obj, completion: {_ in sema.signal()})
            }
            
            _ = sema.wait(timeout: DispatchTime.distantFuture)
        }
    }
}

// MARK: - Local->Cloud Requests

extension BackApp
{
    static func syncLocalWithCloud<T>(instance: T, completion: @escaping (BAResult<T>) -> Void) where T:BAObject
    {
        instance.LocalUsedProperties[k.DefaultKeys.Obj_Sync] = true
        
        // Get initial _ID, may be Local/Cloud
        let objID = instance._id
        
        var isCreate = false
        if instance.isLocalObject
        {
            isCreate = true
        }
        
        //Creating the already local created object into the Cloud
        save(instance: instance) { (cloud_Result) in
            
            // If object Cloud save success
            if cloud_Result.isSuccess
            {
                let type = Swift.type(of: instance)
                let instance_M = type.instance(objType: type, _id: instance._id, local_id: instance.local_id)
                
                print(instance_M)
                print(instance_M.propertiesToUpdate)
                print(instance_M.FieldsToUpdate)
                
                savedObjects[objID] = instance_M
                
                if instance_M.FieldsToUpdate.isEmpty
                {
                    instance_M.ba_status = BAObjectStatus.Sync.rawValue
                }
                else
                {
                    instance_M.ba_status = BAObjectStatus.Update.rawValue
                }
                
                let eventually_Result = BackApp.update_Local(instance: instance_M)
                if eventually_Result.isSuccess
                {
                    if isCreate
                    {
                        print(instance_M)
                        print("\(instance_M._id): CREATED")
                    }
                    else
                    {
                        print(instance_M)
                        print("\(instance_M._id): UPDATED")
                    }
                }
            }
            
            completion(cloud_Result)
        }
    }
    
    static func syncLocalDeletedWithCloud<T>(instance: T, completion: @escaping (BAResult<T>) -> Void) where T:BAObject
    {
        instance.LocalUsedProperties[k.DefaultKeys.Obj_Sync] = true
        
        //Delete the already local created object from the Cloud
        delete(instance: instance, completion: { cloud_Result in
            
            // If object Cloud save success
            if cloud_Result.isSuccess
            {
                _ = BackApp.delete_Local(instance: instance, preChecks: false)
                print("\(instance._id): DELETED")
            }
            else
            {
                //
            }
            
            instance.LocalUsedProperties.removeValue(forKey: k.DefaultKeys.Obj_Sync)
            
            completion(cloud_Result)
        })
    }
}

extension BAObject
{
    func isSyncProcess() -> Bool
    {
        if self.LocalUsedProperties[k.DefaultKeys.Obj_Sync] as? Bool == true
        {
            return true
        }
        
        return false
    }
    
    func isSyncPointer(_ pointer: BAObject) -> Bool
    {
        if pointer.isLocalObject, (self.LocalUsedProperties[k.DefaultKeys.Obj_Sync] as? Bool) == true
        {
            return true
        }
        
        return false
    }
    
    func addNeedToSync(_ fields: [String])
    {
        if self.FieldsToUpdate.isEmpty
        {
            self.FieldsToUpdate = fields.joined(separator: ",")
        }
        else
        {
            var arr = Set(self.FieldsToUpdate.components(separatedBy: ","))
            for field in fields
            {
                if field.isEmpty
                {
                    continue
                }
                
                arr.insert(field)
            }
            
            self.FieldsToUpdate = arr.joined(separator: ",")
        }
    }
}
