//
//  BAGeoPoint.swift
//  Pods
//
//  Created by GeghamHarutyunyan on 3/20/17.
//
//

import Foundation

final public class BAGeoPoint: NSObject
{
    ///Latitude of point in degrees. Valid range is from -90.0 to 90.0.
    public var latitude: Double
    
    ///Longitude of point in degrees. Valid range is from -180.0 to 180.0.
    public var longitude: Double
    
    
    //MARL: - Base
    class func geoPoint() -> BAGeoPoint
    {
        let geo = BAGeoPoint(0, lon: 0)
        return geo
    }
    
    class func geoPoint(_ lat: Double, lon: Double) -> BAGeoPoint
    {
        let geo = BAGeoPoint(lat, lon: lon)
        return geo
    }
    
    required public init(_ lat: Double, lon: Double)
    {
        self.latitude = lat
        self.longitude = lon
        
        super.init()
    }
}
