//
//  Template.swift
//  BackApp
//
//  Created by GeghamHarutyunyan on 3/7/17.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import Foundation
import BackApp

class Level: BAObject
{
    @objc dynamic var name:String?
    @objc dynamic var history = BARelation(LevelHistory.self)
    
    open class override func mapName() -> String
    {
        return "Level"
    }
    
    open override class func isLocalClass() -> Bool
    {
        return true
    }
}

class LevelHistory: BAObject
{
    @objc dynamic var lastPlayed:Date?
    @objc dynamic var minMoves = BAInteger()
    
    open class override func mapName() -> String
    {
        return "LevelHistory"
    }
    
    open override class func isLocalClass() -> Bool
    {
        return true
    }
}

class Template: BAObject
{
    @objc dynamic var name:String?
    @objc dynamic var lastViewed:Date?
    //dynamic var creator:MyUser?
    
    open class override func mapName() -> String
    {
        return "Template"
    }
    
    open override class func isLocalClass() -> Bool
    {
        return true
    }
}
