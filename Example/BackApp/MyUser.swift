//
//  MyUser.swift
//  BackApp
//
//  Created by GeghamHarutyunyan on 1/26/17.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import Foundation
import BackApp

class MyUser: BAUser
{
    @objc dynamic var isUser = BABool()
    @objc dynamic var num = BANumber()
    @objc dynamic var integerVal = BAInteger()
    @objc dynamic var birthday:Date?
    
    @objc dynamic var secondName: String?
    @objc dynamic var title: String?
    @objc dynamic var arrayObj = BAArray()
    @objc dynamic var dictObj = BADictionary()
    @objc dynamic var obj: BADictionary?
    @objc dynamic var notificationsCount = BAInteger()
    @objc dynamic var myName: String?
    @objc dynamic var createdBook: Book?
    @objc dynamic var lastBook: Book?
    @objc dynamic var myBooks = BARelation(Book.self)
    @objc dynamic var favBooks = BARelation(Book.self)
    @objc dynamic var friends = BARelation(MyUser.self)
    @objc dynamic var profileImage: BAFile?
    @objc dynamic var profileImage_2: BAFile?
    @objc dynamic var myBooksCount = BAFunction(of: .Size, relation: "myBooks")
}
