//
//  Book.swift
//  BackApp
//
//  Created by GeghamHarutyunyan on 1/24/17.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import UIKit
import BackApp

@objc
class Like: BAObject
{
    @objc var isLiked = BABool(true)
    
    open class override func mapName() -> String
    {
        return "Like"
    }
}

class Book: BAObject
{
    @objc dynamic var title:String?
    @objc dynamic var creator: MyUser?
    @objc dynamic var lastViewed:Date?
    @objc dynamic let pagesCount = BAInteger()
    @objc dynamic let authors = BARelation(MyUser.self)
    @objc dynamic var authorsCount = BAFunction(of: .Size, relation: "authors")
    @objc dynamic var myBestLike: Like?
    @objc dynamic var likes = BARelation(Like.self)
    @objc dynamic var isMe = BAFunction(of: .Filter, relation: "authors").addFilter(add: "_id", value: MyUser.currentUserID).returnType(type: MyUser.self)
    @objc dynamic var sumInt = BAFunction(of: .Sum, relation: "authors").by(property: "integerVal")
    @objc dynamic var minInt = BAFunction(of: .Min, relation: "authors").by(property: "integerVal")
    @objc dynamic var maxInt = BAFunction(of: .Max, relation: "authors").by(property: "integerVal")
    
    open class override func mapName() -> String
    {
        return "Book"
    }
}
