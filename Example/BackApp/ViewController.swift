 //
//  ViewController.swift
//  BackApp
//
//  Created by Gegham Harutyunyan on 01/16/2017.
//  Copyright (c) 2017 Gegham Harutyunyan. All rights reserved.
//

import UIKit
import BackApp


class ViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
//        BackApp.getAll_Local(items: Book.self) { (result) in
//            print(result)
//        }
//        
//        var query = BAQuery(Book.self)
//        query.includePointers()
//        BackApp.getAll_Local(items: Book.self, filter:query) { (result) in
//            print(result)
//        }
        
        if let user = MyUser.currentUser()
        {
            userLabel.text = user.username
            print(user)
            
//            BackApp.getAll_Local(items: Book.self, completion: { (result) in
//                
//                let book = result.value?.first
//                book?.title = "aaaaaa"
//                
//                self.bookObj = book
//                print(book ?? "asd")
//            })
            
            //Cloud fetch
//            user.fetch(completion: { (result) in
//                print(user)
//                
//                //Cloud fetch
//                user.fetchFull(completion: { (result) in
//                    print(user)
//                })
//                
//            })
            
            
            //Local fetch
//            user.fetchFull_Local(completion: { (result) in
//                print(user)
//            })
//            
//            print(MyUser.currentUser()!)
            
//            user.observe(.Next, callback: {
//                print(user)
//            })
//            
//            user.observe(property: "integerVal", type: .Last, callback: { 
//                print("aaa")
//            })
//            
//            user.observe(property: "birthday", type: .Last, callback: {
//                print("bbb")
//            })
//            
//            user.observe(property: "isUser", type: .Last, callback: {
//                print("ccc")
//            })
            
//            let deadlineTime = DispatchTime.now() + .seconds(10)
//            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
//                print(user)
//                print(MyUser.currentUser()!)
//            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Image Picker
    
    @IBAction func photosButtonHandler(_ sender: Any)
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum)
        {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        
        imageView.image = info[UIImagePickerControllerOriginalImage] as! UIImage?
    }
    
    @IBAction func uploadImageHandler(_ sender: Any)
    {
        
//        if let img = imageView.image
//        {
//            let file = BAFile(img, 0.7)
//            file.saveFile(completion: { (result) in
//                print(result)
//            })
//        }
        
//        BackApp.resetPassword(email: "gegham.hg@gmail.com") { (result) in
//            print(result)
//        }
        
        var query = BAInstallation.query()
        query.where(key: "deviceType", equalTo: "ios")
        
        BAPush()
            .setQuery(query)
            .setMessage("Barev Erjankutyun !!!")
            .sendPush { (result) in
            print(result)
        }
    }
    
    
    // MARK: - Handlers
    
    @IBAction func registerHandler(_ sender: Any)
    {
        let user = MyUser()
        user.username = "wwww_New"//"Gegham"
        user.password = "wwww!@#123"//"QWE!@#123"
        user.email = "gegham.hg@gmail.com"
        
        BackApp.registration(newUser: user) { (result) in
            print(result)
            
            self.userLabel.text = user.username
        }
    }

    @IBAction func loginHandler(_ sender: Any)
    {
        let user = MyUser()
        user.username = "gugu"
        user.password = "gugu!@#123"
        //user.password = nil
        
        BackApp.login(user: user) { (result) in
            print(result)
            
            //let us = MyUser.currentUser()
            
            self.userLabel.text = user.username
            
            //myBooks  //createdBook
            user.fetchFull(completion: { (fetch_result) in
                print(user)
            })
        }
    }
    
    @IBAction func logOutHandler(_ sender: Any)
    {
        BackApp.logOut()
    }
    
    // MARK: - CRUD
    var bookObj: Book? = nil
    var isTrue = false
    var lev: Level? = nil
    var like_1 : Like? = nil
    var like_2 : Like? = nil
    @IBAction func createHandler(_ sender: Any)
    {
        //////// Local relations ///////////
        /*
        if let resL_All = BackApp.getAll_Local(items: Level.self).value
        {
            for item in resL_All
            {
                print(BackApp.delete_Local(instance: item))
            }
        }
        
        if let resH_All = BackApp.getAll_Local(items: LevelHistory.self).value
        {
            for item in resH_All
            {
                print(BackApp.delete_Local(instance: item))
            }
        }
        
        
        let level = Level()
        level.name = "Level_1"
        
        let his = LevelHistory()
        his.lastPlayed = Date()
        his.minMoves.value = 14
        
        level.history.add(object: his)
        
        let result = BackApp.save_Local(instance: level)
        print(result)
        
        
        //remove relation obj
        his.minMoves.value = 25
        level.history.remove(object: his)
        
        
        let result_1 = BackApp.save_Local(instance: level)
        print(result_1)
        lev = level
        */
        
        /*let result = BackApp.getAll_Local(items: Book.self)
        if let book = result.value?.first
        {
            bookObj = book
            
            print(bookObj ?? "")
        }
        else
        {
            let book = Book()
            book.title = "lllooocccaaalll_1"
            book.lastViewed = Date()
            book.pagesCount.cValue = 10341
            let like = Like()
            book.myBestLike = like
            
            
            BackApp.save_Eventually(instance: book) { (result) in
                
                self.bookObj = book
                
                print(self.bookObj ?? "")
            }
        }*/
        
        
//        let book = Book()
//        book.title = "lllooocccaaalll"
//        book.lastViewed = Date()
//        let randomIndex = Int(arc4random_uniform(UInt32(5000)))
//        book.pagesCount.cValue = Int64(randomIndex)
//        let like = Like()
//        book.myBestLike = like
//        
//        let user = MyUser.currentUser()
//        user?.createdBook = book
//        
//        BackApp.save_Eventually(instance: user!) { (result) in
//            print(user ?? "adadad")
//        }
        
        
        /*
        let like = Like()
        BackApp.save(instance: like) { (res) in
            print(res)
            
            let book = Book()
            book.title = "lllooocccaaalll"
            book.likes.add(object: like)
            
            BackApp.delete(instance: like, completion: { (result) in
                print(result)
            })
        }
        */
        
        /*
        if isTrue == false
        {
            //isTrue = true
            /*let book = Book()
            let randomIndex = Int(arc4random_uniform(UInt32(5000)))
            book.title = "lllooocccaaalll_\(randomIndex)"
            
            let user = MyUser.currentUser()
            //user?.birthday = Date()
            user?.favBooks.add(object: book)
            
            BackApp.save_Eventually(instance: user!) { (result) in
                print(result)
            }*/
            
            let user = MyUser.currentUser()
            let book_1 = Book()
            let randomIndex_1 = Int(arc4random_uniform(UInt32(5000)))
            book_1.title = "lllooocccaaalll_\(randomIndex_1)"
            
            //user?.birthday = Date()
            user?.favBooks.add(object: book_1)
            
            BackApp.save_Eventually(instance: user!) { (result) in
                print(result)
            }
            
            print("uraaa!!!")
        }
        else
        {
            isTrue = false
            let user = MyUser.currentUser()
            user?.birthday = Date()
            BackApp.save_Eventually(instance: user!) { (result) in
                print(result)
            }
        }*/
  
  
        
        let book = Book()
//        if (self.bookObj != nil)
//        {
//            book = self.bookObj!
//        }
        
        let randomIndex = Int(arc4random_uniform(UInt32(5000)))
        print(randomIndex)
        book.title = "lllooocccaaalll_\(randomIndex)"
        
//        book.creator = MyUser.currentUser()
        like_1 = Like()
        like_2 = Like()
//        book.myBestLike = like
        
        book.myBestLike = like_1
        self.bookObj = book
        
        //book.likes.add(object: like_1!)
        BackApp.save_Eventually(instance: book) { (res) in
            print(res)
        }
        
//        book.likes.add(object: self.like_2!)
//        BackApp.save_Eventually(instance: book) { (res) in
//            print(res)
//        }
        
        
//        BackApp.save_Eventually(instance: book) { (result) in
//            
//            print(result)
//            self.bookObj = book
//            
//            let like_1 = Like()
//            like_1.isLiked.value = false
//            
//            book.likes.add(object: like_1)
//            //book.likes.remove(object: like)
//            
//            BackApp.save_Eventually(instance: book, completion: { (res) in
//                print(res)
//            })
//        }
        
        //book.creator = MyUser.currentUser()
        
//        BackApp.save_Eventually(instance: book) { (result) in
//            
//                            print(result)
//                            print(book)
//            
////            let deadlineTime = DispatchTime.now() + .seconds(5)
////            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
////                print(book)
////            }
//        }
        
//        let temp = Template()
//        temp.lastViewed = Date()
//        temp.name = "New Temp"
//        temp.creator = MyUser.currentUser()
//        
//        BackApp.create_Local(instance: temp) { (result) in
//            print(result)
//        }
    }
    
    @IBAction func updateHandler(_ sender: Any)
    {
        if bookObj != nil
        {
            bookObj?.title = "ttttttiiiiiittttttlllllleeeeee"
            
            BackApp.save_Eventually(instance: bookObj!, completion: { (result) in
                print(result)
            })
        }
        /*
        if let user = MyUser.currentUser()
        {
            user.integerVal.value = 12
            user.birthday = Date()
//            user.integerVal.increment()
            
//            let randomIndex = Int(arc4random_uniform(UInt32(5000)))
//            let book = Book()
//            book.title = "\(randomIndex)"
//            book.lastViewed = Date()
//            book.pagesCount.cValue = Int64(randomIndex - 333)
//            
//            user.myBooks.add(object: book)
            
            BackApp.save(instance: user, completion: { (result) in
                print(user)
            })
  */
            
//            user.integerVal.value = 2222
//            //user.friends.remove(object: MyUser.currentUser()!)
//            if let img = imageView.image
//            {
//                let file = BAFile(img, 0.7)
//                user.profileImage = file
//                
//                let file_1 = BAFile(img, 0.7)
//                user.profileImage_2 = file_1
//                //            file.saveFile(completion: { (result) in
//                //                print(result)
//                //            })
//            }
//            
//            BackApp.save_Eventually(instance: user, completion: { (result) in
//                print(result)
//            })
//        }
        
        
        //Make current user access with function
        //Which will return MyUser type user

        //Update User
//        if let user = MyUser.currentUser()
//        {
//            let book = Book()
//            book._id = "58b42c98295e9c1130cc0936"
//            
//            //user.createdBook = book
//            
//            BackApp.update(instance: user, completion: { (result) in
//                print(result)
//            })
//        }
        
        //Book Update
//        let book = Book()
//        book._id = "58b42c98295e9c1130cc0936"
//        
//        //book.authors.add(object: MyUser.currentUser()!)
//        
//        BackApp.update(instance: book, completion: { (result) in
//            
//            print(book)
//        })
        
        //User Update
//        let array = ["arm@gmail.com", "geg@gmail.com", "miq@gmail.com", "dav@gmail.com", "kar@gmail.com"]
//        let randomIndex = Int(arc4random_uniform(UInt32(array.count)))
//        
//        let user = MyUser.currentUser()
//        user?.email = array[randomIndex]
//        //user.arrayObj?.value?.append(5)
//        
//        let book = Book()
//        book._id = "58b3dd1e295e9c16ac1ae457"
//        
//        let book_1 = Book()
//        book_1._id = "58b3dd1e295e9c16ac1ae456"
//        
//        let book_2 = Book()
//        book_2.title = "Tataaam Book"
//        
//        user?.myBooks.add(object: book)
//        user?.myBooks.add(object: book_1)
//        user?.myBooks.add(object: book_2)
//        
//        user?.update {
//            print(user!)
//        }
    }
    
    var books = [Book]()
    @IBAction func getHandler(_ sender: Any)
    {
        /*
        if let resL_All = BackApp.getAll_Local(items: Level.self).value
        {
            print(resL_All)
            print(lev ?? "aa")
        }
        
        if let resH_All = BackApp.getAll_Local(items: LevelHistory.self).value
        {
            print(resH_All)
            print(lev ?? "aa")
        }
  */
    
        var query = Book.query()
        query.limit = 2
        
        BackApp.getAll(items: Book.self,filter: query) { (result) in
            self.books = result.value!
            print(result)
        }
        
        /*MyUser.currentUser()?.fetchFull(completion: { (result) in
            print(result)
        })*/
        
//        MyUser.currentUser()?.fetch(keys: ["createdBook"], completion: { (result) in
//            print(result)
//        })
    }
    
    @IBAction func deleteHandler(_ sender: Any)
    {
        guard bookObj == nil else {
            
            bookObj?.likes.remove(object: like_1!)
            
            BackApp.save(instance: bookObj!) { (res) in
                print(res)
            }
            
            bookObj?.likes.remove(object: like_2!)
            
            BackApp.save_Eventually(instance: bookObj!) { (res) in
                print(res)
            }
            
            return
        }
        /*
        guard bookObj == nil else {
            
            BackApp.delete_Eventually(instance: bookObj!) {result in
                
                self.bookObj = nil
                print(result)
                print("Deleted")
            }
            
            return
        }
        */
    }
    
    @IBAction func getAllHandler(_ sender: Any)
    {
        var orFilter_1 = Book.query()
        orFilter_1.where(key: "pagesCount", equalTo: 14)
        orFilter_1.where(key: "title", equalTo: "Book 144")
        
        var orFilter_2 = Book.query()
        orFilter_2.where(key: "pagesCount", equalTo: 10)
        
        var filter = BAQuery.orQuery(with: orFilter_1,orFilter_2)
//        filter.select(keys: "title", "pagesCount","creator")
//        filter.include(key: "creator")
//        filter.include(key: "authors")
//        filter.whereExists(key: "lastViewed")
//        filter.whereExists(key: "creator")
//        filter.whereDoesNotExist(key: "title")
//        filter.where(key: "authors", equalTo: MyUser.currentUser()!)
//        filter.where(key: "pagesCount", equalTo: 44)
//        filter.where(key: "lastViewed", equalTo: Date())
//        filter.where(key: "title", equalTo: "Book 2")
//        filter.where(key: "title", equalTo: [1,3,"sdfdf"])
//        filter.where(key: "lastViewed", lessThan: Date())
//        filter.where(key: "_id", lessThanOrEqual: "58c8fe96295e9c0b908d0cf8")
//        filter.where(key: "isEnd", equalTo: true)
//        filter.where(key: "_id", containedIn: ["58c9bad4295e9c0b94bbf010", "58c8ff9e295e9c0b908d0cfd", "58c8ff7a295e9c0b908d0cfc"])
        
        filter.orderBy(descending: "pagesCount")
        filter.includeAllFunctions()
//
        filter.limit = 2
//        filter.skip = 13
        
        BackApp.getAll(items: Book.self, filter: filter) { (result) in
            
            print(result.value!)
            
//            let us = MyUser.currentUser()
//            print(us!)
        }
    }
}
